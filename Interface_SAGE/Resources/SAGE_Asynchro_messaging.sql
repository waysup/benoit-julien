USE [TEST_DECALOG]
GO
/****** Object:  StoredProcedure [dbo].[ZZ_Send_Async_Message]    Script Date: 04/12/2020 17:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[ZZ_Send_Async_Message] @Msg varchar(MAX) ,@LstUSer varchar(MAX) WITH EXECUTE AS 'USER_CBASE' 
AS 
BEGIN
	SET NOCOUNT ON;

	-- Création table de mémorisation des message si n'existe pas
	IF not exists (select object_id from  sys.tables where name = 'ZZ_Async_Message' )
	BEGIN
		CREATE TABLE [dbo].[ZZ_Async_Message](
			[Msg] [varchar](max) NULL,
			[Date] [datetime] NULL
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
	END


	declare @spid smallint;

	-- test si l'utilisateur est connecté
	SELECT TOP 1 @spid = spid FROM sys.sysprocesses JOIN cbUserSession ON spid = cbSession AND cbUserName in ( @LstUSer)  WHERE dbid = DB_ID();
	IF @Msg <> ''
	BEGIN
		IF isnull(@spid,'' )  <> ''
			BEGIN
				-- SI OUI, envoi direct du message
				EXEC CB_SendMessage @spid ,  @Msg;
			END
		ELSE
			BEGIN
				-- SINON, mémorisation du message
				INSERT INTO dbo.ZZ_Async_Message (Msg, date) values (@Msg, SYSDATETIME() )
			END
	END	
	-- SI utilisateur connecté, envoi de tous les message mémorisés
	IF isnull(@spid,'') <> ''
	BEGIN
		
		declare @msg_async varchar(MAX),
				@date datetime;

		DECLARE cMessages CURSOR LOCAL FOR SELECT Msg, date FROM dbo.ZZ_Async_Message 
		OPEN cMessages;
		FETCH NEXT FROM cMessages INTO @msg_async, @Date;
		WHILE @@FETCH_STATUS = 0
		BEGIN

			-- Send async message
			set @Msg = CONCAT('Message du  : ',CONVERT(VARCHAR,@Date,23) ,CHAR(13),   @msg_async);
			EXEC CB_SendMessage @spid, @Msg ;

			-- delete from saved message
			Delete dbo.ZZ_Async_Message where Msg =  @msg_async and Date = @date;

			FETCH NEXT FROM cMessages INTO @msg_async, @Date;
		END
		CLOSE cMessages;
		DEALLOCATE cMessages;
	END
END