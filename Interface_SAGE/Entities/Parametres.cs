﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_SAGE.Entities
{
    public class Parametres
    {
        #region public properties
        // SAGE 
        public Boolean  Prod { get; set; }

        public Boolean GratuitRemise{ get; set; }
        public String Sage_Raccourci { get; set; }
        public String Sage_Utilisateur { get; set; }
        public String Sage_MdpCripte { get; set; }

        // WOocmmerce import docuemnt
        public String WOO_WorkDir { get; set; }
        public String WOO_ExportDir { get; set; }
        public String WOO_DataSep { get; set; }
        public String WOO_DataDel { get; set; }
        public String WOO_DocFicNomORDER { get; set; }
        public String WOO_DocFicNomPRODUCT { get; set; }
        public Boolean WOO_Ligne_entete { get; set; }
        public String WOO_DocSouche { get; set; }
        public String WOO_RemisePanier { get; set; }
        public String WOO_DocLibr_Origine_commande { get; set; }
        public String WOO_DocLibr_Comment_LIV { get; set; }
        public String WOO_Libr_Horodatage { get; set; }
        public String FormatDate { get; set; }
        public String PrefixClient { get; set; }
        public String WOO_ClientLibr_ID { get; set; }
        public String WooConversion { get; set; }
        public String FuturConversion { get; set; }
        public String WOO_StatClient { get; set; }
        public String WOO_Stat1 { get; set; }
        public String WOO_Stat2 { get; set; }
        public String WOO_Stat3 { get; set; }
        public String WOO_StatTarif1 { get; set; }
        public String WOO_StatTarif2 { get; set; }
        public String WOO_StatTarif3 { get; set; }
        public String WOO_StatCpta1 { get; set; }
        public String WOO_StatCpta2 { get; set; }
        public String WOO_StatCpta3 { get; set; }
        public String WOO_StatCptG1 { get; set; }
        public String WOO_StatCptG2 { get; set; }
        public String WOO_StatCptG3 { get; set; }
        public String WOO_StatPayeur1 { get; set; }
        public String WOO_StatPayeur2 { get; set; }
        public String WOO_StatPayeur3 { get; set; }
        public String WOO_ISOFR { get; set; }
        public String WOO_CptaUE { get; set; }
        public String WOO_CptaDOM { get; set; }
        public String WOO_CptaAutre { get; set; }
        public string WOO_APIURL { get; set; }
        public string WOO_APIlogin { get; set; }
        public string WOO_APIMdp { get; set; }

        // FUTURLOG API Parametres
        public String Futur_APIURI { get; set; }
        public String Futur_APIMerchantcode{ get; set; }
        public String Futur_APILogin { get; set; }
        public String Futur_APIKey { get; set; }
        public String Futur_VentesSend { get; set; }
        public String Futur_VendesGet { get; set; }
        public String Futur_AchatsSend { get; set; }
        public String Futur_AchatGet { get; set; }
        public String Futur_StockGet { get; set; }
        public String Futur_ProduitSend { get; set; }
        public String Futur_ListSoucheExport { get; set; }
        public String Futur_Libr_Horodatage { get; set; }
        public Boolean Futur_Exportconfirme { get; set; }
        public Boolean Futur_Exportcomposants { get; set; }
        public String Futur_Depot { get; set; }
        public String FTPProtocol { get; set; }
        public String FTPPort { get; set; }
        public String FTPUri { get; set; }
        public String FTPUser { get; set; }
        public String FTP_MdpCripte { get; set; }
        public String FTPSSH { get; set; }
        public String FTPPathOrder { get; set; }
        public String FTPPathProduct { get; set; }

        #endregion

        #region dynamic registry properties 

        public DateTime lastImport_Liv
        {
            get 
              { 
                try
                {
                    String LastTimeExport = DataProtection.ReadAppSetting("SAGE_lastImport_Liv");
                    return DateTime.Parse(LastTimeExport);
                }
                catch (Exception E)
                {
                    lastImport_Liv = new DateTime(2000, 01, 01);
                    String LastTimeExport = DataProtection.ReadAppSetting("SAGE_lastImport_Liv");
                    return DateTime.Parse(LastTimeExport);
                }
            }

            set
            {
                DataProtection.AddUpdateAppSettings("SAGE_lastImport_Liv", value.ToString());
            }
        }


        public DateTime lastImport_Recept
        {
            get
            {
                try
                {
                    String LastTimeExport = DataProtection.ReadAppSetting("SAGE_lastImport_Recept");
                    return DateTime.Parse(LastTimeExport);
                }
                catch (Exception E)
                {
                    lastImport_Recept = new DateTime(2000, 01, 01);
                    String LastTimeExport = DataProtection.ReadAppSetting("SAGE_lastImport_Recept");
                    return DateTime.Parse(LastTimeExport);
                }
            }
            set
            {
                DataProtection.AddUpdateAppSettings("SAGE_lastImport_Recept", value.ToString());
            }
        }
    #endregion

        #region static methods
    public static Parametres Init_Parametres()
    {
        DirectoryInfo AppDir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
        DirectoryInfo ParamDir = AppDir.CreateSubdirectory("Paramètres");
        DirectoryInfo WorkDir = AppDir.CreateSubdirectory("Exports");

        // création des fichiers paramètres
        String webfileconv = ParamDir.FullName + @"\WooConversion.csv";
        String ftpfileconv = ParamDir.FullName + @"\FuturConversion.csv";
        if (File.Exists(webfileconv) == false)
            File.Create(webfileconv);
        if (File.Exists(ftpfileconv) == false)
            File.Create(ftpfileconv);


            Parametres ParametresINI = new Parametres()
            {
                Prod = false,
                GratuitRemise = false,
                Sage_Raccourci = @"C:\Users\Public\Documents\Sage\Entreprise 100c\Bijou.gcm",
                Sage_Utilisateur = "<Administrateur>",
                Sage_MdpCripte = DataProtection.Protect(""),
                WOO_WorkDir = WorkDir.FullName,
                WOO_ExportDir = WorkDir.FullName,
                WOO_DataSep = ";",
                WOO_DataDel = "",
                WOO_DocFicNomORDER = "*orders*",
                WOO_DocFicNomPRODUCT = "*products*",
                WOO_Ligne_entete = true,
                WOO_DocSouche = "N° Pièce",
                WOO_RemisePanier = "**not used** ZREMISE",
                WOO_DocLibr_Origine_commande = "Divers",
                WOO_DocLibr_Comment_LIV = "Commentaires",
                WOO_Libr_Horodatage = "WOO_Horodatage_Ligne",
                FormatDate = "Not Used",
                PrefixClient = "C",
                WOO_ClientLibr_ID = "Ref_Externe",
                WooConversion = webfileconv,
                FuturConversion = ftpfileconv,
                WOO_StatClient = "1",
                WOO_Stat1 = "BToB",
                WOO_Stat2 = "BToC",
                WOO_Stat3 = "Other",
                WOO_StatTarif1 = "Grossistes",
                WOO_StatTarif2 = "Clients comptoir",
                WOO_StatTarif3 = "Détaillants",
                WOO_StatCpta1 = "Ventes France",
                WOO_StatCpta2 = "Ventes France",
                WOO_StatCpta3 = "Ventes France",
                WOO_StatCptG1 = "4110000",
                WOO_StatCptG2 = "4130000",
                WOO_StatCptG3 = "4110000",
                WOO_StatPayeur1 = "CWEB",
                WOO_StatPayeur2 = "",
                WOO_StatPayeur3 = "",
                WOO_ISOFR = "FR",
                WOO_CptaUE = "Ventes France",
                WOO_CptaDOM = "Ventes DOM TOM",
                WOO_CptaAutre = "Ventes export",
                WOO_APIURL = "",
                WOO_APIlogin = "",
                WOO_APIMdp = DataProtection.Protect(""),

                Futur_APIURI = "https://jws.futurlog.com",
                Futur_APIMerchantcode = "",
                Futur_APILogin = "",
                Futur_APIKey = "",
                Futur_VentesSend = "/Order/CreateNewOrder/{merchantCode}/{login}/{key}",
                Futur_VendesGet = "/Order/GetShipments/{merchantCode}/{login}/{key}/{dateFromUtc}",
                Futur_AchatsSend = "/Product/CreateNewExpectedReceipt/{merchantCode}/{login}/{key}",
                Futur_AchatGet = "/Product/GetReceipts/{merchantCode}/{login}/{key}/{dateFromUtc}",

                Futur_StockGet = "/Product/GetStocks/{merchantCode}/{login}/{key}",
                Futur_ProduitSend = "/Product/CreateNewProducts/{merchantCode}/{login}/{key}",
                Futur_ListSoucheExport = "",
                Futur_Libr_Horodatage = "",
                Futur_Exportconfirme = true,
                Futur_Exportcomposants = false,
                Futur_Depot = "",

                FTPProtocol = "FTP",
                FTPPort = "21",
                FTPUri ="",
                FTPUser="",
                FTP_MdpCripte = DataProtection.Protect(""),
                FTPSSH = "",
                FTPPathOrder = "/home/Order/",
                FTPPathProduct = "/home/Product/"
            };
        return ParametresINI;
    }
    #endregion
}

}
