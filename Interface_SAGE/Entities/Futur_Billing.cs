﻿using System;

namespace Interface_SAGE.Entities

{
    public class Billing : WebEntitie
    {

        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public String BillNumber { get; set; }
        public String City { get; set; }
        public String CorporateName { get; set; }
        public String CountryCode { get; set; }
        public Decimal? Discount { get; set; }
        public Decimal? DiscountHT { get; set; }
        public Decimal? DiscountTTC { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String ProvinceCode { get; set; }
        public Decimal? ShipmentPrice { get; set; }
        public Decimal? ShipmentVAT { get; set; }
        public Decimal? TotalAmount { get; set; }
        public String ZipCode { get; set; }



    }
}
