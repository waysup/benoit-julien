﻿using System;
using System.Collections.Generic;

namespace Interface_SAGE.Entities
{
    public class ShipmentReturn : WebEntitie   // FuturLog item   ORDER
    {
        /*
         * Url	/Order/GetShipments/{merchantCode}/{login}/{key}/{dateFromUtc}
         * Type	        GET
         * En-tête	    Accept "application/json"
         * Paramètres	dateFromUtc = Toutes les commandes dont un changement d'état a eu lieu après cette date (décalage UTC/GMT +0)
         * Formats acceptés : "yyyy-MM-dd" ou "yyyy-MM-ddTHH:mm:ss"
         * 
         * Réponse
         * HTTP Status	200
         * Corps	Liste de ShipmentReturn
        */
        public String BrandCode { get; set; }
        public String OrderNumber { get; set; }
        public String State { get; set; }
        public Int32 StateCode { get; set; }
        public List<OrderReturnLine> OrderLines{ get; set; }
        public List<Parcel> Parcels{ get; set; }
    }

    public class OrderReturnLine : WebEntitie
    {
        public Int32? InitialQuantity { get; set; }
        public String ProductCode { get; set; }
        public List<ShippedBatcheReturn> ShippedBatchesReturn { get; set; }
    }

    public class ShippedBatcheReturn : WebEntitie
    {
        public String Number { get; set; }
        public Int32? Quantity { get; set; }
    }

    public class Parcel : WebEntitie
    {
        public String CarrierCode { get; set; }
        public String PackNumber { get; set; }
        public DateTime? ShippingDate { get; set; }
        public String State { get; set; }
        public Int32 StateCode { get; set; }
        public String TrackingNumber { get; set; }
        public String TrackingUrl { get; set; }
        public Decimal? Weight { get; set; }
        public List<ParcelLine> ParcelLines{ get; set; }


    }
    public class ParcelLine : WebEntitie
    {
        public String ProductCode { get; set; }
        public Int32 Quantity { get; set; }
    }
}
