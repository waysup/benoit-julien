﻿using System;
using System.Collections.Generic;

namespace Interface_SAGE.Entities
{

    public class ReceiptReturn : WebEntitie
    {
        /*
        * Obtenir la liste des réceptions
        * Requête
        * Url	    /Product/GetReceipts/{merchantCode}/{login}/{ key}/{ dateFromUtc}
        * Type      GET
        * En-tête	Accept "application/json"
        * 
        * Réponse
        * HTTP Status	200
        * Corps	Liste de ReceiptReturn
        * 
        */


        public DateTime EntryDateUtc { get; set; }
        public DateTime? ReceiptDate { get; set; }
        public String ReceiptNumber { get; set; }
        public String State { get; set; }
        public Int32 StateCode { get; set; }
        public List<ReceipReturnLine> ReceiptLines { get; set; }
    }
    public class ReceipReturnLine : WebEntitie
    {
        public String ProductCode { get; set; }
        public Int32 Quantity { get; set; }
        public Int32? ReceivedQuantity { get; set; }
        public Int32? ReceivedQuantityDamaged { get; set; }
        public List<ReceiptLinePartialReception> ReceiptLinePartialReceptions { get; set; }
    }

    public class ReceiptLinePartialReception : WebEntitie
    {
        public DateTime Date { get; set; }
        public String ProductBatchNumber { get; set; }
        public Int32? ReceivedQuantity { get; set; }
        public Int32? ReceivedQuantityDamaged { get; set; }
    }

}