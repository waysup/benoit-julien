﻿using System;

namespace Interface_SAGE.Entities
{
    public class Address : WebEntitie
    {
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public String City { get; set; }
        public String Comments { get; set; }
        public String CorporateName { get; set; }
        public String CountryCode { get; set; }
        public String Email { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MobilePhone { get; set; }
        public String Phone { get; set; }
        public String PickupPointNumber { get; set; }
        public String ProvinceCode { get; set; }
        public String ZipCode { get; set; }

    }
   
}

