﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_SAGE.Entities
{

    // Toute les classe mappé dans le module de conversion doivent hériterr de celle-ci pour l'implémentation des interface de comparaison de classe
    public class WebEntitie : IComparable, IComparable<WebEntitie>, IComparer<WebEntitie>
    {
        public int CompareTo(object obj)
        {
            return this.Equals(obj) ? 0 : 1;
        }

        public int CompareTo(WebEntitie other)
        {
            return this.Equals(other) ? 0 : 1;
        }

        int IComparer<WebEntitie>.Compare(WebEntitie x, WebEntitie y)
        {
            return x.CompareTo(y);
        }
    }
}
