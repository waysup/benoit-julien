﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_SAGE.Entities
{
    public class WOO_Stock
    {
        public String Reference { get; set; }//Référence
        public String Designation { get; set; }  //Désignation
        public String Design_Long { get; set; }  //Descriptif détaillé
        public String Liste_Bundle { get; set; }  //Liste de Bundle
        public String Famille { get; set; }  //Famille
        public String PU_public { get; set; }  //Prix unitaire HT
        public String Taux { get; set; }  //Taux TVA

    }
}
