﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_SAGE.Entities
{
    public class WOO_DOC_statut
    {
        public String Cde_Numero { get; set; }//Numéro de commande web 
        public Int32 Cde_Etat { get; set; }//État de la commande
        public DateTime Cde_Date { get; set; }//Date de commande
        public DateTime Cde_DateLiv { get; set; }//Date de Livraison
        public String Cde_Reference { get; set; }//Référence
        public Decimal Cde_QteLivree { get; set; }//Quantité Livrée
    }
}
