﻿using Interface_SAGE.Formats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_SAGE.Entities
{
    public class WOO_Doc : WebEntitie
    {
        public String Ligne_Numero { get; set; }//Numéro de ligne
        public String Cde_Numero{ get; set; }//Numéro de commande web
        public String Cde_Etat { get; set; }//État de la commande
        public String Cde_Date { get; set; }//Date de commande
        public String Cde_Transac { get; set; }//ID de transaction
        public String Cde_Devise { get; set; }//Devise
        public String Client_Note { get; set; }//Note du client
        public String Client_ID { get; set; }//ID du client
        public String Client_Email { get; set; }//E-mail du client,
        public String Client_Nom { get; set; }//Nom complet (Facturation)
        public String Client_Societe { get; set; }//Société (Facturation)
        public String Client_Adr1 { get; set; }//Adresse 1 (Facturation)
        public String Client_Adr2 { get; set; }//Adresse 2 (Facturation)
        public String Client_Ville { get; set; }//Ville (Facturation)
        public String Client_CP { get; set; }//Code postal (Facturation)
        public String Client_Pays { get; set; }//Nom du pays (Facturation)
        public String Client_tel { get; set; }//Téléphone (Facturation)
        public String Liv_Nom { get; set; }//Nom complet (Livraison)
        public String Liv_Societe { get; set; }//Société (Livraison)
        public String Liv_Adr1 { get; set; }//Adresse 1 (Livraison)
        public String Liv_Adr2 { get; set; }//Adresse 2 (Livraison)
        public String Liv_CP { get; set; }//Code postal (Livraison)
        public String Liv_Ville { get; set; }//Ville (Livraison)
        public String Liv_Pays { get; set; }//Nom du pays (Livraison)
        public String Ligne_Article { get; set; }//UGS
        public String Ligne_NumberOf_Article { get; set; }//Article #
        public String Ligne_Design { get; set; }//Nom de l’élément
        public String Ligne_PURem { get; set; }//Prix du produit
        public String Ligne_qte { get; set; }//Quantité (- Remboursement)
        public String Ligne_PUHT { get; set; }//Sous-total de la ligne de commande
        public String Ligne_CouponCod { get; set; }//Code de coupon
        public String Ligne_CouponType { get; set; }//Type de coupon
        public String Ligne_couponDesc { get; set; }//Description du coupon
        public String Ligne_ReducMont { get; set; }//Montant de la réduction de l’article
        public String Ligne_Taux { get; set; }//Taux de taxe de l’article
        public String Mode_Paiement { get; set; }//Mode de paiement
        public String Mode_PaiementLib { get; set; }//Titre de la méthode de paiement
        public String Cde_Remise { get; set; }//Montant de la remise panier,
        public String Cde_LivraisonLib { get; set; }//Titre de la méthode de livraison,
        public String Cde_LivraisonCode { get; set; }//Méthode de livraison
        public String Cde_LivraisonTTC { get; set; }//Livraison + Taxe
        public String Cde_LivraisonHT { get; set; }//Montant de la livraison (- Remboursement)
        public String Cde_MontantTTC { get; set; }//Montant total de la commande (- Remboursement)

        public String Client_TypeClient { get; set; } // TODO    a ajouter typologie de client
        public String Client_ISOPays { get; set; } // TODO    a ajouter typologie de client

        public static WOO_Doc  Get_FromCSV(String[] data)
        {
            if (data.Length < 45)
                return null;

            

            WOO_Doc wDoc = new WOO_Doc()
            {
                Ligne_Numero = data[0],
                Cde_Numero = data[1],
                Cde_Etat = data[2],
                Cde_Date = data[3],
                Cde_Transac = data[4],
                Cde_Devise = data[5],
                Client_Note = data[6].Substring(0, Math.Min(69, data[6].Length)),
                Client_ID = data[7],
                Client_Email = data[8].Substring(0, Math.Min(69, data[8].Length)),
                Client_Nom = data[9].Substring(0, Math.Min(69, data[9].Length)),
                Client_Societe = data[10].Substring(0, Math.Min(35, data[10].Length)),
                Client_Adr1 = data[11].Substring(0, Math.Min(35, data[11].Length)),
                Client_Adr2 = data[12].Substring(0, Math.Min(35, data[12].Length)),
                Client_Ville = data[13].Substring(0, Math.Min(35, data[13].Length)),
                Client_CP = data[14].Substring(0, Math.Min(35, data[14].Length)),
                Client_Pays = data[15].Substring(0, Math.Min(35, data[15].Length)),
                Client_tel = data[16].Substring(0, Math.Min(21, data[16].Length)),
                Liv_Nom = data[17].Substring(0, Math.Min(69, data[17].Length)),
                Liv_Societe = data[18].Substring(0, Math.Min(35, data[18].Length)),
                Liv_Adr1 = data[19].Substring(0, Math.Min(35, data[19].Length)),
                Liv_Adr2 = data[20].Substring(0, Math.Min(35, data[20].Length)),
                Liv_CP = data[21].Substring(0, Math.Min(9, data[21].Length)),
                Liv_Ville = data[22].Substring(0, Math.Min(35, data[22].Length)),
                Liv_Pays = data[23].Substring(0, Math.Min(35, data[23].Length)),
                Ligne_Article = data[24],
                Ligne_NumberOf_Article = data[25],
                Ligne_Design = data[26].Substring(0, Math.Min(69, data[26].Length)),
                Ligne_PURem = data[27].Replace('.',','),
                Ligne_qte = data[28].Replace('.', ','),
                Ligne_PUHT = data[29].Replace('.', ','),
                Ligne_CouponCod = data[30],
                Ligne_CouponType = data[31],
                Ligne_couponDesc = data[32],
                Ligne_ReducMont = data[33].Replace('.', ','),
                Ligne_Taux = data[34].Replace('.', ','),
                Mode_Paiement = data[35],
                Mode_PaiementLib = data[36],
                Cde_Remise = data[37].Replace('.', ','),
                Cde_LivraisonLib = data[38],
                Cde_LivraisonCode = data[39],
                Cde_LivraisonTTC = data[40].Replace('.', ','),
                Cde_LivraisonHT = data[41].Replace('.', ','),
                Cde_MontantTTC = data[42].Replace('.', ','),
                Client_TypeClient= data[43],
                 Client_ISOPays = data[44],
            };
            // Format colonnes
            Liste_Correspondance lst_conv = Liste_Correspondance.Get_Liste_Correspondance();
            lst_conv.Conv_allDonnee(wDoc);

            return wDoc;
        }
    }
}
