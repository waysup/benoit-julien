﻿using System;
using System.Collections.Generic;

namespace Interface_SAGE.Entities
{
    public class Product : WebEntitie
    {
        /*
         * Déclarer un nouvel article
         * Requête
         * Url	    /Product/CreateNewProduct/{merchantCode}/{login}/{key}         /Product/UpdateProduct/{merchantCode}/{login}/{key}
         * Type	    POST
         * En-tête	Accept "application/json"
         * Corps	Product
         *             
         * Commentaire	Si le produit existe déjà, il est mis à jour
        */

        /*
         * 
         * Déclarer des articles en masse
         * Requête
         * Url	    /Product/CreateNewProducts/{merchantCode}/{login}/{key}        /Product/UpdateProducts/{merchantCode}/{login}/{key}
         * Type	    POST
         * En-tête	Accept "application/json"
         * Corps	Liste de Product
         * 
         * Commentaire	Si un produit existe déjà, il est mis à jour
         * Nb: En cas d'erreur sur un produit, aucun produit n'est enregistré
        */

        public String AlertThreshold { get; set; }
        public String BarCode { get; set; }
        public String Code { get; set; }
        public String Color { get; set; }
        public String ExternalCode { get; set; }
        public String Family { get; set; }
        public Decimal? Height { get; set; }
        public String HsCode { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean IsLotManaged { get; set; }
        public String Label { get; set; }
        public Decimal? Length { get; set; }
        public String Model { get; set; }
        public String OriginCountryCode { get; set; }
        public String Parameter1 { get; set; }
        public String Parameter2 { get; set; }
        public String PictureUrl { get; set; }
        public String Size { get; set; }
        public Decimal? TvaRate { get; set; }
        public String Type { get; set; }
        public Decimal? UnitPriceHT { get; set; }
        public Decimal? WeePrice { get; set; }
        public Decimal? Weight { get; set; }
        public Decimal? Width { get; set; }
        public List<ProductInBundle> ProductsInBundle { get; set; }
    }

    public class ProductInBundle : WebEntitie
    {
        public String Code { get; set; }
        public int? Quantity { get; set; }
    }
}
