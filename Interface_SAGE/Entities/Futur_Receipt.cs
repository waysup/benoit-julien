﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_SAGE.Entities
{
    /*
     * Créer un nouvel attendu de réception
     * Requête: 
     * Url	    /Product/CreateNewExpectedReceipt/{merchantCode}/{login}/{key}
     * Type	    POST
     * En-tête	Accept "application/json"
     * Corps	Receipt
    */
    public class Receipt : WebEntitie
    {
        public String CarrierName { get; set; }
        public String DeliveryComments { get; set; }
        public String ReceiptNumber { get; set; }
        public DateTime ScheduledDate { get; set; }
        public String SupplierName { get; set; }
        public List<ReceiptLine> ReceiptLines { get; set; }
    }
    public class ReceiptLine : WebEntitie
    {
        public String ProductCode { get; set; }
        public Int32 Quantity { get; set; }
    }
}
