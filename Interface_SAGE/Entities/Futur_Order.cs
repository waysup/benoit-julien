﻿using System;
using System.Collections.Generic;

namespace Interface_SAGE.Entities
{
    /*
     * Déclarer une nouvelle commande
    * Requête
    * Url	    /Order/CreateNewOrder/{merchantCode}/{login}/{key}
    * Type	    POST
    * En-tête	Accept "application/json"
    * Corps	    Order
    */
    public class Order : WebEntitie   // FuturLog item   ORDER
    {
        public String BrandCode { get; set; }
        public String CurrencyCode { get; set; }
        public String CustomerNumber { get; set; }
        public DateTime? DateUtc { get; set; }
        public String Language { get; set; }
        public String MerchantCarrierCode { get; set; }
        public String MerchantCarrierLabel { get; set; }
        public String OrderNumber { get; set; }
        public String PickerComments { get; set; }
        public DateTime? ScheduledTransmissionDate { get; set; }
        public String ShippingServiceCode { get; set; }
        public Address Address { get; set; }
        public Billing Billing { get; set; }
        public List<Gift> Gift { get; set; }    // TODO CONFLIT de définition, surveiller si liste ou single
        public List<OrderLine> OrderLines { get; set; }

    }
    public class OrderLine : WebEntitie
    {
        public String ProductCode { get; set; }
        public String ProductLabel { get; set; }
        public Int32? Quantity { get; set; }
        public Decimal? UnitPrice { get; set; }
        public Decimal? UnitPriceHT { get; set; }
        public Decimal? UnitPriceTTC { get; set; }
        public Decimal? VATRate { get; set; }

    }


    public class Gift : WebEntitie
    {
        public String Message { get; set; }
        public Int32? PackageType { get; set; }
    }
}
