﻿using Interface_SAGE.Helper;
using Objets100cLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface_SAGE.Process
{
    public class VentesLivraison
    {

        //Fonction permettant de tester si le numéro de série a déjà été affectée
        //à la ligne de document
        /* Exemple utilisation fonctione SERIE, 
          * Auteur CTD / documentation objet métier V7 -> traduit en C#
          */
        public static Boolean SerieAlreadyUse(IPMDocTransformer pTransfo, IBODocumentVenteLigne3 pLig, IBOArticleDepotLot pLot)
        { Boolean bRes = false;
            try
            {
                foreach (IBODocumentVenteLigne3 pL in pTransfo.ListLignesATransformer)
                {
                    if (pL.Article != null & pL.Article.Equals(pLig.Article) & pTransfo.UserLotsToUse[pL].Count > 0)
                    {
                        for (int i = 1; i <= pTransfo.UserLotsToUse[pL].Count; i++)
                        {
                            IUserLot pTmpUserLot = pTransfo.UserLotsToUse[pL][i];
                            if (pTmpUserLot.Lot.Equals(pLot))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { Console.WriteLine(ex.Message); }
            return bRes;
        }

        /* Procédure permettant d'affecter des numéros de série aux lignes du processus
         * Auteur CTD / documentation objet métier V7 -> traduit en C#
         * ******
         * Modification : Ajout en parametre puis en Query pas N° au lieu de liste
         * ******
         */
        public static  Boolean  SetSerie(IPMDocTransformer pTransfo, IBODocumentVenteLigne3 pLig, String Serie)
        {
            if (String.IsNullOrEmpty(Serie) == true)
            {
                try
                {
                    Boolean bReadAllSerie = false;
                    while (bReadAllSerie == false & pTransfo.UserLotsQteRestantAFournir[pLig] > 0)
                    {
                        IBODepot3 pDepot = pLig.Depot;
                        IBOArticleDepot3 pArtDepot = pLig.Article.FactoryArticleDepot.ReadDepot(pDepot);

                        foreach (IBOArticleDepotLot pArtDepotLot in pArtDepot.FactoryArticleDepotLot.QueryNoSerie(Serie))
                        {
                            if (pArtDepotLot.IsEpuised == false & pArtDepotLot.StockATerme() > 0 & SerieAlreadyUse(pTransfo, pLig, pArtDepotLot) == false)
                            {
                                IUserLot pUserLot = pTransfo.UserLotsToUse[pLig].AddNew();
                                pUserLot.Set(pArtDepotLot, 1, pArtDepotLot.Complement);
                                bReadAllSerie = false;
                                return true; ;
                            }
                            
                        }
                        bReadAllSerie = true;

                    }
                }
                catch (Exception ex)
                { Commun.Journalise("Erreur Recherche dispo serie ", ex.Message); }
                return false;
            }
            else
            {
                if ( pTransfo.UserLotsQteRestantAFournir[pLig] > 0)
                {
                    IBODepot3 pDepot = pLig.Depot;
                    IBOArticleDepot3 pArtDepot = pLig.Article.FactoryArticleDepot.ReadDepot(pDepot);

                    foreach (IBOArticleDepotLot pArtDepotLot in pArtDepot.FactoryArticleDepotLot.QueryNoSerie(Serie))
                    {
                        if (pArtDepotLot.IsEpuised == false & pArtDepotLot.StockATerme() > 0 & SerieAlreadyUse(pTransfo, pLig, pArtDepotLot) == false)
                        {
                            IUserLot pUserLot = pTransfo.UserLotsToUse[pLig].AddNew();
                            pUserLot.Set(pArtDepotLot, 1, pArtDepotLot.Complement);
                            return true; ;
                        }
                    }
                }
                return false;
            }
        }

        /* Procédure permettant d'affecter des numéros de lot aux lignes du processus
         * Auteur CTD / documentation objet métier V7 -> traduit en C#
         * ******
         * Modification : Ajout en parametre puis en Query pas N° au lieu de liste
         * ******
         */
        public static Boolean SetLot(IPMDocTransformer pTransfo, IBODocumentVenteLigne3 pLig,String Lot, Double dQteFournirLot, Dictionary<IBOArticleDepotLot,Double> Table_Lot)
        {
            try
            {
                IBODepot3 pDepot = pLig.Depot;
                IBOArticleDepot3 pArtDepot = pLig.Article.FactoryArticleDepot.ReadDepot(pDepot);
                int i = 0;

                foreach (IBOArticleDepotLot pArtDepotLot in pArtDepot.FactoryArticleDepotLot.QueryNoSerie(Lot))
                {
                    Double dQteTb = 0;
                    Double dQteFournir = Math.Min(pTransfo.UserLotsQteRestantAFournir[pLig], dQteFournirLot);
                    if (!pArtDepotLot.IsEpuised & pArtDepotLot.StockATerme() > 0)
                    {
                        IUserLot pUserLot = pTransfo.UserLotsToUse[pLig].AddNew();



                        if (Table_Lot.TryGetValue(pArtDepotLot, out double qtyused) == true & qtyused > 0)
                        {
                            dQteTb = Table_Lot[pArtDepotLot];
                            if (dQteFournir <= dQteTb)
                            {
                                pUserLot.Set(pArtDepotLot, dQteFournir, pArtDepotLot.Complement);
                                Table_Lot[pArtDepotLot] = dQteTb - dQteFournir;
                                return true;
                            }
                            else
                            {
                                pUserLot.Set(pArtDepotLot, dQteTb, pArtDepotLot.Complement);
                                Table_Lot[pArtDepotLot] = 0;
                                dQteFournirLot -= dQteTb;
                            }
                        }
                        else
                        {
                            if (dQteFournir <= pArtDepotLot.StockATerme())
                            {
                                pUserLot.Set(pArtDepotLot, dQteFournir, pArtDepotLot.Complement);
                                Table_Lot.Add(pArtDepotLot, pArtDepotLot.StockATerme() - dQteFournir);
                                return true;
                            }
                            else
                            {
                                pUserLot.Set(pArtDepotLot, pArtDepotLot.StockATerme(), pArtDepotLot.Complement);
                                Table_Lot.Add(pArtDepotLot, 0);
                                dQteFournirLot -= dQteTb;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
            return false;
        }
        
        public static IBICollection ValideLivraisonProcess(IPMDocTransformer pTransfo,Boolean simulation)
        {
            try
            {
                if (pTransfo.CanProcess)
                {
                    if (simulation == false)
                    {
                        pTransfo.Process();
                        return pTransfo.ListDocumentsResult;
                    }
                }
                else
                    throw new Exception("Validation du document commercial impossible : ");
            }
            catch (Exception ex)
            {
                StringBuilder erreurs = new StringBuilder(ex.Message);

                foreach (IFailInfo om_Erreur in pTransfo.Errors)
                    erreurs.AppendLine(om_Erreur.Text);

                Commun.Journalise("ValideDocCommerciale", "Erreurs du process de création : " + erreurs.ToString());
                throw new Exception(erreurs.ToString());
            }
            return null;
        }
    }
}

