﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Helper;
using Objets100cLib;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Security.AccessControl;
using System.Security.Principal;
using Interface_SAGE.Core;
using Interface_SAGE.Entities;
using Interface_SAGE.Formats;
using Newtonsoft.Json;
using Interface_SAGE.Process;

namespace Interface_SAGE.Tasks
{

    public class Futur_VenteGet
    {
        private MainProgram Core;
        private Boolean Verbose;
        private String FicNameJsonFake;

        BSCIALApplication100c om_Cial;
        BSCPTAApplication100c om_Cpta;

        public Futur_VenteGet(MainProgram core, String JsonFake = "", Boolean verbose = false)
        {
            Core = core;
            Verbose = verbose;
            FicNameJsonFake = JsonFake;

            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }

        public async Task ImportDoc(int mode)
        {

            // connexion à l'API - récupération du TOKEN
            List<ShipmentReturn> ListDoc = null;
            try
            {
                if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                {
                    try
                    {
                        /*ListDoc = new List<ShipmentReturn>();
                        var s = new ShipmentReturn();
                        OrderReturnLine l = new OrderReturnLine();
                        l.ProductCode = "aaa";
                        l.ShippedBatchesReturn = new List<ShippedBatcheReturn>();
                        OrderReturnLine l2 = new OrderReturnLine();
                        l2.ProductCode = "aaa";
                        l2.ShippedBatchesReturn = new List<ShippedBatcheReturn>();
                        s.OrderLines = new List<OrderReturnLine>(){l,l2}; 
                        ListDoc.Add(s);
                        String j = Newtonsoft.Json.JsonConvert.SerializeObject(ListDoc, Newtonsoft.Json.Formatting.Indented);
                        File.AppendAllText(FicNameJsonFake, j);
                        */
                        Commun.Journalise("MODE SIMULATION", "lecture JSON dans fichier, -- " + FicNameJsonFake);

                        String jsondata = File.ReadAllText(FicNameJsonFake);

                        ListDoc = JsonConvert.DeserializeObject<List<ShipmentReturn>>(jsondata);
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("Import_DocumentEAsync", "ERREUR lecture du fichier JSON Fake : " + E.Message, Fl_Alerte: !Core.Automate);
                        return;
                    }
                }
                else
                {
                    DateTime UTC = Core.GetParametres.lastImport_Liv.ToUniversalTime();
                    string strUTC = UTC.ToString("u");
                    String URL = Core.GetParametres.Futur_VendesGet.Replace("{dateFromUtc}", strUTC);

                    // Récupération des commandes par interrogation de l'API
                    ListDoc = await Core.WebAPI.Get<List<ShipmentReturn>>(URL);

                    Core.GetParametres.lastImport_Liv = DateTime.Now;

                    if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                    {
                        String j = Newtonsoft.Json.JsonConvert.SerializeObject(ListDoc, Newtonsoft.Json.Formatting.Indented);
                        File.AppendAllText(FicNameJsonFake, j);
                    }
                }

            }
            catch (Exception E)
            {
                Commun.Journalise("Import_DocumentEAsync", "ERREUR lor de l'appel de l'API : " + E.Message, Fl_Alerte: !Core.Automate);
                return;
            }

            ListDoc.RemoveAll(x => x.StateCode != 39 & x.StateCode != 40 );


            if (ListDoc.Count == 0)
                return;

            Commun.Journalise("Import_Documents", "Liste de document à traiter : " + ListDoc.Count);

            foreach (ShipmentReturn wDoc in ListDoc)
            {
                String Msgerror = "";

                try
                {
                    if (shipment_Treatment(wDoc, ref Msgerror) == false)
                    {
                        // Erreur traité dans finnaly
                    }
                }
                catch (Exception E)
                {
                    if (Verbose)
                    {
                        Msgerror = " Inattendue " + E.Message + Environment.NewLine + " / " + E.StackTrace + Environment.NewLine + " / " + E.TargetSite;
                        Msgerror += E.InnerException != null ? Environment.NewLine + E.InnerException.Message : "";
                    }
                    else
                    {
                        Msgerror = " Inattendue " + E.Message;
                    }
                }
                finally
                {
                    if (String.IsNullOrEmpty(Msgerror) == true)
                        Commun.Journalise("Intégration DOC", "Traitement livraison de la pièce avec succès N° " + wDoc.OrderNumber);
                    else
                        Commun.Journalise("Intégration DOC", "ERREUR à la pièce  " + wDoc.OrderNumber + " : " + Msgerror, Fl_Alerte: !Core.Automate);
                }
            }
        }

        private Boolean shipment_Treatment(ShipmentReturn wDoc, ref String MsgErr)
        {
            // Recherche du docuemnt
            if (om_Cial.FactoryDocumentVente.ExistPiece(DocumentType.DocumentTypeVenteCommande, wDoc.OrderNumber) == false)
            {
                MsgErr = "La commande " + wDoc.OrderNumber + " n'a pas été trouvée";
                return false;
            }

            IBODocumentVente3 oDoc = om_Cial.FactoryDocumentVente.ReadPiece(DocumentType.DocumentTypeVenteCommande, wDoc.OrderNumber) as IBODocumentVente3;
            var Tmp_LotSAGE = new Dictionary<IBOArticleDepotLot, double>();
            var Tmp_LotFutur = new List<ShippedBatcheReturn>();
            IPMDocTransformer IProc = om_Cial.Transformation.Vente.CreateProcess_Livrer();
            IProc.AddDocument(oDoc);

            try
            {

                foreach (IBODocumentVenteLigne3 oLigne in IProc.ListLignesATransformer)
                {
                    if (oLigne.Article == null & oLigne.Depot == null)
                        continue;

                    if (String.IsNullOrEmpty(Core.GetParametres.Futur_Depot) == false)
                        if (oLigne.Depot.DE_Intitule != Core.GetParametres.Futur_Depot & oLigne.Depot.DE_Code != Core.GetParametres.Futur_Depot)
                            continue;

                    
                    Double Qte_ResteA_Livrer = oLigne.DL_QteBC;

                    foreach (OrderReturnLine wligne in wDoc.OrderLines.Where(x => x.ProductCode == oLigne.Article.AR_Ref))
                    {


                        if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie | oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot)
                        {
                            foreach (ShippedBatcheReturn batch in wligne.ShippedBatchesReturn)
                            {
                                if (Tmp_LotFutur.Contains(batch))
                                    continue;

                                if (Qte_ResteA_Livrer == 0)
                                    continue;

                                if (String.IsNullOrEmpty(batch.Number) == false)
                                {
                                    //Test sur le suivi de stock de l'article
                                    if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie)
                                    {
                                        //Affectation des numéros de série
                                        if (VentesLivraison.SetSerie(IProc, oLigne, batch.Number) == true)
                                        {
                                            Tmp_LotFutur.Add(batch);
                                            Qte_ResteA_Livrer -= (double)batch.Quantity;
                                        }
                                    }
                                    else if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot)
                                    {
                                        //Affectation des numéros de lot
                                        if (VentesLivraison.SetLot(IProc, oLigne, batch.Number, (Double)batch.Quantity, Tmp_LotSAGE) == true)
                                        {
                                            Tmp_LotFutur.Add(batch);
                                            Qte_ResteA_Livrer -= (double)batch.Quantity;
                                        }
                                    }
                                    else
                                    {
                                        MsgErr += "L'article " + oLigne.Article.AR_Ref + " n'est pas suivi par n° lot ou série";
                                    }

                                }
                                else
                                {
                                    if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie | oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot)
                                    {
                                        MsgErr += "L'article " + oLigne.Article.AR_Ref + " est pas suivi par n° lot ou série";
                                    }
                                    Tmp_LotFutur.Add(batch);
                                    Qte_ResteA_Livrer -= (double)batch.Quantity;
                                }
                            }
                        }

                        else
                        {
                            double Quantity = 0;
                            foreach ( Parcel p in wDoc.Parcels)
                            {
                                Quantity += p.ParcelLines.Where(x => x.ProductCode == oLigne.Article.AR_Ref).Sum(x => x.Quantity);
                            }

                            //Tmp_LotFutur.Add(batch);
                            Qte_ResteA_Livrer -= Quantity;
                            
                        }
                    }

                    if (Qte_ResteA_Livrer != 0)
                    {
                        oLigne.DL_QtePL = oLigne.DL_QteBC - Qte_ResteA_Livrer;      // Persistant dans process ????
                        // INFO  "L'article " + oLigne.Article.AR_Ref + " est pas suivi par n° lot ou série";
                    }
                }
            }
            catch (Exception E)
            {
                MsgErr += "Erreur lors d'affectation des lots : " + E.Message;
                return false;
            }

            foreach (OrderReturnLine wligne in wDoc.OrderLines)
                if (wligne.ShippedBatchesReturn != null)
                    foreach (ShippedBatcheReturn batch in wligne.ShippedBatchesReturn)
                        if (Tmp_LotFutur.Contains(batch) == false)
                            MsgErr += " Erreur, le lot : " + batch.Number + " n'a pas pu être affecté.";

            if (MsgErr != "")
                return false;



            // indisponibilité de stock normallement géré par le process
            /* foreach (IBODocumentVenteLigne3 pLig in IProc.ListLignesATransformer)
             {

                 if (IProc.UserLotsQteRestantAFournir[pLig] > 0)
                 {
                     if (IProc.UserLotsQteRestantAFournir[pLig] == pLig.DL_Qte)
                         IProc.RmvDocumentLigne(pLig);

                     pLig.DL_QtePL = pLig.DL_QtePL - IProc.UserLotsQteRestantAFournir[pLig];
                 }
             }*/

            try
            {
                if (IProc.CanProcess)
                {
                    IProc.Process();
                }
                else
                    throw new Exception("Transformation Réception");
            }
            catch (Exception ex)
            {
                StringBuilder erreurs = new StringBuilder(ex.Message);

                foreach (IFailInfo om_Erreur in IProc.Errors)
                    erreurs.AppendLine(om_Erreur.Text);

                MsgErr += "Erreurs du process de création : " + erreurs.ToString();
                return false;
            }

            return true;
        }
    }
}
