﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Helper;
using Objets100cLib;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Security.AccessControl;
using System.Security.Principal;
using Interface_SAGE.Core;
using Interface_SAGE.Entities;
using Interface_SAGE.Formats;

namespace Interface_SAGE.Tasks
{

    public class Futur_AchatSend
    {
        private MainProgram Core;
        //private Liste_Correspondance lst_conv;
        private DateTime Date_Export;
        private Boolean Verbose;
        private String FicNameJsonFake;

        BSCIALApplication100c om_Cial;
        BSCPTAApplication100c om_Cpta;

        public Futur_AchatSend(MainProgram core, String JsonFake = "", Boolean verbose = false)
        {
            Core = core;
            Date_Export = DateTime.Now;
            //lst_conv = Liste_Correspondance.Get_Liste_Correspondance(core);
            Verbose = verbose;
            FicNameJsonFake = JsonFake;

            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }

        public async 
        Task
ExportAchat(int mode)
        {
            /*
             * Mode 0 = Normal
             * Mode 1 = Sans horodatage
             * Mode 2 = Exporté tout
             */

            // Chargement du critère SOUCHE
            List<IBISouche> Lst_souch = Get_List_Souche();

            // Parcours de la liste des documens actifs

            string libre_horo = Core.GetParametres.Futur_Libr_Horodatage;
            DocumentType DocType = DocumentType.DocumentTypeAchatCommandeConf;

            Dictionary<IBODocumentAchat3, Receipt> List_export = new Dictionary<IBODocumentAchat3, Receipt>();
            Boolean Au_moins_un_doc = false;

            foreach (IBODocumentAchat3 oDoc in om_Cial.FactoryDocumentAchat.QueryTypePieceOrderPiece(DocType, "", "ZZZZZZ"))
            {

                // test sur la souche docuemnt
                if (Lst_souch.Contains(oDoc.Souche) == false)
                    continue;

                if (Core.GetParametres.Futur_Exportconfirme == true)
                {
                    if (oDoc.DO_Statut != DocumentStatutType.DocumentStatutTypeAPrepare)
                        continue;
                }

                // Mode simulation, ou ré-exportation totale
                if (mode != 2)
                {
                    // Test de l'horodatage
                    if (oDoc.InfoLibre[libre_horo] != null &
                        oDoc.InfoLibre[libre_horo] != new DateTime(1899, 12, 30) &
                        oDoc.InfoLibre[libre_horo] != new DateTime(1753, 01, 01))
                        continue;
                }

                try
                {

                    List<ReceiptLine> lignes = Get_List_Line(oDoc);
                    if (lignes.Count == 0)
                        continue;


                    String CurrencyCode = "EUR";
                    foreach (IBPDossierCial DosParam in om_Cial.FactoryDossier.List)
                    {
                        IBPDevise2 devise = DosParam.DeviseCompte;
                        if (devise != null)
                            CurrencyCode = devise.D_CodeISO;
                        break;
                    }

                    // Réservation du document
                    try
                    { oDoc.CouldModified(); }
                    catch
                    {
                        Commun.Journalise("Export Achat", "Le document " + oDoc.DO_Piece + " est ouvert, il ne pourra pas être exporté.");
                        continue;
                    }

                    // Ajout du document à la liste à exporter
                    Receipt newRecept = new Receipt
                    {
                        CarrierName = oDoc.Expedition == null? "" : oDoc.Expedition.E_Intitule,
                        DeliveryComments = oDoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Comment_LIV],
                        ReceiptNumber = oDoc.DO_Piece,
                        ScheduledDate = oDoc.DO_DateLivr,
                        SupplierName = oDoc.Fournisseur.CT_Intitule,
                        ReceiptLines = lignes
                    };

                    List_export.Add(oDoc, newRecept);
                    Au_moins_un_doc = true;

                }
                catch (Exception E)
                { Commun.Journalise("Export Achat", "Erreur lors de la lecture du document " + oDoc.DO_Piece + " : " + E.Message); }

            }


            if (Au_moins_un_doc == false)
            {
                // Annuler l'export / libération ressources
            }
            else
            {
                if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                {
                    Commun.Journalise("MODE SIMULATION", "écriture JSON dans fichier, -- " + FicNameJsonFake);
                }

                foreach (KeyValuePair<IBODocumentAchat3, Receipt> KVExport in List_export)
                {
                    IBODocumentAchat3 DocExport = KVExport.Key;
                    Receipt receptDoc = KVExport.Value;

                    // Réalisation de l'export
                    try
                    {
                        Commun.Journalise("Export Achat", "Export Document : " + DocExport.DO_Piece);

                        if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                        {
                            try
                            {
                                String jsondata = Newtonsoft.Json.JsonConvert.SerializeObject(receptDoc, Newtonsoft.Json.Formatting.Indented);
                                File.AppendAllText(FicNameJsonFake, jsondata);
                            }
                            catch (Exception E)
                            {
                                Commun.Journalise("Export Achat", "ERREUR écriture du fichier JSON Fake : " + E.Message, Fl_Alerte: !Core.Automate);
                                return;
                            }
                        }
                        else
                        {
                            // Réalisation de l'export
                            await Core.WebAPI.Post<Receipt>(Core.GetParametres.Futur_AchatsSend, receptDoc);
                        }

                        try
                        {
                            if (mode == 0)
                            {
                                // Mise à jour de l'horodatage sur le document SAGE
                                try
                                { DocExport.InfoLibre[libre_horo] = DateTime.Now; }
                                catch
                                { DocExport.InfoLibre[libre_horo] = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"); }
                                finally
                                {
                                    DocExport.Write();
                                }
                            }
                        }
                        catch (Exception Ex)
                        {

                            Commun.Journalise("Export Achat", "Erreur lors de la mise à jour Horodatage Document " + DocExport.DO_Ref + " : " + Ex.Message);
                        }
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("Export Achat", "Erreur lors de l'appel à l'API : " + E.Message);
                    }
                    finally
                    { DocExport.Read(); }
                }
            }
        }

        private List<ReceiptLine> Get_List_Line(IBODocumentAchat3 oDoc)
        {
            List<ReceiptLine> result = new List<ReceiptLine>();

            foreach (IBODocumentAchatLigne3 ligne in oDoc.FactoryDocumentLigne.List)
            {
                if (ligne.Article == null)
                    continue;

                if (ligne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeAucun)
                    continue;

                if (String.IsNullOrEmpty(Core.GetParametres.Futur_Depot) == false)
                    if (ligne.Depot.DE_Intitule != Core.GetParametres.Futur_Depot & ligne.Depot.DE_Code != Core.GetParametres.Futur_Depot)
                        continue;

                ReceiptLine receiptLine = new ReceiptLine
                {
                    ProductCode = ligne.Article.AR_Ref,
                    Quantity = (int)ligne.DL_Qte
                };
                result.Add(receiptLine);
            }
            return result;

        }
        private List<IBISouche> Get_List_Souche()
        {
            List<IBISouche> liste = new List<IBISouche>();
            try
            {
                if (Core.GetParametres.Futur_ListSoucheExport == "*" | String.IsNullOrEmpty(Core.GetParametres.Futur_ListSoucheExport) == true)
                {
                    foreach (IBISouche eltSouche in Core.baseSage.OM_baseCial.FactorySoucheAchat.List)
                        liste.Add(eltSouche);
                    return liste;
                }

                String[] list_txt = Core.GetParametres.Futur_ListSoucheExport.Split(new char[] { ';', ' ', ',' });

                foreach (String souche_txt in list_txt)
                    if (String.IsNullOrEmpty(souche_txt) == false)
                    {
                        if (Core.baseSage.OM_baseCial.FactorySoucheAchat.ExistIntitule(souche_txt) == false)
                        {
                            Commun.Journalise("Export Achat", "ERREUR, le paramètre Souche n'a pas été trouvé");
                            throw new Exception("ERREUR, le paramètre Souche n'a pas été trouvé");
                        }
                        else
                        {
                            liste.Add(Core.baseSage.OM_baseCial.FactorySoucheAchat.ReadIntitule(souche_txt));
                        }
                    }
            }
            catch (Exception E)
            {
                Commun.Journalise("Export_DOC", "Erreur lors de la création de liste SOUCHES.");
            }
            return liste;
        }
    }
}
