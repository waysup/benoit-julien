﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Helper;
using Objets100cLib;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Security.AccessControl;
using System.Security.Principal;
using Interface_SAGE.Core;
using Interface_SAGE.Entities;
using Interface_SAGE.Formats;
using Newtonsoft.Json;
using Interface_SAGE.Process;

namespace Interface_SAGE.Tasks
{

    public class Futur_AchatGet
    {
        private MainProgram Core;
        private Boolean Simulation;
        private String FicNameJsonFake;

        BSCIALApplication100c om_Cial;
        BSCPTAApplication100c om_Cpta;

        public Futur_AchatGet(MainProgram core, String JsonFake = "", Boolean simulation = false)
        {
            Core = core;
            Simulation = simulation;
            FicNameJsonFake = JsonFake;

            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }

        public async Task ImportAchat(int mode)
        {

            // connexion à l'API - récupération du TOKEN
            List<ReceiptReturn> ListDoc = null;
            try
            {
                if (Simulation == true & String.IsNullOrEmpty(FicNameJsonFake) == false)
                {
                    try
                    {
                        //ListDoc = Get_INIT_DATA();

                        //String j = Newtonsoft.Json.JsonConvert.SerializeObject(ListDoc, Newtonsoft.Json.Formatting.Indented);
                        //File.AppendAllText(FicNameJsonFake, j);
                        
                        Commun.Journalise("MODE SIMULATION", "lecture JSON dans fichier, -- " + FicNameJsonFake);

                        String jsondata = File.ReadAllText(FicNameJsonFake);
                        ListDoc = JsonConvert.DeserializeObject<List<ReceiptReturn>>(jsondata);
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("Import_Achat", "ERREUR lecture du fichier JSON Fake : " + E.Message, Fl_Alerte: !Core.Automate);
                        return;
                    }
                }
                else
                {
                    DateTime UTC = Core.GetParametres.lastImport_Recept.ToUniversalTime();
                    string strUTC = UTC.ToString("u");
                    String URL = Core.GetParametres.Futur_AchatGet.Replace("{dateFromUtc}", strUTC);

                    // Récupération des commandes par interrogation de l'API
                    ListDoc = await Core.WebAPI.Get<List<ReceiptReturn>>(URL);

                    Core.GetParametres.lastImport_Recept = DateTime.Now;
                }
                
            }
            catch (Exception E)
            {
                Commun.Journalise("Import Achat", "ERREUR lor de l'appel de l'API : " + E.Message, Fl_Alerte: !Core.Automate);
                return;
            }

            ListDoc.RemoveAll(x =>  x.StateCode != 4 & x.StateCode != 5);

            if (ListDoc.Count == 0)
                return;

            Commun.Journalise("Import Achat", "Liste de document à traiter : " + ListDoc.Count);

            foreach (ReceiptReturn wDoc in ListDoc)
            {
                String Msgerror = "";

                try
                {
                    if (Receipt_Treatment(wDoc, ref Msgerror) == false)
                    {
                        // Erreur traité dans finnaly
                    }
                }
                catch (Exception E)
                {
                    if (Simulation)
                    {
                        Msgerror = " Inattendue " + E.Message + Environment.NewLine + " / " + E.StackTrace + Environment.NewLine + " / " + E.TargetSite;
                        Msgerror += E.InnerException != null ? Environment.NewLine + E.InnerException.Message : "";
                    }
                    else
                    {
                        Msgerror = " Inattendue " + E.Message;
                    }
                }
                finally
                {
                    if (String.IsNullOrEmpty(Msgerror) == true)
                        Commun.Journalise("Intégration DOC", "Traitement livraison de la pièce avec succès N° " + wDoc.ReceiptNumber);
                    else
                        Commun.Journalise("Intégration DOC", "ERREUR à la pièce  " + wDoc.ReceiptNumber+ " : " + Msgerror, Fl_Alerte: !Core.Automate);
                }
            }
        }

        private Boolean Receipt_Treatment(ReceiptReturn wDoc, ref String MsgErr)
        {
            // Recherche du docuemnt
            if (om_Cial.FactoryDocumentAchat.ExistPiece(DocumentType.DocumentTypeAchatCommandeConf, wDoc.ReceiptNumber) == false)
            {
                MsgErr = "La commande " + wDoc.ReceiptNumber + " n'a pas été trouvée";
                return false;
            }

            IBODocumentAchat3 oDoc = om_Cial.FactoryDocumentAchat.ReadPiece(DocumentType.DocumentTypeAchatCommandeConf, wDoc.ReceiptNumber) as IBODocumentAchat3;
            var Tmp_LotSAGE = new Dictionary<IBOArticleDepotLot, double>();
            var Tmp_LotFutur = new List<ReceiptLinePartialReception>();
            IPMDocTransformer IProc = om_Cial.Transformation.Achat.CreateProcess_Receptionner();
            IProc.AddDocument(oDoc);

            try
            {
                foreach (IBODocumentAchatLigne3 oLigne in IProc.ListLignesATransformer)
                {
                    if (oLigne.Article == null | oLigne.Depot == null)
                        continue;

                    if (String.IsNullOrEmpty(Core.GetParametres.Futur_Depot) == false)
                        if (oLigne.Depot.DE_Intitule != Core.GetParametres.Futur_Depot & oLigne.Depot.DE_Code != Core.GetParametres.Futur_Depot)
                            continue;

                    Double Qte_ResteA_receptionner = oLigne.DL_QteBC;
                    IBODepot3 pDepot = oLigne.Depot;
                    IBOArticleDepot3 pArtDepot = oLigne.Article.FactoryArticleDepot.ReadDepot(pDepot);


                    foreach (ReceipReturnLine wligne in wDoc.ReceiptLines.Where(x => x.ProductCode == oLigne.Article.AR_Ref))
                    {

                        if (wligne.ReceiptLinePartialReceptions.Count == 0)
                        {
                            if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie | oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot)
                            {
                                // Not an error, Not a réception
                                //MsgErr += "L'article " + oLigne.Article.AR_Ref + " est pas suivi par n° lot ou série";
                            }
                            else
                            {
                                //Tmp_LotFutur.Add(batch);
                                Qte_ResteA_receptionner -= (double)(wligne.ReceivedQuantity??0);
                            }
                        }
                        else
                        foreach (ReceiptLinePartialReception batch in wligne.ReceiptLinePartialReceptions)
                        {
                            if (Tmp_LotFutur.Contains(batch))
                                continue;

                            if (Qte_ResteA_receptionner == 0)
                                continue;

                            if (String.IsNullOrEmpty(batch.ProductBatchNumber) == false)
                            {
                                //Test sur le suivi de stock de l'article
                                if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie)
                                {
                                    if (batch.ReceivedQuantity != 1)
                                    {
                                        MsgErr += "La quantité d'un N°série ne peut être supérieure à 1 : Article " + wligne.ProductCode;
                                    }
                                    else
                                    {
                                        IBOArticleDepotLot pArtDepotLot = pArtDepot.FactoryArticleDepotLot.Create() as IBOArticleDepotLot;
                                        if (pArtDepotLot.FactoryArticleDepotLot.ExistNoSerie(batch.ProductBatchNumber))
                                        {
                                            MsgErr += "Le N°série " + batch.ProductBatchNumber + "  existe déjà pour l'article " + wligne.ProductCode;
                                        }
                                        else
                                        {
                                            IUserLot pUserLot = IProc.UserLotsToUse[oLigne].AddNew();
                                            pArtDepotLot.NoSerie = batch.ProductBatchNumber;
                                            pUserLot.Set(pArtDepotLot, 1, pArtDepotLot.Complement);

                                            Tmp_LotFutur.Add(batch);
                                            Qte_ResteA_receptionner -= 1;
                                        }
                                    }
                                }
                                else if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot)
                                {
                                    IBOArticleDepotLot pArtDepotLot = pArtDepot.FactoryArticleDepotLot.Create() as IBOArticleDepotLot;
                                    IUserLot pUserLot = IProc.UserLotsToUse[oLigne].AddNew();
                                    pArtDepotLot.NoSerie = batch.ProductBatchNumber;
                                    pUserLot.Set(pArtDepotLot, (double)(batch.ReceivedQuantity ??0), pArtDepotLot.Complement);
                                
                                    Tmp_LotFutur.Add(batch);
                                    Qte_ResteA_receptionner -= (double)(batch.ReceivedQuantity??0);
                                }
                                else
                                {
                                    MsgErr += "L'article " + oLigne.Article.AR_Ref + " n'est pas suivi par n° lot ou série";
                                }  
                            }
                            else
                            {

                                    // PAs de batch si pas gérer en lot 
                                MsgErr += "ALERTE A SURVEILLER - L'article " + oLigne.Article.AR_Ref + " n'est pas suivi par n° lot ou série";
                                if (oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie | oLigne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot)
                                {
                                    MsgErr += "L'article " + oLigne.Article.AR_Ref + " est pas suivi par n° lot ou série";
                                }
                                else
                                {
                                    Tmp_LotFutur.Add(batch);
                                    Qte_ResteA_receptionner-= (double)batch.ReceivedQuantity;
                                }
                            }
                        }
                    }

                    if (Qte_ResteA_receptionner != 0)
                    {
                        oLigne.DL_QteBL = oLigne.DL_QteBC - Qte_ResteA_receptionner;      // Persistant dans process ????
                    }
                }
            }
            catch (Exception E)
            {
                MsgErr += "Erreur lors d'affectation des lots : " + E.Message;
                return false;
            }

            foreach (ReceipReturnLine wligne in wDoc.ReceiptLines)
                foreach (ReceiptLinePartialReception batch in wligne.ReceiptLinePartialReceptions)
                    if (Tmp_LotFutur.Contains(batch) == false)
                        MsgErr += " Erreur, le lot : " + batch.ProductBatchNumber + " n'a pas pu être affecté.";

            if (MsgErr != "")
                return false;



            // indisponibilité de stock normallement géré par le process
            /* foreach (IBODocumentVenteLigne3 pLig in IProc.ListLignesATransformer)
             {

                 if (IProc.UserLotsQteRestantAFournir[pLig] > 0)
                 {
                     if (IProc.UserLotsQteRestantAFournir[pLig] == pLig.DL_Qte)
                         IProc.RmvDocumentLigne(pLig);

                     pLig.DL_QtePL = pLig.DL_QtePL - IProc.UserLotsQteRestantAFournir[pLig];
                 }
             }*/
            try
            {
                VentesLivraison.ValideLivraisonProcess(IProc, false);
            }
            catch (Exception E)
                { MsgErr += E.Message; }
            return true;
        }

        private List<ReceiptReturn> Get_INIT_DATA()
        {
            List<ReceiptReturn> ListDoc = new List<ReceiptReturn>();
            var s = new ReceiptReturn();
            s.ReceiptDate = DateTime.Today;
            s.ReceiptNumber = "BL001";
            s.StateCode = 300;
            ReceipReturnLine l = new ReceipReturnLine();
            l.ProductCode = "aaa";
            l.ReceivedQuantity = 5;

            l.ReceiptLinePartialReceptions = new List<ReceiptLinePartialReception>();
            ReceiptLinePartialReception p = new ReceiptLinePartialReception();
            p.Date = DateTime.Now;
            p.ProductBatchNumber = "LOT1";
            p.ReceivedQuantity = 2;
            ReceiptLinePartialReception p2 = new ReceiptLinePartialReception();
            p2.Date = DateTime.Now;
            p2.ProductBatchNumber = "LOT1";
            p2.ReceivedQuantity = 2;
            l.ReceiptLinePartialReceptions.Add(p);
            l.ReceiptLinePartialReceptions.Add(p2);

            ReceipReturnLine l1 = new ReceipReturnLine();
            l1.ProductCode = "aaa";
            l1.ReceivedQuantity = 5;
            l1.ReceiptLinePartialReceptions = new List<ReceiptLinePartialReception>();
            ReceiptLinePartialReception p3 = new ReceiptLinePartialReception();
            p3.Date = DateTime.Now;
            p3.ProductBatchNumber = "LOT1";
            p3.ReceivedQuantity = 2;
            ReceiptLinePartialReception p4 = new ReceiptLinePartialReception();
            p4.Date = DateTime.Now;
            p4.ProductBatchNumber = "LOT1";
            p4.ReceivedQuantity = 2;
            l.ReceiptLinePartialReceptions.Add(p3);
            l.ReceiptLinePartialReceptions.Add(p4);
            s.ReceiptLines = new List<ReceipReturnLine>(){l,l1
            };
            ListDoc.Add(s);

            return ListDoc;
        }
    }
}
