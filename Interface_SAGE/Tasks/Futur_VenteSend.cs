﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Helper;
using Objets100cLib;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Security.AccessControl;
using System.Security.Principal;
using Interface_SAGE.Core;
using Interface_SAGE.Entities;
using Interface_SAGE.Formats;

namespace Interface_SAGE.Tasks
{

    public class Futur_VenteSend
    {
        private MainProgram Core;
        private Liste_Correspondance lst_conv;
        private DateTime Date_Export;
        private Boolean Verbose;
        private String FicNameJsonFake;

        BSCIALApplication100c om_Cial;
        BSCPTAApplication100c om_Cpta;

        public Futur_VenteSend(MainProgram core, String JsonFake = "", Boolean verbose = false)
        {
            Core = core;
            Date_Export = DateTime.Now;
            lst_conv = Liste_Correspondance.Get_Liste_Correspondance(core);
            Verbose = verbose;
            FicNameJsonFake = JsonFake;

            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }

        public async 
        Task
ExportDoc(int mode)
        {
            /*
             * Mode 0 = Normal
             * Mode 1 = Sans horodatage
             * Mode 2 = Exporté tout
             */

            // Chargement du critère SOUCHE
            List<IBISouche> Lst_souch = Get_List_Souche();

            // Parcours de la liste des documens actifs

            // RAPPEL DES STATUTS
            // 2 Commande en cours de traitement
            // 3 Commande traitée et en attente de livraison
            // 4 Processus de livraison enclenché
            // 5 Commande livrée
            //

            string libre_horo = Core.GetParametres.Futur_Libr_Horodatage;
            DocumentType DocType = DocumentType.DocumentTypeVenteCommande;

            Dictionary<IBODocumentVente3, Order> List_export = new Dictionary<IBODocumentVente3, Order>();
            Boolean Au_moins_un_doc = false;

            foreach (IBODocumentVente3 oDoc in om_Cial.FactoryDocumentVente.QueryTypePieceOrderPiece(DocType, "", "ZZZZZZ"))
            {
                // Test Origine commande
                if (String.IsNullOrEmpty(Core.GetParametres.WOO_DocLibr_Origine_commande) == false)
                {
                    if (oDoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Origine_commande] == "WooCommerce")
                    {
                        // commande Woocommerce
                    }
                    if (oDoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Origine_commande] == "EDI")
                    {
                        // commande EDI
                    }
                    if (oDoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Origine_commande] == "")
                    {
                        // Autre commande , A définir si envoi
                    }

                    // test sur la souche docuemnt
                    if (Lst_souch.Contains(oDoc.Souche) == false)
                        continue;

                    if (Core.GetParametres.Futur_Exportconfirme == true)
                    {
                        if (oDoc.DO_Statut != DocumentStatutType.DocumentStatutTypeAPrepare)
                            continue;
                    }

                    // Mode simulation, ou ré-exportation totale
                    if (mode != 2)
                    {
                        // Test de l'horodatage
                        if (oDoc.InfoLibre[libre_horo] != null &
                            oDoc.InfoLibre[libre_horo] != new DateTime(1899, 12, 30) &
                            oDoc.InfoLibre[libre_horo] != new DateTime(1753, 01, 01))
                            continue;
                    }

                    try
                    {

                        List<OrderLine> lignes = Get_List_Line(oDoc);
                        if (lignes.Count == 0)
                            continue;


                        String CurrencyCode = "EUR";
                        foreach (IBPDossierCial DosParam in om_Cial.FactoryDossier.List)
                        {
                            IBPDevise2 devise = DosParam.DeviseCompte;
                            if (devise != null)
                                CurrencyCode = devise.D_CodeISO;
                            break;
                        }

                        // Réservation du document
                        try
                        { oDoc.CouldModified(); }
                        catch
                        {
                            Commun.Journalise("Export Document", "Le document " + oDoc.DO_Piece + " est ouvert, il ne pourra pas être exporté.");
                            continue;
                        }

                        // Ajout du document à la liste à exporter
                        Order newExp = new Order
                        {
                            BrandCode = Core.GetParametres.Futur_APIMerchantcode,
                            CurrencyCode = CurrencyCode,
                            CustomerNumber = oDoc.DO_Ref,
                            DateUtc = oDoc.DO_Date.ToUniversalTime(),
                            Language = "FR",
                            //MerchantCarrierCode
                            MerchantCarrierLabel = oDoc.Expedition.E_Intitule,
                            OrderNumber = oDoc.DO_Piece,
                            PickerComments = "",
                            ScheduledTransmissionDate = null,
                            //ShippingServiceCode
                            Address = new Address
                            {
                                Address1 = oDoc.LieuLivraison.LI_Intitule,
                                Address2 = oDoc.LieuLivraison.Adresse.Adresse,
                                Address3 = oDoc.LieuLivraison.Adresse.Complement,
                                City = oDoc.LieuLivraison.Adresse.Ville,
                                Comments = oDoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Comment_LIV],
                                CorporateName = oDoc.Client.CT_Intitule,
                                CountryCode = om_Cpta.FactoryPays.ExistIntitule(oDoc.LieuLivraison.Adresse.Pays) ?
                                                ((IBOPays)om_Cpta.FactoryPays.ReadIntitule(oDoc.LieuLivraison.Adresse.Pays)).CodeISO2 :
                                                "FR",
                                Email = oDoc.LieuLivraison.Telecom.EMail != "" ?
                                        oDoc.LieuLivraison.Telecom.EMail : oDoc.Client.Telecom.EMail,
                                FirstName = oDoc.Client.CT_Intitule,
                                LastName = "",
                                MobilePhone = oDoc.LieuLivraison.Telecom.Portable != "" ?
                                              oDoc.LieuLivraison.Telecom.Portable :
                                              oDoc.Client.Telecom.Portable,
                                Phone = oDoc.LieuLivraison.Telecom.Telephone != "" ?
                                        oDoc.LieuLivraison.Telecom.Telephone :
                                        oDoc.Client.Telecom.Telephone,
                                PickupPointNumber = "",
                                ProvinceCode = oDoc.LieuLivraison.Adresse.CodeRegion,
                                ZipCode = oDoc.LieuLivraison.Adresse.CodePostal

                            },
                            Billing = new Billing
                            {
                                Address1 = oDoc.LieuLivraison.LI_Intitule,
                                Address2 = oDoc.LieuLivraison.Adresse.Adresse,
                                Address3 = oDoc.LieuLivraison.Adresse.Complement,
                                BillNumber = "",
                                City = oDoc.LieuLivraison.Adresse.Ville,
                                CorporateName = oDoc.Client.CT_Intitule,
                                CountryCode = om_Cpta.FactoryPays.ExistIntitule(oDoc.LieuLivraison.Adresse.Pays) ?
                                                ((IBOPays)om_Cpta.FactoryPays.ReadIntitule(oDoc.LieuLivraison.Adresse.Pays)).CodeISO2 :
                                                "FR",
                                // Discount 
                                // DiscountHT  
                                //  DiscountTTC 
                                FirstName = oDoc.Client.CT_Intitule,
                                LastName = "",
                                ProvinceCode = oDoc.LieuLivraison.Adresse.CodeRegion,
                                ShipmentPrice = (Decimal?)oDoc.FraisExpedition,
                                ShipmentVAT = (Decimal?)Get_Taux_Transport(oDoc),
                                TotalAmount = (Decimal?)oDoc.Valorisation.TotalTTC,
                                ZipCode = oDoc.LieuLivraison.Adresse.CodePostal

                            },
                            //Gift = new Gift (),
                            OrderLines = lignes

                        };
                        List_export.Add(oDoc, newExp);
                        Au_moins_un_doc = true;

                    }
                    catch (Exception E)
                    { Commun.Journalise("Export Document", "Erreur lors de la lecture du document " + oDoc.DO_Piece + " : " + E.Message); }

                }
            }


            if (Au_moins_un_doc == false)
            {
                // Annuler l'export / libération ressources
            }
            else
            {
                if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                {
                    Commun.Journalise("MODE SIMULATION", "écriture JSON dans fichier, -- " + FicNameJsonFake);
                }

                foreach (KeyValuePair<IBODocumentVente3, Order> KVExport in List_export)
                {
                    IBODocumentVente3 DocExport = KVExport.Key;
                    Order orderDoc = KVExport.Value;

                    // Réalisation de l'export
                    try
                    {
                        Commun.Journalise("Export document", "Export Document : " + DocExport.DO_Piece);

                        if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                        {
                            try
                            {
                                String jsondata = Newtonsoft.Json.JsonConvert.SerializeObject(orderDoc, Newtonsoft.Json.Formatting.Indented);
                                File.AppendAllText(FicNameJsonFake, jsondata);
                            }
                            catch (Exception E)
                            {
                                Commun.Journalise("Export statut DOC", "ERREUR écriture du fichier JSON Fake : " + E.Message, Fl_Alerte: !Core.Automate);
                                return;
                            }
                        }
                        else
                        {
                            // Réalisation de l'export
                            await Core.WebAPI.Post<Order>(Core.GetParametres.Futur_VentesSend, orderDoc);
                        }

                        try
                        {
                            if (mode == 0)
                            {
                                // Mise à jour de l'horodatage sur le document SAGE
                                try
                                { DocExport.InfoLibre[libre_horo] = DateTime.Now; }
                                catch
                                { DocExport.InfoLibre[libre_horo] = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"); }
                                finally
                                {
                                    DocExport.Write();
                                }
                            }
                        }
                        catch (Exception Ex)
                        {

                            Commun.Journalise("Export Statut Document", "Erreur lors de la mise à jour Horodatage Document " + DocExport.DO_Ref + " : " + Ex.Message);
                        }
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("Export Statut Document", "Erreur lors de l'appel à l'API : " + E.Message);
                    }
                    finally
                    { DocExport.Read(); }
                }
            }
        }

        private List<OrderLine> Get_List_Line(IBODocumentVente3 oDoc)
        {
            List<OrderLine> result = new List<OrderLine>();

            foreach (IBODocumentVenteLigne3 ligne in oDoc.FactoryDocumentLigne.List)
            {
                if (ligne.Article == null)
                    continue;

                if (ligne.Article.AR_SuiviStock == SuiviStockType.SuiviStockTypeAucun && ligne.IsComposeDeCompose == false)
                    continue;

                if (Core.GetParametres.Futur_Exportcomposants == false)
                    if (ligne.ArticleCompose != null && ligne.IsComposeDeCompose == false)
                        continue;

                if (String.IsNullOrEmpty(Core.GetParametres.Futur_Depot) == false && ligne.IsComposeDeCompose == false)
                    if (ligne.Depot.DE_Intitule != Core.GetParametres.Futur_Depot & ligne.Depot.DE_Code != Core.GetParametres.Futur_Depot)
                        continue;

                OrderLine orderLine = new OrderLine
                {
                    ProductCode = ligne.Article.AR_Ref,
                    ProductLabel = ligne.DL_Design,
                    Quantity = (int)ligne.DL_Qte,
                    UnitPrice = ligne.Valorisee ? (decimal?)ligne.Valorisation.PU_Net : 0,
                    UnitPriceHT = ligne.Valorisee ? (decimal?)ligne.Valorisation.PU_Net : 0,
                    UnitPriceTTC = ligne.Valorisee ? (decimal?)(ligne.Valorisation.TTC * ( 1 + (ligne.Taxe[0] != null ? ligne.Taxe[0].TA_Taux : 0))) : 0,
                    VATRate = (decimal?)(ligne.Taxe[0] != null ? ligne.Taxe[0].TA_Taux : 0)
                };
                result.Add(orderLine);
            }
            return result;

        }
        private List<IBISouche> Get_List_Souche()
        {
            List<IBISouche> liste = new List<IBISouche>();
            try
            {
                if (Core.GetParametres.Futur_ListSoucheExport == "*" | String.IsNullOrEmpty(Core.GetParametres.Futur_ListSoucheExport) == true)
                {
                    foreach (IBISouche eltSouche in Core.baseSage.OM_baseCial.FactorySoucheVente.List)
                        liste.Add(eltSouche);
                    return liste;
                }

                String[] list_txt = Core.GetParametres.Futur_ListSoucheExport.Split(new char[] { ';', ' ', ',' });

                foreach (String souche_txt in list_txt)
                    if (String.IsNullOrEmpty(souche_txt) == false)
                    {
                        if (Core.baseSage.OM_baseCial.FactorySoucheVente.ExistIntitule(souche_txt) == false)
                        {
                            Commun.Journalise("Export Document", "ERREUR, le paramètre Souche n'a pas été trouvé");
                            throw new Exception("ERREUR, le paramètre Souche n'a pas été trouvé");
                        }
                        else
                        {
                            liste.Add(Core.baseSage.OM_baseCial.FactorySoucheVente.ReadIntitule(souche_txt));
                        }
                    }
            }
            catch (Exception E)
            {
                Commun.Journalise("Export_DOC", "Erreur lors de la création de liste SOUCHES.");
            }
            return liste;
        }

        private Double Get_Taux_Transport(IBODocumentVente3 Doc)
        {
            if (Doc.Expedition == null)
                return 0;
            if (Doc.Expedition.ArticleFraisExpedition == null)
                return 0;

            IBICategorieCompta catcpa = Doc.CategorieCompta;
            IBOArticle3 Art_trans = Doc.Expedition.ArticleFraisExpedition;
            IBOTaxe3 taxe = null;
            if (Art_trans.FactoryArticleParamCompta.ExistCategorieCompta(catcpa))
            {
                IBOArticleParamCompta3 paramArt = Art_trans.FactoryArticleParamCompta.ReadCategorieCompta(catcpa);
                for (short i = 1; i <= 3; i++)
                {
                    if (paramArt.Taxe[i] != null)
                    {
                        taxe = paramArt.Taxe[i];
                        break;
                    }
                }
            }
            if (taxe == null)
            {
                foreach (IBOFamilleParamCompta3 paramFam in Art_trans.Famille.FactoryFamilleParamCompta.List)
                {
                    if (paramFam.CategorieCompta == catcpa)
                    {
                        for (short i = 1; i<= 3; i++)
                        {
                            if (paramFam.Taxe[i] != null)
                            {
                                taxe = paramFam.Taxe[i];
                                break;
                            }
                        }
                    }
                }
            }
            if (taxe == null)
                return 0;
            else
                return taxe.TA_Taux;
        }
    }
}
