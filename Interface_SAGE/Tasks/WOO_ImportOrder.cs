﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interface_SAGE.Helper;
using Interface_SAGE.Formats;
using Objets100cLib;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using CsvHelper.Configuration;
using CsvHelper;
using System.Text;
using Interface_SAGE.Core;
using Interface_SAGE.Process;
using Interface_SAGE.Entities;
using System.Windows.Forms;
using System.Diagnostics;

namespace Interface_SAGE.Tasks
{
    public class WOO_ImportOrder
    {
        private MainProgram Core;
        private Liste_Correspondance lst_conv;
        private Boolean simulation;

        private Char Sep;
        private Char Del;

        private BSCIALApplication100c om_Cial;
        private BSCPTAApplication100c om_Cpta;

        public WOO_ImportOrder(MainProgram core, Boolean Simulation = false)
        {
            Core = core;
            simulation = Simulation;

            // Format colonnes
            lst_conv = Liste_Correspondance.Get_Liste_Correspondance(Core);

            // Base Sage 
            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }

        public void Recup_File_FTP()
        {
            if (String.IsNullOrEmpty(Core.GetParametres.FTPUri) == true)
                return;
           

            try
            {
            /*
                DirectoryInfo temp = new DirectoryInfo(@"c:\temp");
                temp.Create();
                temp.CreateSubdirectory("ftp");

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = Interface_SAGE.Properties.Settings.Default.WinSCP_EXE;
                startInfo.Arguments = @" / log=""C:\temp\WinSCP.log"" /ini=nul /command ""open ftp://waysup:***@78.41.55.50/"" ""get -delete /files/GAYA/ORDER/*.csv c:\temp\ftp\""  ""exit""";

                try
                {
                    using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
                        exeProcess.WaitForExit(20000);
                }
                catch (Exception E)
                {
                    Commun.Journalise("Import_Document", "Erreur Exécution scritp FTP");
                }

                DirectoryInfo pathDir = new DirectoryInfo(Core.GetParametres.WOO_WorkDir);
                foreach (FileInfo f in new DirectoryInfo("c:\\temp\\ftp").EnumerateFiles("*"))
                {
                    String dest = f.FullName.Replace(f.DirectoryName, pathDir.FullName);
                    f.MoveTo(dest);
                }
            */

                 FTP ftp = new FTP(Core);
                String local = Core.GetParametres.WOO_WorkDir.Last() == '\\' ? Core.GetParametres.WOO_WorkDir : Core.GetParametres.WOO_WorkDir + '\\';
                String remote = Core.GetParametres.FTPPathOrder + (String.IsNullOrEmpty(Core.GetParametres.WOO_DocFicNomORDER) == false ? Core.GetParametres.WOO_DocFicNomORDER : "*.csv");
                var list = ftp.Get_Files(local, remote, true);
                Commun.Journalise("Import_Document", "Appel FTP, Récupération de " + list.Count + " fichiers");
            }
            catch (Exception E)
            {
                Commun.Journalise("Import_Document", "Erreur de communication FTP : " + E.Message);
            }

        }
        public void Import_Document()
        {
            // Gestion des sous répertoires
            DirectoryInfo pathDir = new DirectoryInfo(Core.GetParametres.WOO_WorkDir);
            if (pathDir.Exists == false)
            {
                Commun.Journalise("Import Document", "Répertoire de travail inexistant ou innaccessible, ABANDON traitement");
                return;
            }

            // Création des répertoires de destination
            DirectoryInfo ErreurDir = pathDir.CreateSubdirectory("EN ERREUR");
            DirectoryInfo TraiteDir = pathDir.CreateSubdirectory("TRAITES");

            // Charactères du format de fichier
            Sep = Commun.Get_Char(Core.GetParametres.WOO_DataSep);
            Del = Commun.Get_Char(Core.GetParametres.WOO_DataDel);
            
            foreach (FileInfo FicImport in pathDir.EnumerateFiles(String.IsNullOrEmpty(Core.GetParametres.WOO_DocFicNomORDER) == false ? Core.GetParametres.WOO_DocFicNomORDER : "*.*"))
            {
                if (FicImport.Extension.ToUpper() == ".CSV" | FicImport.Extension.ToUpper() == ".TXT")
                {
                    SortedList<String, List<WOO_Doc>> Liste_Import = new SortedList<String, List<WOO_Doc>>();
                    List<String> List_Ref_Err = new List<String>();

                    Commun.Journalise("Import Document", "  ----  Traitement du fichier : " + FicImport.FullName + "  ----");

                    Int32 nb_ligne = 0;
                    Int32 nb_erreur = 0;
                    try
                    {
                        bool premiere_ligne = true;
                        bool traiter_ligne = true;

                        /// Lecture du fichier ligne à ligne 
                        using (StreamReader SR = FicImport.OpenText())
                        {
                            while (SR.EndOfStream == false)
                            {
                                // Lecture de la ligne en cours
                                String rec = SR.ReadLine();

                                // NE pas tenir compte de la première ligne ?
                                traiter_ligne = true;
                                if (premiere_ligne == true)
                                {
                                    premiere_ligne = false;
                                    if (Core.GetParametres.WOO_Ligne_entete == true)
                                        traiter_ligne = false;
                                }

                                if (traiter_ligne == true)
                                {
                                    nb_ligne++;
                                    // Passage de la ligne en tableau de données
                                    String[] data = rec.Split(Sep);

                                    // Suppression délimiteur
                                    for (int indice = 0; indice < data.Length; indice++)
                                    {
                                        if (data[indice].Length >= 2 & data[indice].FirstOrDefault() == Del & data[indice].LastOrDefault() == Del)
                                            data[indice] = data[indice].Substring(1, data[indice].Length - 2);
                                    }

                                    WOO_Doc wDoc = WOO_Doc.Get_FromCSV(data);

                                    if (tester_ligne(wDoc, nb_ligne, out String newRef) == false)
                                    {
                                        nb_erreur++;
                                        List_Ref_Err.Add(newRef);
                                    }
                                    else
                                    {
                                        if (List_Ref_Err.Contains(newRef) == false)
                                        {
                                            if (Liste_Import.TryGetValue(newRef, out List<WOO_Doc> ExistListe) == true)
                                                ExistListe.Add(wDoc);
                                            else
                                            {
                                                List<WOO_Doc> NewListe = new List<WOO_Doc>();
                                                NewListe.Add(wDoc);
                                                Liste_Import.Add(newRef, NewListe);
                                            }
                                        }
                                    }
                                }
                            }
                            // Fin de lecture du fichier
                            SR.Close();

                            // Nettoyage des lignes OK précédantes des lignes KO dans le processus de test
                            foreach (String Ref_erreur in List_Ref_Err)
                                if (Liste_Import.ContainsKey(Ref_erreur) == true)
                                {
                                    Liste_Import.Remove(Ref_erreur);
                                    Commun.Journalise("Import Document", "Erreur Document " + Ref_erreur + " en erreur, il ne sera pas créé");
                                }

                            /***
                             * si une erreur bloque tout l'import, chnager pour cette condtion (nb_erreur == 0)
                             */
                            if (Liste_Import.Count > 0)
                            {
                                foreach (KeyValuePair<string, List<WOO_Doc>> KV in Liste_Import)
                                    Traitement_liste_import(KV.Value, KV.Key);
                            }

                            Commun.Journalise("Import Document", " Nombre de ligne traitées : " + nb_ligne);
                            Commun.Journalise("Import Document", " Nombre de lignes en erreur : " + nb_erreur);

                        }
                    }
                    catch (System.Exception E)
                    {
                        nb_erreur++;
                        Commun.Journalise("Import Document", "Erreur inattendue lors de la lecture du fichier : " + E.Message + "\n Trace :" + E.StackTrace);
                    }
                    finally
                    {
                        try
                        {
                            String destpath = "";
                            if (nb_erreur > 0)
                                destpath = ErreurDir.FullName;
                            else
                                destpath = TraiteDir.FullName;

                            String destfic = FicImport.FullName.Replace(FicImport.DirectoryName, destpath);
                            FicImport.MoveTo(destfic);
                        }
                        catch (Exception E)
                        {
                            Commun.Journalise("Import Document", "Erreur lors de de l'archivage du ficher : " + E.Message);
                        }
                    }
                }
            } 
        }
    
        private Boolean tester_ligne(WOO_Doc wDoc, int numligne, out String Ref_Cde)
        {
            // troncature de la référence pour les recherches ET l'affectation
            Ref_Cde = wDoc.Cde_Numero;
            Ref_Cde = Ref_Cde.Substring(0, Math.Min(17, Ref_Cde.Length));

            try
            {
                 String ct_num = Core.baseSage.Get_Ctnum_By_IdExterne(wDoc.Client_ID, Core.GetParametres.WOO_ClientLibr_ID);
                if (ct_num != "")
                {
                    IBOClient3 test_omClient = om_Cpta.FactoryClient.ReadNumero(ct_num);
                    foreach (IBODocumentVente3 eltDoc in om_Cial.FactoryDocumentVente.QueryTiersOrderDate(test_omClient))
 
                        if (eltDoc.DO_Ref == Ref_Cde)
                        {
                            Commun.Journalise("Import Document - Test Ligne", "Le document " + Ref_Cde + " existe déjà pour ce client, création impossible.");
                            return false;
                        }
                }
                else
                    ct_num = Creation_nouveau_client(wDoc, numligne);
                
                if (Modification_client(ct_num, wDoc, numligne) == false)
                    return false;

                // Date Commande
                DateTime Date_Cde = DateTime.Now; ;
                if (DateTime.TryParseExact(wDoc.Cde_Date, new string[] { "dd/MM/yyyy", "dd-MM-yyyy", "ddMMyy", "yyyyMMdd" }, CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime date) == false)
                {
                    Commun.Journalise("Import Document - Test Ligne", "ERREUR ligne " + numligne.ToString() + " format de date incorrect.");
                    return false;
                }

    
                // Livraison
                /*  A valider sinon "Titre de la méthode de livraison" */
                if (String.IsNullOrEmpty(wDoc.Cde_LivraisonCode) == false)
                {
                    if (om_Cial.FactoryExpedition.ExistIntitule(wDoc.Cde_LivraisonCode) == false)
                    {
                        Commun.Journalise("Import Document - Test Ligne", "Le mode d'expédition " + wDoc.Cde_LivraisonCode + " est incorrect.");
                        return false;
                    }
                }


                // Test référence 
                IBOArticle3 omArt = null;
                if (String.IsNullOrEmpty(wDoc.Ligne_Article) == false)
                {
                    if (om_Cial.FactoryArticle.ExistReference(wDoc.Ligne_Article) == false)
                    {
                        Commun.Journalise("Import Document - Test Ligne", "La référence article " + wDoc.Ligne_Article + " non trouvée.");
                        return false;
                    }
                    omArt = om_Cial.FactoryArticle.ReadReference(wDoc.Ligne_Article);
                }
                else
                {
                    Commun.Journalise("Import Document - Test Ligne", "La référence article est obligatoire.");
                    return false;
                }

                // test Prix unitaire
                if (String.IsNullOrEmpty(wDoc.Ligne_PUHT) == false & Double.TryParse(wDoc.Ligne_PUHT, out Double PU) == false)
                {
                    Commun.Journalise("Import Document - Test Ligne", "Format du prix unitaire incorrect.");
                    return false;
                }

                // test quantité
                if (String.IsNullOrEmpty(wDoc.Ligne_qte) == false & Double.TryParse(wDoc.Ligne_qte, out Double Qte) == false)
                {
                    Commun.Journalise("Import Document - Test Ligne", "Format de la quantité incorrect.");
                    return false;
                }


                // recherche du taux de TVA
                Double Taux = 0;
                if (String.IsNullOrEmpty(wDoc.Ligne_Taux) == false)
                {
                    IBICategorieCompta categorieCpta_client = om_Cpta.FactoryClient.ReadNumero(ct_num).CategorieCompta;
                    if (Double.TryParse(wDoc.Ligne_Taux, out Taux) == false)
                    {
                        Commun.Journalise("Import Document - Test Ligne", "Format du taux de TVA incorrect.");
                        return false;
                    }
                    /* ... INFORMATION ONLY ... */
                    IBOTaxe3 Art_Taxe1 = null;
                    foreach (IBPCategorieComptaVente NCat in om_Cial.FactoryCategorieComptaVente.List)
                        if (NCat.Domaine == DomaineType.DomaineTypeVente & NCat == categorieCpta_client)
                        {
                            if (omArt.FactoryArticleParamCompta.ExistCategorieCompta(NCat))
                            {
                                IBOArticleParamCompta3 ParamComptaArt = omArt.FactoryArticleParamCompta.ReadCategorieCompta(NCat);
                                if (ParamComptaArt.Taxe[1] != null)
                                {
                                    Art_Taxe1 = ParamComptaArt.Taxe[1];
                                    break;
                                }
                                else
                                    break;
                            }
                            else
                                foreach (IBOFamilleParamCompta3 ParamComptaFam in omArt.Famille.FactoryFamilleParamCompta.List)
                                    if (ParamComptaFam.CategorieCompta == NCat)
                                    {
                                        Art_Taxe1 = ParamComptaFam.Taxe[1];
                                        break;
                                    }      
                        }

                    if (Art_Taxe1 == null)
                    {
                        Commun.Journalise("Import Document - Test Ligne", "Taux de TVA non paramétré - Article " + wDoc.Ligne_Article + " ligne " + numligne);
                        //return false;
                    }
                    else if (Art_Taxe1.TA_Taux != Taux & (Art_Taxe1.TA_Taux - Taux) > 0.02) // BUG in file with rounded VAT calculated
                    {
                        Commun.Journalise("Import Document - Test Ligne", "Taux de TVA applicable différent : " + Art_Taxe1.TA_Taux.ToString() + " - Article " + wDoc.Ligne_Article + " ligne " + numligne);
                        // NOT AN ERROR   return false;
                    }
                }

                //  Mode_Paiement 
                if (String.IsNullOrEmpty(wDoc.Mode_Paiement) == false)
                {
                    if (om_Cpta.FactoryReglement.ExistIntitule(wDoc.Mode_Paiement) == false)
                    {
                        Commun.Journalise("Import Document - Test Ligne", "Le Modele de Reglement " + wDoc.Mode_Paiement + " non trouvé.");
                        return false;
                    }
                }

                // test Prix transport
                if (String.IsNullOrEmpty(wDoc.Cde_LivraisonTTC) == false & Double.TryParse(wDoc.Cde_LivraisonTTC, out Double PxTrans) == false)
                {
                    Commun.Journalise("Import Document - Test Ligne", "Format du total TTC incorrect.");
                    return false;
                }

                // test  remise pied
                if (String.IsNullOrEmpty(wDoc.Cde_Remise) == false & Double.TryParse(wDoc.Cde_Remise, out Double RemisePied) == false)
                {
                    Commun.Journalise("Import Document - Test Ligne", "Format de la remise en pied incorrect.");
                    return false;
                }

            }
            catch (Exception E)
            {
                Commun.Journalise("Import Document - Test Ligne", "Erreur inattendue : " + E.Message);
                return false;
            }
            return true;
        }

        private void Traitement_liste_import(List<WOO_Doc> Liste_Ligne, String Ref_Cde)
        {
            oDoc newDoc = new oDoc();

            // Récupération première ligne pour générer l'entête
            WOO_Doc data_ent = Liste_Ligne.FirstOrDefault<WOO_Doc>();

            String Id_Client = data_ent.Client_ID;
            try
            {
                // chargement du client
                IBOClient3 trt_omClient = Core.baseSage.Get_ClientBy_IdExterne(Id_Client, Core.GetParametres.WOO_ClientLibr_ID);

                // Date Commande
                DateTime Date_Cde = DateTime.ParseExact(data_ent.Cde_Date, new string[] { "dd/MM/yyyy", "dd-MM-yyyy", "ddMMyy", "yyyyMMdd" }, CultureInfo.CurrentCulture, DateTimeStyles.None);

              
                // création d'un process
                PieceCommerciale PCom = new PieceCommerciale(om_Cial, DocumentType.DocumentTypeVenteCommande);
                newDoc.Doc = PCom.CreePieceVente(trt_omClient.CT_Num,
                                            Date_Cde,
                                            refPiece: Ref_Cde,
                                            Souche: Core.GetParametres.WOO_DocSouche
                                            );


                

                // Adresse de livraison
                if (String.IsNullOrEmpty(data_ent.Liv_Nom) == false)
                {
                    newDoc.Doc.LieuLivraison = Core.baseSage.GetAdresse(trt_omClient, data_ent.Liv_Nom);
                }

                if (String.IsNullOrEmpty(data_ent.Cde_LivraisonCode) == false)
                {
                    if (om_Cial.FactoryExpedition.ExistIntitule(data_ent.Cde_LivraisonCode) == true)
                        newDoc.Doc.Expedition = om_Cial.FactoryExpedition.ReadIntitule(data_ent.Cde_LivraisonCode);
                }

                // Gestion des lignes documents
                foreach (WOO_Doc data_Ligne in Liste_Ligne)
                {
                    String Ref_Article = data_Ligne.Ligne_Article;
                    Double PU = Double.Parse(data_Ligne.Ligne_PUHT);
                    Double Qte = Double.Parse(data_Ligne.Ligne_qte);
                    Double Remise = Double.Parse(data_Ligne.Ligne_ReducMont);

                    //gestion des prix à 0 ou cadeau
                    IBODocumentVenteLigne3 oLigne = null;
                    // création de la ligne dans le process
                    if ( Core.GetParametres.GratuitRemise == false)
                        oLigne = PCom.AjouteLigneSpeciale(Ref_Article, Qte, remiseU: Remise, PUht: PU, IsRemiseExceptionnelle: PU == 0 ? true : false);
                    else
                        oLigne = PCom.AjouteLigneSpeciale(Ref_Article, Qte, remiseU: Remise, PUht: PU);
                       

                    // Désignation
                    if (String.IsNullOrEmpty(data_Ligne.Ligne_Design) == false)
                    {
                        oLigne.DL_Design = data_Ligne.Ligne_Design.Substring(0, Math.Min(data_Ligne.Ligne_Design.Length, 69));
                    }
                    // Pas de recherche du taux de TVA
                    // Une ALERTE doit être envoyé à l'utilisateur

                }

                // test Prix transport
                if (String.IsNullOrEmpty(data_ent.Cde_LivraisonHT) == false)
                    newDoc.Doc.FraisExpedition = Double.Parse(data_ent.Cde_LivraisonHT);

                newDoc.Doc.Write();
                // Validation process
                try
                {
                    IBODocument3 odoc = PCom.ValideDocCommerciale(false);

                    // champ libre DOC 1
                    if (String.IsNullOrEmpty(Core.GetParametres.WOO_DocLibr_Origine_commande) == false)
                        odoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Origine_commande] = "WooCommerce";

                    // champ libre DOC 2
                    if (String.IsNullOrEmpty(Core.GetParametres.WOO_DocLibr_Comment_LIV) == false)
                        if (String.IsNullOrEmpty(data_ent.Client_Note) == false)
                            odoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Comment_LIV] = data_ent.Client_Note;

                    odoc.Write();

                    Boolean Act_a_creer  = false;
                    // Controle TTC pour passage statut confirmé
                    try
                    {
                        IBODocumentVente3 odocPersist = om_Cial.FactoryDocumentVente.ReadPiece(odoc.DO_Type, odoc.DO_Piece);
                        if (odocPersist.Valorisation.TotalTTC != Double.Parse(data_ent.Cde_MontantTTC))
                            Commun.Journalise("Intégration documents", "Ecart de TTC détectée, : " + odocPersist.DO_Piece + " Client " + odocPersist.Client.CT_Num);
                        else
                        {
                            odocPersist.DO_Statut = DocumentStatutType.DocumentStatutTypeAPrepare;
                            odocPersist.Write();
                            odocPersist.Read();
                            Act_a_creer = true;
                        }
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("Intégration documents", "Erreur lors du contrôle final de valeur : " + E.Message);
                    }


                    // Création de l'Acompte sur document persistant
                    if ( String.IsNullOrEmpty(data_ent.Cde_Transac) == false)
                    {
                        if (simulation == false & Act_a_creer == true)
                        {
                            IBODocumentVente3 odocPersist = om_Cial.FactoryDocumentVente.ReadPiece(odoc.DO_Type, odoc.DO_Piece);
                            IBODocumentAcompte3 oAcpt = odocPersist.FactoryDocumentAcompte.Create() as IBODocumentAcompte3;
                            oAcpt.DR_Date = odocPersist.DO_Date;
                            oAcpt.DR_Libelle = "Transaction " + odoc.DO_Ref;
                            oAcpt.DR_Montant = Double.Parse(data_ent.Cde_MontantTTC); // odocPersist.DO_TotalTTC;

                            if (om_Cpta.FactoryReglement.ExistIntitule(data_ent.Mode_Paiement) == true)
                                oAcpt.Reglement = om_Cpta.FactoryReglement.ReadIntitule(data_ent.Mode_Paiement);

                            oAcpt.SetDefault();
                            oAcpt.WriteDefault();
                        }
                        else
                            Commun.Journalise("Intégration documents", "Acompte existant non créé  " + Ref_Cde );
                        
                    }
                }
                catch (Exception E)
                {
                    Commun.Journalise("Intégration documents", "ERREUR d'intégration : " + Ref_Cde + " : " + E.Message);
                    return;
                }
            }
            catch (Exception E)
            { throw E; }

        }


        private String Creation_nouveau_client(WOO_Doc data, int num_ligne)
        {
            IBOClient3 Ins_omclient;

            String Nom_Client = data.Client_Nom;
            try
            {
                // Creation du nouveau client
                Ins_omclient = om_Cpta.FactoryClient.Create() as IBOClient3;

                String code_client = "";
                // Numérotation automatique
                code_client = om_Cpta.FactoryTiersType.ReadTypeTiers(TiersType.TiersTypeClient).NextCT_Num;

                if (String.IsNullOrEmpty(code_client) == true)
                {
                    // Numerotation manuelle
                    code_client = Nom_Client;

                    string pattern = @"[^a-zA-Z]";
                    var rex = new Regex(pattern);
                    code_client = rex.Replace(code_client, "").ToUpper();

                    string prefix_cod = Core.GetParametres.PrefixClient;
                    code_client = prefix_cod + code_client.Substring(0, Math.Min(17 - prefix_cod.Length, code_client.Length));

                    // TODO Problème si client existe déjà !!!
                }

                Ins_omclient.CT_Num = code_client;
                Ins_omclient.CT_Intitule = data.Client_Nom.Substring(0, Math.Min(35, data.Client_Nom.Length));

                Ins_omclient.SetDefault();
                Ins_omclient.WriteDefault();

                Ins_omclient.InfoLibre[Core.GetParametres.WOO_ClientLibr_ID] = data.Client_ID;
                Ins_omclient.Write();
                Ins_omclient.Read();

                Commun.Journalise("Création Nouveau client", " Création code client -> " + Ins_omclient.CT_Num + " - " + Ins_omclient.CT_Intitule);
            }
            catch (Exception E)
            {
                Commun.Journalise("Création Nouveau client", "Le création de la fiche a échouée : " + E.Message);
                return null;
            }


            // Catégorie du client 
            String DefautCatTarif = "";
            String DefautCatCompta = "";
            String DefautCptColl = "";
            String DefautPayeur = "";

            try
            {   Ins_omclient.CT_Stat[short.Parse(Core.GetParametres.WOO_StatClient)] = data.Client_TypeClient;  }
            catch (Exception E)
            { Commun.Journalise("Création Nouveau client", "La mise à jour statistique a échouée : " + E.Message); }


            if (data.Client_TypeClient.Equals(Core.GetParametres.WOO_Stat1) == true)
            {
                DefautCatTarif = Core.GetParametres.WOO_StatTarif1;
                DefautCatCompta = Core.GetParametres.WOO_StatCpta1;
                DefautCptColl = Core.GetParametres.WOO_StatCptG1;
                DefautPayeur = Core.GetParametres.WOO_StatPayeur1;
            }
            else if (data.Client_TypeClient.Equals(Core.GetParametres.WOO_Stat2) == true)
            {
                DefautCatTarif = Core.GetParametres.WOO_StatTarif2;
                DefautCatCompta = Core.GetParametres.WOO_StatCpta2;
                DefautCptColl = Core.GetParametres.WOO_StatCptG2;
                DefautPayeur = Core.GetParametres.WOO_StatPayeur2;
            }
            else if (data.Client_TypeClient.Equals(Core.GetParametres.WOO_Stat3) == true)
            {
                DefautCatTarif = Core.GetParametres.WOO_StatTarif3;
                DefautCatCompta = Core.GetParametres.WOO_StatCpta3;
                DefautCptColl = Core.GetParametres.WOO_StatCptG3;
                DefautPayeur = Core.GetParametres.WOO_StatPayeur3;
            }

            // Modification Categorie comptable si non FR
            if (data.Client_ISOPays != Core.GetParametres.WOO_ISOFR & String.IsNullOrEmpty(data.Client_ISOPays) == false)
            {
                String PriorCatCompta = Get_Catcompta_From_Pays(data.Client_ISOPays);
                if (PriorCatCompta != "")
                    DefautCatCompta = PriorCatCompta;
            }
            // Catégorie tarif 
            if (String.IsNullOrEmpty(DefautCatTarif) == false)
            {
                if (om_Cial.FactoryCategorieTarif.ExistIntitule(DefautCatTarif) == false)
                {
                    Commun.Journalise("Création données client", "Les catégorie tarifaire " + DefautCatTarif + " n'existe pas.");
                    return null;
                }
                Ins_omclient.CatTarif = om_Cial.FactoryCategorieTarif.ReadIntitule(DefautCatTarif);
            }

            // Catégorie comptable (Seulement en création)
            if (String.IsNullOrEmpty(DefautCatCompta) == false)
            {
                if (om_Cial.FactoryCategorieComptaVente.ExistIntitule(DefautCatCompta) == false)
                {
                    Commun.Journalise("Création données client", "Les catégorie Comptable " + DefautCatCompta + " n'existe pas.");
                    return null;
                }
                Ins_omclient.CategorieCompta = om_Cial.FactoryCategorieComptaVente.ReadIntitule(DefautCatCompta);
            }


            // Compte Collectif
            if (String.IsNullOrEmpty(DefautCptColl) == false)
            {
                if (om_Cpta.FactoryCompteG.ExistNumero(DefautCptColl) == false)
                {
                    Commun.Journalise("Création données client", "Les Compte collectif par défaut n'existe pas.");
                    return null;
                }
                Ins_omclient.CompteGPrinc = om_Cpta.FactoryCompteG.ReadNumero(DefautCptColl);
            }

            // Tiers Payeur 
            if (String.IsNullOrEmpty(DefautPayeur) == false)
            {
                if (om_Cpta.FactoryClient.ExistNumero(DefautPayeur) == false  )
                {
                    Commun.Journalise("Création données client", "Le Tiers Payeur n'existe pas.");
                    return null;
                }
                Ins_omclient.TiersPayeur = om_Cpta.FactoryClient.ReadNumero(DefautPayeur);
            }

            Ins_omclient.Write();
            Ins_omclient.Read();

            return Ins_omclient.CT_Num;
        }
        private Boolean Modification_client(String code_client, WOO_Doc data, int num_ligne)
        {
            // Chargement
            IBOClient3 Upt_omclient = om_Cpta.FactoryClient.ReadNumero(code_client);
            try
            {
                // Réservation de la fiche + Test si client en sommeil
                Upt_omclient.CouldModified();
                if (Upt_omclient.CT_Sommeil == true)
                {
                    Commun.Journalise("Modification données client", " Le Client " + data.Client_ID + " / " + Upt_omclient.CT_Num + " existe et en sommeil.");
                    return false;
                }
            }
            catch
            {
                Commun.Journalise("Modification données client", "Le fiche client  " + data.Client_ID + " / " + Upt_omclient.CT_Num + " est ouverte, mise à jour impossible.");
                return false;
            }

            try
            {
                // Sauvegarde de l'adresse initiale pour mise à jour ultérieure
                String Adress_initiale = Upt_omclient.CT_Intitule;

                // test intitulé
                if (String.IsNullOrEmpty(data.Client_Nom) == true)
                {
                    Commun.Journalise("Modification données client", "Le libellé est obligatoire.");
                    return false;
                }
                else
                    Upt_omclient.CT_Intitule = data.Client_Nom.Substring(0, Math.Min(35, data.Client_Nom.Length));

                // Classement
                string pattern = @"[^a-zA-Z .-]";
                var rex = new Regex(pattern);
                String Classement = rex.Replace(Upt_omclient.CT_Intitule, "").ToUpper();
                Upt_omclient.CT_Classement = Classement.Substring(0, Math.Min(17, Classement.Length));  /*** MAJOR BUG FIXED HERE ***/

                // Reste des champs sans contrôle de cohérence
                Upt_omclient.CT_Contact = data.Client_Societe; 
                Upt_omclient.Adresse.Adresse = data.Client_Adr1;
                Upt_omclient.Adresse.Complement = data.Client_Adr2;
                Upt_omclient.Adresse.CodePostal = data.Client_CP;
                Upt_omclient.Adresse.Ville = data.Client_Ville;
                Upt_omclient.Adresse.Pays = data.Client_Pays;
                Upt_omclient.Telecom.Telephone = data.Client_tel;
                Upt_omclient.Telecom.EMail = data.Client_Email;

                // Mise à jour de la sous fiche 
                foreach (IBOClientLivraison3 AdrExist in Upt_omclient.FactoryClientLivraison.List)
                    if (AdrExist.LI_Intitule == Adress_initiale)
                    {
                        AdrExist.LI_Intitule = Upt_omclient.CT_Intitule;
                        AdrExist.Adresse.Adresse = Upt_omclient.Adresse.Adresse;
                        AdrExist.Adresse.Complement = Upt_omclient.Adresse.Complement;
                        AdrExist.Adresse.CodePostal = Upt_omclient.Adresse.CodePostal;
                        AdrExist.Adresse.CodeRegion = Upt_omclient.Adresse.CodeRegion;
                        AdrExist.Telecom.Telephone = Upt_omclient.Telecom.Telephone;
                        AdrExist.Telecom.Portable = Upt_omclient.Telecom.Portable;
                        AdrExist.Telecom.Telecopie = Upt_omclient.Telecom.Telecopie;
                        AdrExist.Telecom.EMail = Upt_omclient.Telecom.EMail;
                        AdrExist.Adresse.Ville = Upt_omclient.Adresse.Ville;
                        AdrExist.Adresse.Pays = Upt_omclient.Adresse.Pays;
                        AdrExist.Write();
                        AdrExist.Read();
                        break;
                    }

                Upt_omclient.Write();
                Upt_omclient.Read();
                // Adresse de livraison
                if (String.IsNullOrEmpty(data.Liv_Nom) == false)
                {
                    IBOClientLivraison3 AdrLiv = null;
                    foreach (IBOClientLivraison3 FAdr in Upt_omclient.FactoryClientLivraison.List)
                        if (FAdr.LI_Intitule == data.Liv_Nom)
                        {
                            FAdr.Adresse.Adresse = data.Liv_Adr1;
                            FAdr.Adresse.Complement = data.Liv_Adr2;
                            FAdr.Adresse.CodePostal = data.Liv_CP;
                            FAdr.Adresse.Ville = data.Liv_Ville;
                            FAdr.Adresse.Pays = data.Liv_Pays;
                            FAdr.Telecom.EMail = data.Client_Email;
                            FAdr.Telecom.Telephone = data.Client_tel;

                            FAdr.Write();
                            FAdr.Read();

                            AdrLiv = FAdr;
                            break;
                        }

                    if (AdrLiv == null)
                        AdrLiv = Ajouter_Liv_Client(Upt_omclient, data);

                }

                Upt_omclient.Write();
                Upt_omclient.Read();
            }
            catch (Exception E)
            {
                Commun.Journalise("Création client", "Exception :" + E.Message + " -- " + E.StackTrace + " \\ " + E.TargetSite);
                throw E;
            }

            return true;
        }

        private IBOClientLivraison3 Ajouter_Liv_Client(IBOClient3 loc_omClient, WOO_Doc data)
        {
            IBOClientLivraison3 AdrNew = loc_omClient.FactoryClientLivraison.Create() as IBOClientLivraison3;
            AdrNew.SetDefault();
            AdrNew.LI_Intitule = data.Liv_Nom;
            AdrNew.Adresse.Adresse = data.Liv_Adr1;
            AdrNew.Adresse.Complement = data.Liv_Adr2;
            AdrNew.Adresse.CodePostal = data.Liv_CP;
            AdrNew.Adresse.Ville = data.Liv_Ville;
            AdrNew.Adresse.Pays = data.Liv_Pays;
            AdrNew.Telecom.EMail = data.Client_Email;
            AdrNew.WriteDefault();
            AdrNew.Read();

            return AdrNew;
        }

        private IBOClientLivraison3 Update_Liv_Client(ref IBOClientLivraison3 AdrExist, WOO_Doc data)
        {
            AdrExist.Adresse.Adresse = data.Liv_Adr1;
            AdrExist.Adresse.Complement = data.Liv_Adr2;
            AdrExist.Adresse.CodePostal = data.Liv_CP;
            AdrExist.Adresse.Ville = data.Liv_Ville;
            AdrExist.Adresse.Pays = data.Liv_Pays;
            AdrExist.Telecom.EMail = data.Client_Email;
            AdrExist.Write();
            AdrExist.Read();

            return AdrExist;
        }

        private String Get_Catcompta_From_Pays(String ISO_Pays)
        {
            // comparaison initiale à la liste des code ISO des dom-TOM
            List<String> ISO_DOM = new List<string> { "BL", "GP", "MF", "MQ", "NC", "PF", "PM", "RE", "TF", "WF" };
            if (ISO_DOM.Contains(ISO_Pays) == true)
                return Core.GetParametres.WOO_CptaDOM;


            IBOPays pays = null;
            // Recherche par code ISO
            foreach (IBOPays p in om_Cpta.FactoryPays.List)
                if (p.CodeISO2 == ISO_Pays)
                { 
                    pays = p; 
                    break;
                }

            // Recherche par libellé ou Code
            if (pays == null)
                foreach (IBOPays p in om_Cpta.FactoryPays.List)
                { 
                    if (p.Intitule.ToUpper() == ISO_Pays.ToUpper())
                    {
                        pays = p;
                        break;
                    }
                    if (p.Intitule.ToUpper() == ISO_Pays.ToUpper())
                    {
                        pays = p;
                        break;
                    }
                }

            if (pays == null)
                return "";

            // comparaison à la liste des code ISO des dom-TOM
            if (ISO_DOM.Contains(pays.CodeISO2) == true)
                return Core.GetParametres.WOO_CptaDOM;

            // Test + précis sur France
            if (pays.CodeISO2 == "FR" | pays.Code.ToUpper() == "FRA" | pays.Intitule.ToUpper() == "FRANCE")
                return "";

            // test si UE
            if (pays.IsSEPA == true)
                return Core.GetParametres.WOO_CptaUE;

            

            return Core.GetParametres.WOO_CptaAutre;

        }
    }
}
