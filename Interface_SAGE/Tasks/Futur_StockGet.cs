﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Helper;
using Objets100cLib;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Security.AccessControl;
using System.Security.Principal;
using Interface_SAGE.Core;
using Interface_SAGE.Entities;
using Interface_SAGE.Formats;
using Newtonsoft.Json;
using Interface_SAGE.Process;
using System.Collections;

namespace Interface_SAGE.Tasks
{
    public class StockSAGE
    {
        public String Reference { get; set; }
        public string Serie { get; set; }
        public double Tot_qty { get; set; }
        public double Count { get; set; }
    }

    public class Futur_stockGet
    {
        private MainProgram Core;
        private Liste_Correspondance lst_conv;
        private DateTime Date_Export;
        private Boolean Verbose;
        private String FicNameJsonFake;

        Dictionary<String, Dictionary<String, Double>> liste_regul_globale = new Dictionary<String, Dictionary<String, Double>>();
        List<BatchStock> Liste_Batch_Lus = new List<BatchStock>();

        BSCIALApplication100c om_Cial;
        BSCPTAApplication100c om_Cpta;

        public Futur_stockGet(MainProgram core, String JsonFake = "", Boolean verbose = false)
        {
            Core = core;
            lst_conv = Liste_Correspondance.Get_Liste_Correspondance(core);
            Verbose = verbose;
            FicNameJsonFake = JsonFake;

            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }

        public async Task ImportStock(int mode)
        {

            // connexion à l'API - récupération du TOKEN
            List<StockReturn> ListStock = null;
            try
            {
                

                    // Récupération des commandes par interrogation de l'API
                    ListStock = await Core.WebAPI.Get<List<StockReturn>>(Core.GetParametres.Futur_StockGet);

                    if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                    {
                        String j = Newtonsoft.Json.JsonConvert.SerializeObject(ListStock, Newtonsoft.Json.Formatting.Indented);
                        File.AppendAllText(FicNameJsonFake, j);
                    }
               
                if (Verbose)
                    Commun.Journalise("retour GET", "Nbr de commande récupérée : " + ListStock.Count);

            }
            catch (Exception E)
            {
                Commun.Journalise("Import_DocumentEAsync", "ERREUR lor de l'appel de l'API : " + E.Message, Fl_Alerte: !Core.Automate);
                return;
            }
            if (ListStock.Count == 0)
                return;

            Commun.Journalise("Import_Documents", "Liste de document à traiter : " + ListStock.Count);

            // création du document de stock regul
            int Nbr_regulIn = 0;
            int Nbr_regulOut = 0;

            // Lise globale des regularisation par REFERENCE / LOT / QTE avec QTE + ou -
            IBODocumentStock3 oregulIn = null;
            IBODocumentStock3 oregulOut = null;


            // Parcours du retour de FuturLOG
            foreach (StockReturn wStock in ListStock)
            {
                String Msgerror = "";

                try
                {
                    // Liste par référence 
                    Dictionary<String, Double> liste_regul = Stock_Treatment(wStock, ref Msgerror);

                    if (String.IsNullOrEmpty(Msgerror) == false)
                    {
                        Commun.Journalise("Comparaison REGUL", "ERREUR au produit   " + wStock.Code + " : " + Msgerror, Fl_Alerte: !Core.Automate);

                    }
                    else
                    {
                        if (liste_regul != null)
                        liste_regul_globale.Add(wStock.Code, liste_regul);
                    }
                }
                catch (Exception E)
                {
                    if (Verbose)
                    {
                        Msgerror = " Inattendue " + E.Message + Environment.NewLine + " / " + E.StackTrace + Environment.NewLine + " / " + E.TargetSite;
                        Msgerror += E.InnerException != null ? Environment.NewLine + E.InnerException.Message : "";
                    }
                    else
                    {
                        Msgerror = " Inattendue " + E.Message;
                    }
                }
                finally
                {
                    if (String.IsNullOrEmpty(Msgerror) == false)
                        Commun.Journalise("Intégration DOC", "ERREUR au produit   " + wStock.Code + " : " + Msgerror, Fl_Alerte: !Core.Automate);
                }
            }

            try
            {
                // Parcours de la base SAGE
                IBICollection list_Article = om_Cial.FactoryArticle.List;
                IEnumerable<IBOArticle3> I = list_Article.OfType<IBOArticle3>();
                IEnumerable<IBOArticle3> Art_non_trouve = I.Where(art => art.AR_SuiviStock != SuiviStockType.SuiviStockTypeAucun &
                                                     ListStock.Select<StockReturn, String>(x => x.Code).Contains(art.AR_Ref) == false);

                foreach (IBOArticle3 art in Art_non_trouve)
                {
                    if (art.FactoryArticleGammeEnum1.List.Count > 0)
                        continue;
                    //Commun.Journalise(art.AR_Ref, "************");

                    IBOArticleDepot3 pArtDepot = art.FactoryArticleDepot.ReadDepot(Get_FuturDepo());
                    if (art.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie | art.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot)
                    {
                        var dict = new Dictionary<string, double>();
                        foreach (IBOArticleDepotLot lot in pArtDepot.FactoryArticleDepotLot.QueryNonEpuise())
                        {
                            // Commun.Journalise("lot :", lot.NoSerie);
                            if (dict.TryGetValue(lot.NoSerie, out double qte_ini) == true)
                                dict[lot.NoSerie] = qte_ini + lot.StockReel();
                            else
                                dict.Add(lot.NoSerie, -1 * lot.StockReel());
                        }
                    }
                    else
                    {
                        // Commun.Journalise("qte :", pArtDepot.StockReel().ToString());
                        var dict = new Dictionary<string, double>();
                        dict.Add("", -1 * (pArtDepot.StockDispo()));
                        liste_regul_globale.Add(art.AR_Ref, dict);
                    }
                }
            }
            catch (Exception E)
            {
                Commun.Journalise("Intégration DOC", "ERREUR inattendue   " + E.Message, Fl_Alerte: !Core.Automate);
                return;
            }

            try
            {
                foreach (var elt in liste_regul_globale)
                {
                    String Ref = elt.Key;
                    //Commun.Journalise(Ref, "************");
                    foreach (var ligne in elt.Value)
                    {
                        String Serie = ligne.Key;
                        double qte = ligne.Value;

                        if (qte > 0)  // ENTREE
                        {
                            Commun.Journalise(Ref, "qte IN :"+ qte.ToString());
                            if (oregulIn == null)
                            {
                                oregulIn = om_Cial.FactoryDocumentStock.CreateType(DocumentType.DocumentTypeStockMouvIn);
                                oregulIn.DepotStockage = Get_FuturDepo();
                                oregulIn.DO_Ref = "Futur IN " + DateTime.Today.ToString("dd/MM");
                                oregulIn.SetDefault();
                                oregulIn.WriteDefault();
                            }
                            IBODocumentStockLigne3 lig = oregulIn.FactoryDocumentLigne.Create() as IBODocumentStockLigne3;
                            lig.SetDefaultArticleReference(Ref, qte);
                            if (String.IsNullOrEmpty(Serie) == false)
                                lig.LS_NoSerie = Serie;
                            if (lig.Article.FactoryArticleCond.ExistPrincipal() == true)
                                lig.DL_Qte /= lig.Article.FactoryArticleCond.ReadPrincipal().EC_Quantite;
                            lig.WriteDefault();
                            Nbr_regulIn++;
                        }
                        else if (qte < 0) // SORTIE
                        {
                            //Commun.Journalise(Ref,"qte OUT :"+ qte.ToString());
                            if (oregulOut == null)
                            {
                                oregulOut = om_Cial.FactoryDocumentStock.CreateType(DocumentType.DocumentTypeStockMouvOut);
                                oregulOut.DepotStockage = Get_FuturDepo();
                                oregulOut.DO_Ref = "Futur OUT " + DateTime.Today.ToString("dd/MM");
                                oregulOut.SetDefault();
                                oregulOut.WriteDefault();
                            }
                            IBODocumentStockLigne3 lig = oregulOut.FactoryDocumentLigne.Create() as IBODocumentStockLigne3;
                            lig.SetDefaultArticleReference(Ref, (-1 * qte));
                            if (lig.Article.FactoryArticleCond.ExistPrincipal() == true)
                                lig.DL_Qte /= lig.Article.FactoryArticleCond.ReadPrincipal().EC_Quantite;

                            if (String.IsNullOrEmpty(Serie) == false)
                                lig.LS_NoSerie = Serie;
                            lig.WriteDefault();
                            Nbr_regulOut++;
                        }
                    }

                }
            }
            catch (Exception E)
            {
                Commun.Journalise("Intégration DOC", "ERREUR inattendue   " + E.Message, Fl_Alerte: !Core.Automate);
                return;
            }

            if (Nbr_regulIn != 0 | Nbr_regulOut != 0)
                Commun.Journalise("Regul Stock", "Nombre de ligne de Régul : Entrées " + Nbr_regulIn + " - Sorties " + Nbr_regulOut); 

        }

        private Dictionary<String, Double> Stock_Treatment(StockReturn wStock, ref String MsgErr)
        {
            Dictionary<String, Double> List_Regul = new Dictionary<String, Double>();
            // Recherche du docuemnt
            if (om_Cial.FactoryArticle.ExistReference(wStock.Code) == false)
            {
                MsgErr = "La référence " + wStock.Code + " n'a pas été trouvée";
                return null;
            }

            IBOArticle3 oArt = om_Cial.FactoryArticle.ReadReference(wStock.Code);

            if (oArt.AR_SuiviStock == SuiviStockType.SuiviStockTypeAucun)
                return null; // Cas des BUNDLE

            IBODepot3 pDepot = Get_FuturDepo();
            if (oArt.FactoryArticleDepot.ExistDepot(pDepot) == false)
            {
                IBOArticleDepot3 AD = oArt.FactoryArticleDepot.Create() as IBOArticleDepot3;
                AD.Depot = pDepot;
                AD.SetDefault();
                AD.WriteDefault();
            }

            if (oArt.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot | oArt.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie)
            {

                IBOArticleDepot3 pArtDepot = oArt.FactoryArticleDepot.ReadDepot(pDepot);
                IBICollection list_par_lot = pArtDepot.FactoryArticleDepotLot.QueryNonEpuise();
                IEnumerable<IBOArticleDepotLot> I = list_par_lot.OfType<IBOArticleDepotLot>();
                IEnumerable<StockSAGE> stockSage = from line in I
                                                   group line by line.NoSerie into lot
                                                   select new StockSAGE
                                                   {
                                                       Reference = lot.First().ArticleDepot.Article.AR_Ref,
                                                       Serie = lot.First().NoSerie,
                                                       Tot_qty = lot.Sum(pc => pc.StockReel()),
                                                       Count = lot.Count(),
                                                   };



                // ICI Pour une réfrence
                // Liste des N° série / totale quantité dispo
                foreach (StockSAGE s in stockSage)
                {
                    //Commun.Journalise("Référence " + r.Reference.PadRight(35), " Lot : " + r.Serie.PadRight(35) + " - total dispo : " + r.Tot_qty + " nb ligne : " + r.Count);

                    Double Qt_Futur = wStock.BatchesStock.Where(x => x.Number == s.Serie).Sum(x => x.OnHandQuantity);
                    Liste_Batch_Lus.AddRange(wStock.BatchesStock.Where(x => x.Number == s.Serie));

                    if (s.Tot_qty != Qt_Futur)
                    {
                        List_Regul.Add(s.Serie, Qt_Futur - s.Tot_qty);
                    }
                }

                foreach (BatchStock bs in wStock.BatchesStock)
                {
                    if (Liste_Batch_Lus.Contains(bs) == false)
                    {
                        List_Regul.Add(bs.Number, bs.OnHandQuantity);
                        Liste_Batch_Lus.Add(bs);
                    }
                }
            }
            else if (oArt.AR_SuiviStock != SuiviStockType.SuiviStockTypeAucun)
            {
                IBOArticleDepot3 pArtDepot = oArt.FactoryArticleDepot.ReadDepot(pDepot);

                if ((pArtDepot.StockReel()) != wStock.AvailableQuantity + wStock.ReservedQuantity)
                {
                    List_Regul.Add("", (wStock.AvailableQuantity + wStock.ReservedQuantity) - pArtDepot.StockReel());
                }
            }
            else if (oArt.AR_SuiviStock == SuiviStockType.SuiviStockTypeAucun)
            {
                // Cas des Bundle en stock chez futurlog et non dans SAGE
                //return null;
            }

                return List_Regul;
        }






        private IBODepot3 Get_FuturDepo()
        {
            foreach (IBODepot3 pDepot in om_Cial.FactoryDepot.List)
            {
                if (String.IsNullOrEmpty(Core.GetParametres.Futur_Depot) == false)
                    if (pDepot.DE_Intitule != Core.GetParametres.Futur_Depot & pDepot.DE_Code != Core.GetParametres.Futur_Depot)
                        continue;
                return pDepot;
            }
            return null;

        }
    }
}
