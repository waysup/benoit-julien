﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Helper;
using Objets100cLib;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Security.AccessControl;
using System.Security.Principal;
using Interface_SAGE.Core;
using Interface_SAGE.Entities;
using Interface_SAGE.Formats;

namespace Interface_SAGE.Tasks
{
    class Futur_ProductSend
    {
        private MainProgram Core;
        private Liste_Correspondance lst_conv;
        private Char Sep;
        private Char Del;
        private DateTime Date_Export;
        private Boolean Verbose;
        private String FicNameJsonFake;
        BSCIALApplication100c om_Cial;
        BSCPTAApplication100c om_Cpta;

        public Futur_ProductSend(MainProgram core, String JsonFake = "", Boolean verbose = false)
        {
            Core = core;
            Date_Export = DateTime.Now;
            lst_conv = Liste_Correspondance.Get_Liste_Correspondance(core);
            Verbose = verbose;
            FicNameJsonFake = JsonFake;

            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }

        public async 
        Task
ProductSend(int mode)
        {
            List<Product> List_export = new List<Product>();
            /* DateTime Date_DernierExport = Core.Get_lastExport_cli();*/
            DateTime Date_DernierExport = DateTime.Now;
            // Base Sage 

            // Catégorie tarifaire prix public
            if (om_Cial.FactoryCategorieTarif.ExistIntitule(Core.GetParametres.WOO_StatTarif1) == false )
            {
                Commun.Journalise("Export article", "La catégorie tarifaire pour calcul Prix Public non trouvée, ABANDON traitement");
                return;
            }
            IBPCategorieTarif catTar = om_Cial.FactoryCategorieTarif.ReadIntitule(Core.GetParametres.WOO_StatTarif1) as IBPCategorieTarif;
      
            foreach (IBOArticle3 oArt in om_Cial.FactoryArticle.ListOrderReference)
            {

                List<ProductInBundle> bundle = Get_ListBundle(oArt);

                //if (oArt.AR_Publie == false)
                if ( oArt.AR_SuiviStock == SuiviStockType.SuiviStockTypeAucun & bundle.Count == 0)
                    continue;

                // Ajout de l'article à la liste à exporter
                Product newExp = new Product
                {

                    //AlertThreshold 
                    BarCode = oArt.AR_CodeBarre,
                    Code = oArt.AR_Ref,
                    // Color 
                    //ExternalCode 
                    Family = oArt.Famille.FA_Intitule,
                    //Height
                    HsCode = oArt.AR_CodeFiscal,
                    IsActive = oArt.AR_Sommeil,
                    IsLotManaged = oArt.AR_SuiviStock == SuiviStockType.SuiviStockTypeLot | oArt.AR_SuiviStock == SuiviStockType.SuiviStockTypeSerie,
                    Label = oArt.AR_Design,
                    //Length 
                    // Model 
                    OriginCountryCode = om_Cpta.FactoryPays.ExistIntitule(oArt.AR_Pays) ? om_Cpta.FactoryPays.ReadIntitule(oArt.AR_Pays).CodeISO2 : "",
                    //Parameter1
                    //Parameter2 
                    //PictureUrl 
                    //Size 
                    TvaRate = (Decimal?)Get_TauxTVA(oArt),
                    //Type 
                    UnitPriceHT = (Decimal)Get_Prix_Unitaire(oArt, catTar),
                    // WeePrice 
                    //Weight 
                    // Width 
                    ProductsInBundle = bundle


                };
                List_export.Add(newExp);
            }

            // Réalisation de l'export
            try
            {

                await Webap.Get_Webap(Core).Post(Core.GetParametres.Futur_ProduitSend, List_export,FicNameJsonSave: FicNameJsonFake);
                // Mise à jour date dernier export
                Commun.Journalise("Export Produit", "Export de la liste Produits : " + List_export.Count);

            }
            catch (Exception E)
            {
                Commun.Journalise("Export Produit", "Erreur lors de l'appel à l'API : " + E.Message);
            }
        }


        
    
    public static Double Get_TauxTVA(IBOArticle3 Art)
    {
            BSCIALApplication100c om_Cial = Art.Stream as BSCIALApplication100c;
            // recherche du taux de TVA -> normalement le premier donc TVA France
            /* ... INFORMATION ONLY ... */
            IBOTaxe3 Art_Taxe1 = null;
            foreach (IBPCategorieComptaVente NCat in om_Cial.FactoryCategorieComptaVente.List)
            {

                // Recherche existance code TVA sur article
                if (Art.FactoryArticleParamCompta.ExistCategorieCompta(NCat))
                {
                    IBOArticleParamCompta3 ParamComptaArt = Art.FactoryArticleParamCompta.ReadCategorieCompta(NCat);
                    if (ParamComptaArt.Taxe[1] != null)
                    {
                        Art_Taxe1 = ParamComptaArt.Taxe[1];
                        break;
                    }
                    else
                    {
                        // Art_Taxe1 still empty
                        // recherche famille break;
                    }
                }
                else
                { // Sinon Taux TVA de la famille
                    foreach (IBOFamilleParamCompta3 ParamComptaFam in Art.Famille.FactoryFamilleParamCompta.List)
                    {
                        if (ParamComptaFam.CategorieCompta == NCat)
                        {
                            Art_Taxe1 = ParamComptaFam.Taxe[1];
                            break;
                        }
                    }
                }
                if (Art_Taxe1 != null)
                    break;
            }


            if (Art_Taxe1 == null)
            {
                return 0;
            }

            return Art_Taxe1.TA_Taux;
        }

        public static Double Get_Prix_Unitaire(IBOArticle3 oArt, IBPCategorieTarif catTar)
        {
            double PxUnit = 0;
            var tarif = oArt.TarifVenteCategorie(catTar, 1);
            if (tarif == null)
                tarif = oArt.TarifVenteCategorie(catTar, 0);
            if (tarif != null)
                PxUnit = tarif.Prix;
            else
                PxUnit = oArt.AR_PrixVen;

            return PxUnit;
        }

        public static List<ProductInBundle> Get_ListBundle(IBOArticle3 Art)
        {
            BSCIALApplication100c basesage =  Art.Stream as BSCIALApplication100c;
            List<ProductInBundle> result = new List<ProductInBundle>();

            foreach (IBOArticleNomenclature3 mIboArt in basesage.FactoryArticle.ReadReference(Art.AR_Ref).FactoryArticleNomenclature.List)
            {
                // Ajout de l'article à la liste à exporter
                ProductInBundle Component = new ProductInBundle
                {
                    Code = mIboArt.ArticleComposant.AR_Ref,
                    Quantity = (int?)mIboArt.NO_Qte
                };
                result.Add(Component);

            }
            return result;
        }
    }
}

