﻿using Interface_SAGE.Core;
using System;
using System.Collections.Generic;
using WinSCP;

namespace Interface_SAGE.Helper
{


    // dépendance NUGET : WinSCPnet.dll   v.5.15.2    Référence : https://winscp.net/eng/docs/library
    // Exemples de sources C# et VB : https://winscp.net/eng/docs/library_examples
    // il est possible de générer des script via l'UI : https://winscp.net/eng/docs/ui_generateurl

    public class FTP
    {
        private SessionOptions sessionOptions;
        public FTP(MainProgram core)
        {

            sessionOptions = new SessionOptions
            {
                Protocol = core.GetParametres.FTPProtocol == "FTP" ? Protocol.Ftp : Protocol.Sftp,
                HostName = core.GetParametres.FTPUri,
                UserName = core.GetParametres.FTPUser,
                Password = DataProtection.Unprotect(core.GetParametres.FTP_MdpCripte)
            };
            if (String.IsNullOrEmpty(core.GetParametres.FTPSSH) == false)
                sessionOptions.SshHostKeyFingerprint = core.GetParametres.FTPSSH;
            if (sessionOptions.Protocol != Protocol.Ftp)
                sessionOptions.PortNumber = Int32.Parse(core.GetParametres.FTPPort);
            if (true)
            {
                //sessionOptions.FtpMode = FtpMode.Active;
                //sessionOptions.FtpSecure
                //sessionOptions.RootPath = "/files/GAYA";
                //sessionOptions.Protocol = Protocol.Webdav;
            }
        }


        /// <summary>
        /// Déposer un ou des fichier sur le FTP distant
        /// </summary>
        /// <param name="localPath">syntaxe : @"d:\toupload\*"</param>
        /// <param name="remotePath">syntaxe : "/home/user/" </param>
        /// <param name="delete">effacement des fichiers locaux après transfert</param>
        /// <returns>Retourne la liste de fichiers transférés</returns>
        public List<String> Put_Files(String localPath, String remotePath, bool delete = false)
        {
            try
            {
                // Création de la liste à retourner
                List<String> FileList = new List<string>();

                using (Session session = new Session())
                {
                    // Connexion
                    session.Open(sessionOptions);

                    // Définition option de session
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    // Upload des fichier
                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(localPath, remotePath, delete, transferOptions);

                    // Auto-Vérif, qui lance l'exception
                    transferResult.Check();

                    // Création de la liste des éléments copier

                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        if (transfer.Error != null)
                        {
                            // TODO gestion des erreurs
                        }
                        else
                        {
                            FileList.Add(transfer.FileName);
                        }
                    }
                }
                return FileList;
            }
            catch (Exception e)
            {
                // Propoagation de l'exception
                throw new Exception("Erreur lors du transfert de fichier via SFTP :" + e.Message, e);
            }
        }

        /// <summary>
        /// Déposer un ou des fichier sur le FTP distant
        /// </summary>
        /// <param name="localPath">syntaxe : @"d:\toupload\*"</param>
        /// <param name="remotePath">syntaxe : "/home/user/" </param>
        /// <param name="delete">effacement des fichiers locaux après transfert</param>
        /// <returns>Retourne la liste de fichiers transférés</returns>
        public Boolean Put_File(String localFic, String remotePath, bool delete = false)
        {
            try
            {
                // Création de la liste à retourner
                List<String> FileList = new List<string>();

                using (Session session = new Session())
                {
                    // Connexion
                    session.Open(sessionOptions);

                    // Définition option de session
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    // Upload des fichier
                    TransferEventArgs transferArgs;
                    transferArgs = session.PutFileToDirectory(localFic, remotePath, delete);

                    // Auto-Vérif, qui lance l'exception
                    if (transferArgs.Error != null)
                    {
                        return false;
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                // Propoagation de l'exception
                throw new Exception("Erreur lors du transfert de fichier via SFTP :" + e.Message, e);
            }
        }
        /// <summary>
        /// Récupérer des fichiers à partir d'un FTP
        /// </summary>
        /// <param name="localPath">syntaxe : @"d:\download\" </param>
        /// <param name="remotePath">syntaxe : "/home/user/*" </param>
        /// <param name="delete">Effacement des fichiers après le transfert</param>
        /// <returns></returns>
        public List<String> Get_Files(String localPath, String remotePath, bool delete = false)
        {
            try
            {
                // Création de la liste à retourner
                List<String> FileList = new List<string>();

                using (Session session = new Session())
                {
                    //session.DebugLogLevel = 2;
                    //session.DebugLogPath = "C:\\temp\\Winscp_debug.log";
                    //session.SessionLogPath = "C:\\temp\\Winscp_session.log";
                    // Connexion
                    session.Open(sessionOptions);

                    // Définition option de session
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    // Upload des fichier
                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles(remotePath, localPath, delete, transferOptions);

                    // Auto-Vérif, qui lance l'exception
                    transferResult.Check();

                    // Création de la liste des éléments copier

                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        if (transfer.Error != null)
                        {
                            // TODO gestion des erreurs
                        }
                        else
                        {
                            FileList.Add(transfer.FileName);
                        }
                    }
                }
                return FileList;
            }
            catch (Exception e)
            {
                // Propoagation de l'exception
                throw new Exception("Erreur lors du transfert de fichier via SFTP :" + e.Message, e);
            }
        }
    }
}
