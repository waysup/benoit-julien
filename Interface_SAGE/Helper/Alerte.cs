﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;


namespace Interface_SAGE.Helper
{
    public class Alerte
    {
        /// <summary>
        /// Envoyer un Email 
        /// </summary>
        /// <param name="adresses">Liste d'adresse mail de destinataires</param>
        /// <param name="subject">Sujet du message</param>
        /// <param name="message">Texte du message</param>
        /// /// <param name="filename">Liste de nom de fichiers joints</param>
        /// <remarks>usage :    string[] users = {"utilisateur@hotmail.fr", "utilisateur@gmail.com"};
        ///                     string[] sendFiles = {@"C:\Temp\fichier.pdf", "C:\Temp\fichier2.pdf"};
        ///                     MailHelper.SendMail(users, "Hello", "Voici des nouvelles !");
        ///</remarks>
        public static void SendMail(string[] adresses, string subject, string message,String[] filenames)
        {
           
            String smtp_Adress = DataProtection.ReadAppSetting("SMTP_FromAdress");
            String smtp_DisplayNname = DataProtection.ReadAppSetting("SMTP_FromDisplayName");
            String smtp_UserName = DataProtection.ReadAppSetting("SMTP_UserName");
            String smtp_Pwd = DataProtection.ReadAppSetting("SMTP_Password");
            String smtp_Host = DataProtection.ReadAppSetting("SMTP_Host");
            String txtsmtp_Port = DataProtection.ReadAppSetting("SMTP_Port");
            if (Int32.TryParse(txtsmtp_Port, out int smtp_Port) == false)
                smtp_Port = 587;
            String txtsmtp_Ssl = DataProtection.ReadAppSetting("SMTP_SSL");
            if (Boolean.TryParse(txtsmtp_Ssl, out Boolean smtp_Ssl) == false)
                smtp_Ssl = false;

            try
            {
                MailMessage mail = new MailMessage();
                //ajouter les destinataires
                foreach (string adress in adresses)
                    mail.To.Add(adress);

                // Ajouter les fichiers joints
                foreach (string filename in filenames)
                    mail.Attachments.Add(new Attachment(filename));

                // Corps du message
                mail.Subject = subject;
                mail.Body = message;
                
                //définir l'expéditeur
                mail.From = new MailAddress(smtp_Adress, smtp_DisplayNname);

                //définir les paramètres smtp pour l'envoi
                SmtpClient smtpServer = new SmtpClient
                {
                    Host = smtp_Host,
                    Port = smtp_Port,
                    EnableSsl = smtp_Ssl,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(smtp_UserName,smtp_Pwd )
                };
                //envoi du mail
                smtpServer.Send(mail);
            }
            catch (Exception E)
            {
                // Raise Error
                throw E;
            }

        }
        /// <summary>
        /// Enovi une notification dans SAGE à l'utilisateur demandé
        /// </summary>
        /// <param name="baseSage">objet base SAGE</param>
        /// <param name="txt">MEssage à envoyé </param>
        /// <param name="UserName">Destinataire =  LOGIN SAGE</param>
        private void Send_Sage_Notification(BaseSage baseSage, String txt,String UserName = "<Administrateur>")
        {
            // Test si existence de la procédure + tentative création
            if (BaseSage.test_Existance_Table_SPE(baseSage.Get_ChaineConnexion(), "ZZ_Send_Async_Message") == false)
                throw new NotImplementedException("Impossible d'envoyer une notification, la base SAGE ne le prends pas en charge.");

            try
            {
                // Appel de la procédure stockée de création des échéances
                SqlConnection sqlConn = new SqlConnection(baseSage.Get_ChaineConnexion());
                SqlCommand sqlCMD = new SqlCommand("ZZ_Send_Async_Message", sqlConn);
                sqlCMD.CommandType = CommandType.StoredProcedure;


                // Définition des paramètres
                SqlParameter Msg_Param = sqlCMD.Parameters.Add("@Msg", SqlDbType.VarChar);
                Msg_Param.Direction = ParameterDirection.Input;
                SqlParameter User_Param = sqlCMD.Parameters.Add("@LstUSer", SqlDbType.VarChar);
                User_Param.Direction = ParameterDirection.Input;

                // qualification des paramètres
                Msg_Param.Value = txt;
                User_Param.Value = UserName;

                sqlConn.Open();

                sqlCMD.ExecuteNonQuery();
            }
            catch (Exception E)
            {
                // Raise Error
                throw E;
            }
        }
    }
}
