﻿using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Interface_SAGE.Core;
using Interface_SAGE.Entities;

public class DataProtection
{
    #region Methode statique SINGLETON
    private static DataProtection Instance;

    public static DataProtection Get_DataParam(String paramFilePath = "")
    {
        if (Instance == null)
        {
            Instance = new DataProtection(paramFilePath );
            return Instance;
        }
        if (paramFilePath != "" & Instance.Get_ParamFilePath().Equals(paramFilePath) == false)
        {
            Instance = new DataProtection(paramFilePath);
            return Instance;
        }
        else
            return Instance;
    }
    #endregion Methode statique SINGLETON

    private String ParamPath;
    private FileInfo ParamFile;
    public Parametres parametres;

    /// <summary>
    /// Ouverture du fichier paramètres
    /// </summary>
    /// <param name="paramFilePath">Nom du fichier parametres à ouvrir</param>
    /// <param name="Liste_Param">Pour initialisation des paramètres avec création d'un fichier défaut.</param>
    public DataProtection(String paramFilePath = "")
    {

        if (String.IsNullOrEmpty(paramFilePath) == true)
        {
            DirectoryInfo AppDir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);

            ParamFile = new FileInfo(AppDir + "\\Paramètres\\ParamDefaut.XML");
            if (ParamFile.Exists == false)
            {
                AppDir.CreateSubdirectory("Paramètres");
                FileStream FS = new FileStream(ParamFile.FullName, FileMode.CreateNew);
                FS.Close();
               
                parametres = Parametres.Init_Parametres();
                SaveParam(ParamFile);
            }

        }
        else
            ParamFile = new FileInfo(paramFilePath);

        ParamPath = ParamFile.FullName;
        LoadParametres();
        if (parametres == null)
        {
            parametres = Parametres.Init_Parametres();
            SaveParam(ParamFile);
        }
    }

    public String Get_ParamFilePath()
    {
        return ParamPath;
    }
    public void Set_ParamFilePath(String paramPath)
    {
        ParamPath = paramPath;
        ParamFile = new FileInfo(paramPath);
        LoadParametres();
    }

    private void LoadParametres()
    {
        try
        {
            using (StreamReader SR = ParamFile.OpenText())
            { 
                if (ParamFile.Extension.ToUpper() == ".XML")
                {
                    System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(Parametres));
                    parametres = (Parametres)xs.Deserialize(SR.BaseStream);

                }
                else
                    parametres = JsonConvert.DeserializeObject<Parametres>(SR.ReadToEnd());
            }
        }
        catch (Exception E)
        { throw E; }
    }

    /// <summary>
    /// Enregistrer Paramètres SAGE dans fichier spécifié
    /// </summary>
    /// <param name="sageParam">Objet de paramètres</param>
    /// <param name="JsonFile">Objet fichier à enregistrer</param>
    public void SaveParam(FileInfo FileParam)
    {
        try
        {
            if (FileParam.Exists == true)
                FileParam.Delete();

            if (ParamFile.Extension.ToUpper() == ".XML")
            {
                System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(Parametres));
                using (StreamWriter SW = FileParam.CreateText())
                {
                    xs.Serialize(SW, parametres);
                    //SW.Write(xs);
                }
            }
            else
            {
                string json = JsonConvert.SerializeObject(parametres);

                using (StreamWriter SW = FileParam.CreateText())
                    SW.Write(json);
            }
        }
        catch (Exception E)
        { throw E; }
    }

    /*
    public static String Get_Parametre(String Key)
    {
        if (DataProtection.Get_DataParam().Parametres.TryGetValue(Key, out String Value))
            return Value;
        else
            return null;
    }
    public static void Set_Parametre(String Key,String Value)
    {
        if (DataProtection.Get_DataParam().Parametres.TryGetValue(Key, out String ExistingValue))
            DataProtection.Get_DataParam().Parametres[Key] = Value;
        else
            DataProtection.Get_DataParam().Parametres.Add(Key, Value);
    }
    */

    #region ENCRYPT
    // Create byte array for additional entropy when using Protect method.
    private static byte[] s_aditionalEntropy = { 2, 0, 2, 0 };

    public static string Protect(String texteAEncoder, byte[] key = null, DataProtectionScope Scope = DataProtectionScope.LocalMachine)
    {
        if (key == null)
            key = s_aditionalEntropy;
        try
        {
            // Encrypt the data using DataProtectionScope.CurrentUser. The result can be decrypted
            // only by the same current user.
            byte[] bits = ProtectedData.Protect(Encoding.ASCII.GetBytes(texteAEncoder), key, Scope);
            return System.Convert.ToBase64String(bits);
        }
        catch (CryptographicException e)
        {
            throw new Exception("La donnée n'a pas été décrytpée. Une erreur est survenue.", e);
        }
    }

    public static String Unprotect(string texteADecoder, byte[] key = null, DataProtectionScope Scope = DataProtectionScope.LocalMachine)
    {
        if (key == null)
            key = s_aditionalEntropy;

        if (String.IsNullOrEmpty(texteADecoder) == true)
            return "";
        try
        {
            //Decrypt the data using DataProtectionScope.CurrentUser.
            return Encoding.ASCII.GetString(ProtectedData.Unprotect(System.Convert.FromBase64String(texteADecoder), key, Scope));
        }
        catch (CryptographicException)
        {
            throw new Exception("La donnée n'a pas été décrytpée. Une erreur est survenue.");
        }
    }
    #endregion ENCRYPT

    #region dynamic settings (scope Appli)
    //**  Keep in mind it should be necessary to open right on READ/WRITE for every user on the app.settings file.  **//
    public static List<KeyValuePair<string, object>> ReadAllSettings()
    {
        List<KeyValuePair<string, object>> result = new List<KeyValuePair<string, object>>();
        try
        {

            var appSettings = ConfigurationManager.AppSettings;

            if (appSettings.Count == 0)
            {
                Console.WriteLine("AppSettings is empty.");
            }
            else
            {
                foreach (var key in appSettings.AllKeys)
                {
                    result.Add(new KeyValuePair<string, object>(key, appSettings[key]));
                }
            }
        }
        catch (ConfigurationErrorsException e)
        {
            throw new Exception("Erreur de lecture dans le fichier de configuration", e);
        }
        return result;
    }

    public static string ReadSetting(string key)
    {
        try
        {
            var appSettings = ConfigurationManager.AppSettings;
            string result = appSettings[key] ?? null;
            return result;
        }
        catch (ConfigurationErrorsException e)
        {
            throw new Exception("Erreur de lecture dans le fichier de configuration", e);
        }
    }

    public static string ReadAppSetting(string key, ConfigurationUserLevel Scope = ConfigurationUserLevel.None)
    {
        try
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(Scope);
            var settings = configFile.AppSettings.Settings;
            var IE = settings.GetEnumerator();
            foreach (KeyValueConfigurationElement setting in settings)
            {
                if (setting.Key == key)
                    return setting.Value;
            }
        }
        catch (ConfigurationErrorsException e)
        {
            throw new Exception("Erreur de lecture dans le fichier de configuration", e);
        }
        return null;
    }


    public static void AddUpdateAppSettings(string key, string value, ConfigurationUserLevel Scope = ConfigurationUserLevel.None)
    {
        try
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(Scope); // Explore Possibility to target a file with another method
            var settings = configFile.AppSettings.Settings;
            if (settings[key] == null)
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key].Value = value;
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }
        catch (ConfigurationErrorsException e)
        {
            throw new Exception("Erreur d'écriture dans le fichier de configuration", e);
        }
    }
    #endregion dynamic settings (scope Appli)
}

