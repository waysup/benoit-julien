﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Entities;
using System.Collections.Generic;

using System.Net;
using System.Net.Http.Headers;
using System.IO;
using Interface_SAGE.Core;

namespace Interface_SAGE.Helper
{
    /* https://stackoverflow.com/questions/9620278/how-do-i-make-calls-to-a-rest-api-using-c
    * To post, use something like this:
    * await HttpHelper.Post<OBJECT>($"/api/values/{id}", OBJECT);
    * 
    * Example for delete:
    * await HttpHelper.Delete($"/api/values/{id}");
    * 
    * Example to get a list:
    * List<OBJECT> OBJECTs = await HttpHelper.Get<List<OBJECT>>("/api/values/");
    * 
    * Example to get only one:
    * OBJECT processedOBJECT = await HttpHelper.Get<OBJECT>($"/api/values/{id}");
    */

    public class Webap
    {
        private MainProgram Core;
        private string apiBasicUri;

        #region Methode statique SINGLETON
        private static Webap Instance;

        public static Webap Get_Webap(MainProgram core)
        {
            if (Instance == null)
            {
                Instance = new Webap(core);
                return Instance;
            }
            else
                return Instance;
        }
        public static Webap Re_init_Webap(MainProgram core)
        {
            Instance = null;
            return Get_Webap(core);
        }
        #endregion Methode statique SINGLETON

        #region Constructeur privé
        private Webap(MainProgram core)
        {
            Core = core;
            apiBasicUri = Core.GetParametres.Futur_APIURI;
        }
        #endregion Constructeur privé

        #region méthodes publique d'accès à l'API


        // Méthode NON asynchrone, elle attends la réponse pour retourner le TOKEN
       public WebToken GetToken<WebToken>(WebAuth AuthValue)
        {
            //TODO vérifier nécessité de la sécurité
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            using (var client = new HttpClient())
            {
                // Définition des adresse destination
                client.BaseAddress = new Uri(apiBasicUri);
                var url = Core.GetParametres.Futur_APIURI;

                // Ajout HEADER du format utilisé 
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Attachement du contenu désérialisé
                HttpResponseMessage response = new HttpResponseMessage();
                HttpContent requestParams = new StringContent(JsonConvert.SerializeObject(AuthValue), Encoding.UTF8, "application/json");

                // Appel de la méthode POST
                response = client.PostAsync(url, requestParams).Result;
                if (response.IsSuccessStatusCode == false)
                    Commun.Journalise("Authentification API KO", "Status : " + response.StatusCode + " / " + response.ReasonPhrase);

                response.EnsureSuccessStatusCode();

                // Désérialisation de la réponse
                string resultContentString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<WebToken>(resultContentString);
            }

        }

        public async Task Post<T>(string url, T contentValue, String token = "",String FicNameJsonSave = "")
        {
            url = Reformat_URL(url);
            using (var client = new HttpClient())
            {
                if (!string.IsNullOrWhiteSpace(token))
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                }
                client.BaseAddress = new Uri(apiBasicUri);
                var content = new StringContent(JsonConvert.SerializeObject(contentValue), Encoding.UTF8, "application/json");

                // Enregistrement sur disque du JSON Pour tests
                if (String.IsNullOrEmpty(FicNameJsonSave) == false)
                {
                    try
                    {
                        File.AppendAllText(FicNameJsonSave, JsonConvert.SerializeObject(contentValue,Formatting.Indented));
                    }
                    catch (Exception E)
                    { Commun.Journalise("Appel GET", "ERREUR  écriture du fichier JSON Save : " + E.Message, Fl_Alerte: !Core.Automate); }
                     return; 
                }


                var result = await client.PostAsync(url, content);
                if (result.IsSuccessStatusCode == false)
                    Commun.Journalise("Appel POST API KO", "Status : " + result.StatusCode + " / " + result.ReasonPhrase);
                result.EnsureSuccessStatusCode();
            }
        }

        public async Task Put<T>(string url, T contentValue, String token = "", String FicNameJsonSave = "")
        {
            url = Reformat_URL(url);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiBasicUri);
                if (!string.IsNullOrWhiteSpace(token))
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                }
                var content = new StringContent(JsonConvert.SerializeObject(contentValue), Encoding.UTF8, "application/json");

                // Enregistrement sur disque du JSON Pour tests
                if (String.IsNullOrEmpty(FicNameJsonSave) == false)
                {
                    try
                    {
                        File.AppendAllText(FicNameJsonSave, JsonConvert.SerializeObject(contentValue, Formatting.Indented));
                    }
                    catch (Exception E)
                    { Commun.Journalise("Appel GET", "ERREUR  écriture du fichier JSON Save : " + E.Message, Fl_Alerte: !Core.Automate); }
                    return;
                }


                var result = await client.PutAsync(url, content);
                if (result.IsSuccessStatusCode == false)
                    Commun.Journalise("Appel PUT API KO", "Status : " + result.StatusCode + " / " + result.ReasonPhrase);
                result.EnsureSuccessStatusCode();
            }
        }

        public async Task<T> Get<T>(string url, String token = "", String FicNameJsonSave = "")
        {
            url = Reformat_URL(url);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiBasicUri);
                if (!string.IsNullOrWhiteSpace(token))
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                }

                var result = await client.GetAsync(url);
                if (result.IsSuccessStatusCode == false)
                    Commun.Journalise("Appel Get API KO", "Status : " + result.StatusCode + " / " + result.ReasonPhrase);
                result.EnsureSuccessStatusCode();
                string resultContentString = await result.Content.ReadAsStringAsync();


                // Enregistrement sur disque du JSON Pour tests
                if (String.IsNullOrEmpty(FicNameJsonSave) == false)
                {
                    try
                    {
                        File.AppendAllText(FicNameJsonSave, resultContentString);
                    }
                    catch (Exception E)
                    { Commun.Journalise("Appel GET", "ERREUR  écriture du fichier JSON Save : " + E.Message, Fl_Alerte: !Core.Automate); }
                }

                try
                {
                    T resultContent = JsonConvert.DeserializeObject<T>(resultContentString);
                    return resultContent;
                }
                catch
                {
                    Response<T> resultContent = JsonConvert.DeserializeObject<Response<T>>(resultContentString);
                    return resultContent.Data;
                }
            }
        }
        class Response<T>
        {
            public  T Data { get; set; }
            public object[] Errors { get; set; }
            public string LogId { get; set; }
        }

        public async Task Delete(string url, String token = "")
        {
            url = Reformat_URL(url);
            using (var client = new HttpClient())
            {
                if (!string.IsNullOrWhiteSpace(token))
                {
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                }
                client.BaseAddress = new Uri(apiBasicUri);
                var result = await client.DeleteAsync(url);
                if (result.IsSuccessStatusCode == false)
                    Commun.Journalise("Appel Delete API KO", "Status : " + result.StatusCode + " / " + result.ReasonPhrase);
                result.EnsureSuccessStatusCode();
            }
        }
        #endregion méthodes publique d'accès à l'API

        #region methodes private
        private String Reformat_URL(String URL,Boolean Add_PrefixURI = false)
        {
            URL = URL.Replace("{merchantCode}", Core.GetParametres.Futur_APIMerchantcode);
            URL = URL.Replace("{login}", Core.GetParametres.Futur_APILogin);
            URL = URL.Replace("{key}", DataProtection.Unprotect(Core.GetParametres.Futur_APIKey));

            // dynamic replacement
            DateTime UTC = DateTime.Now.ToUniversalTime();
            string strUTC = UTC.ToString("u");
            URL = URL.Replace("{dateFromUtc}", strUTC);
            if (Add_PrefixURI == true )
                URL = Core.GetParametres.Futur_APIURI + URL;

            return URL;
        }

        #endregion
    }
}
