﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_SAGE.Core;
using Objets100cLib;


namespace Interface_SAGE.Helper
{
 
    public class BaseSage : IDisposable
    {
       private String Raccourci;
       private String SAGE_base;
       private String SAGE_serveur;
       private String SAGE_User;
       private String SAGE_Pwd;

#if DEBUG
        const String Serialmatch = "5555701";
#else
            const String Serialmatch = "";
#endif


        public  BSCIALApplication100c OM_baseCial { get => _om_baseCial; }
        public  BSCPTAApplication100c OM_baseCpta { get =>  _om_baseCpta; }
        private BSCPTAApplication100c _om_baseCpta;
        private BSCIALApplication100c _om_baseCial;

        public  Boolean IsOpen = false;
        public String Nom_Dossier
        {
            get
            {

                if (OM_baseCial.IsOpen == true)
                {
                    if (String.IsNullOrEmpty(OM_baseCial.Name) == false)
                        return OM_baseCial.Name;
                    else if (String.IsNullOrEmpty(OM_baseCial.CompanyName) == false)
                        return OM_baseCial.CompanyName;
                    else 
                        return OM_baseCial.CompanyDatabaseName;
                }
                else
                {
                    if (String.IsNullOrEmpty(OM_baseCpta.Name) == false)
                        return OM_baseCpta.Name;
                    else if (String.IsNullOrEmpty(OM_baseCpta.CompanyName) == false)
                        return OM_baseCpta.CompanyName;
                    else 
                        return OM_baseCpta.CompanyDatabaseName;
                }

            }
        }

#region construteur
        public BaseSage(MainProgram Core)
        {
            Raccourci = Core.GetParametres.Sage_Raccourci;

            SAGE_base = getNomBase(Raccourci);
            SAGE_serveur = getNomInstanceServeur(Raccourci);
            SAGE_User = Core.GetParametres.Sage_Utilisateur;
            SAGE_Pwd = DataProtection.Unprotect(Core.GetParametres.Sage_MdpCripte);

            _om_baseCpta = new BSCPTAApplication100c();
            _om_baseCial = new BSCIALApplication100c();

        }
#endregion
#region Connexion

        public static BaseSage Connexion_BaseSage(MainProgram Core)
        {

            BaseSage BS = new BaseSage(Core);
            if (Path.GetExtension(BS.Raccourci.ToUpper()) == ".MAE")
                BS.Connexion_BaseSageCompta_Seule();
            else if (Path.GetExtension(BS.Raccourci.ToUpper()) == ".GCM")
                BS.Connexion_BaseSage_Cial();
            else
                throw new Exception("Extension du raccourci SAGE non reconnu.");

            BaseSage.test_Existance_Table_SPE(BS.Get_ChaineConnexion());
            return BS;
        }

            private BaseSage Connexion_BaseSageCompta_Seule()
            {
                Boolean flgConnexionSAGE = false;   //   Indicateur de connexion
            int NbEssais = 0;                       //   nombre d'essais

            try
            {
                //   Tant que la connexion n'est pas active et que le nombre d'essais ne dépasse pas 3
                while (flgConnexionSAGE == false & NbEssais <= 3)
                {
                    //   Incrémente compteur d'essais
                    NbEssais += 1;
                    //   Fermeture si redondance
                    if (this.IsOpen)
                    {
                        OM_baseCpta.Close();
                        this.IsOpen = false;
                    }

                    try
                    {

                        OM_baseCpta.CompanyServer = this.SAGE_serveur;
                        OM_baseCpta.CompanyDatabaseName = this.SAGE_base;
                        OM_baseCpta.Loggable.UserName = String.IsNullOrEmpty(this.SAGE_User) ? "<Administrateur>" : this.SAGE_User;
                        OM_baseCpta.Loggable.UserPwd = String.IsNullOrEmpty(this.SAGE_Pwd) ? "" : this.SAGE_Pwd;

                        OM_baseCpta.Open();

                        //   base Compta ouverte, active l'indicateur
                        if (Tester_Serial(OM_baseCpta) == false)
                        {
                            OM_baseCpta.Close();
                            throw new Exception("Vous n'êtes pas autorisé(e) à) l'usage de cette application. Contacter votre Administrateur");
                        }
                        //   base commerciale ouverte, active l'indicateur
                        if (OM_baseCpta.IsOpen)
                        {
                            IsOpen = true;
                            flgConnexionSAGE = true;
                            //Commun.Journalise("BaseSage-Connexion", "Connexion SQL établie à la base " + CompanyDatabaseName);
                        }
                        else
                        { Commun.Journalise("BaseSage-Connexion", "Connexion à " + this.Nom_Dossier + " non effectuée - Essai " + NbEssais.ToString()); }
                    }
                    catch (Exception E)
                    { Commun.Journalise("BaseSage-Connexion", "ERREUR connexion SQL à la base " + this.SAGE_base + " : " + E.Message); }
                }
            }
            catch (Exception ex)
            { Commun.Journalise("BaseSage-Connexion", "ERREUR : Connexion à la base SAGE " + this.SAGE_base + " : " + ex.Message); }

            return this;
        }

        private BaseSage Connexion_BaseSage_Cial()
        {
            Boolean flgConnexionSAGE = false;   //   Indicateur de connexion
            int NbEssais = 0;                   //   nombre d'essais

            try
            {
                //   Tant que la connexion n'est pas active et que le nombre d'essais ne dépasse pas 3
                while (flgConnexionSAGE == false & NbEssais <= 3)
                {
                    //   Incrémente compteur d'essais
                    NbEssais += 1;
                    //   Fermeture si redondance
                    if (this.IsOpen)
                    {
                        OM_baseCial.Close();
                        this.IsOpen = false;
                    }

                    try
                    {

                        OM_baseCial.CompanyServer = this.SAGE_serveur;
                        OM_baseCial.CompanyDatabaseName = this.SAGE_base;
                        OM_baseCial.Loggable.UserName = String.IsNullOrEmpty(this.SAGE_User) ? "<Administrateur>" : this.SAGE_User;
                        OM_baseCial.Loggable.UserPwd = String.IsNullOrEmpty(this.SAGE_Pwd) ? "" : this.SAGE_Pwd;

                        OM_baseCial.Open();

                        //   base commerciale ouverte, active l'indicateur
                        if (Tester_Serial(OM_baseCial.CptaApplication) == false)
                        {
                            OM_baseCial.Close();
                            throw new Exception("Vous n'êtes pas autorisé(e) à) l'usage de cette application. Contacter votre Administrateur");
                        }
                        //   base commerciale ouverte, active l'indicateur
                        if (OM_baseCial.IsOpen)
                        {
                            if (OM_baseCial.CptaApplication != null)
                                this._om_baseCpta = OM_baseCial.CptaApplication;

                            IsOpen = true;
                            flgConnexionSAGE = true;
                           // Commun.Journalise("BaseSage-Connexion", "Connexion SQL établie à la base " + CompanyDatabaseName);
                        }
                        else
                        { Commun.Journalise("BaseSage-Connexion", "Connexion à " + this.SAGE_base + " non effectuée - Essai " + NbEssais.ToString()); }
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("BaseSage-Connexion", "ERREUR connexion SQL à la base " + this.SAGE_base + " : " + E.Message + Environment.NewLine + E.StackTrace + Environment.NewLine + E.TargetSite);
                        //Commun.Journalise("BaseSage-Connexion", this.Raccourci);
                        //Commun.Journalise("BaseSage-Connexion", String.IsNullOrEmpty(this.SAGE_User) ? "<Administrateur>" : this.SAGE_User);
                            // Commun.Journalise("BaseSage-Connexion", String.IsNullOrEmpty(this.SAGE_Pwd) ? "" : this.SAGE_Pwd);
                    }
                }
            }
            catch (Exception ex)
            { Commun.Journalise("BaseSage-Connexion", "ERREUR : Connexion à la base SAGE " + this.SAGE_base + " : " + ex.Message); }
            return this;
        }

        internal static Boolean Tester_Serial(BSCIALApplication100c baseAppli)

            {

            if (Serialmatch == "")
                return true;
            foreach (ILicenceProduct prod in baseAppli.Licence.Products)
            {
                if (prod.SerialNumber == Serialmatch)
                    return true;
            }

            if (baseAppli.Licence.Product.SerialNumber == Serialmatch)
                return true;

            return false;
        }

        internal static Boolean Tester_Serial(BSCPTAApplication100c baseAppli)

        {
            if (Serialmatch == "")
                return true;
            foreach (ILicenceProduct prod in baseAppli.Licence.Products)
            {
                if (prod.SerialNumber == Serialmatch)
                    return true;
            }
            
                if (baseAppli.Licence.Product.SerialNumber == Serialmatch)
                return true;

            return false;
        }

#endregion Connexion

#region Information raccourci
       
        /// <summary>
        /// A partir du raccourci, lit le contenu et retourne l'instance SQL qui est paramétrée
        /// </summary>
        /// <param name="RacourciSage">raccourci de l'application</param>
        /// <returns>Nom de l'instance SQL </returns>
        public static String getNomInstanceServeur(String RaccourciSage)
        // --------------------------------------------------------------------------------
        //   getNomInstanceServeur
        //   19.07.2019
        //   PROJECT SI
        //   J. BENOIT
        // --------------------------------------------------------------------------------
        //   Récupère le nom de l'instance SQL d'une base Sage 
        // --------------------------------------------------------------------------------
        {
            String nomInstanceServeurSQL = "";

            try
            {
                using (StreamReader fichierAlias = new StreamReader(RaccourciSage, true))
                    while (fichierAlias.EndOfStream == false)
                    {
                        String ligneFichier = fichierAlias.ReadLine();
                        if (ligneFichier.Contains("ServeurSQL="))
                            nomInstanceServeurSQL = ligneFichier.Replace("ServeurSQL=", "");
                        if (nomInstanceServeurSQL.EndsWith("\\"))
                            nomInstanceServeurSQL = nomInstanceServeurSQL.Replace("\\", "");
                    }
            }
            catch (Exception ex)
            { Commun.Journalise("getNomInstanceServeur", ex.Message); }

            return nomInstanceServeurSQL;
        }
        /// <summary>
        /// Retourne le nom de la base (nom du fichier raccourci
        /// </summary>
        /// <param name="RaccourciSage">raccourci de l'application</param>
        /// <returns>Nom de la base sQL</returns>
        public static String getNomBase(String RaccourciSage)
        // --------------------------------------------------------------------------------
        //   getNomBaseCompta
        //   19.07.2019
        //   PROJECT SI
        //   J. BENOIT
        // --------------------------------------------------------------------------------
        //   Récupère le nom de la base SQL d'une base Sage Paie
        // --------------------------------------------------------------------------------
        {
            FileInfo FI = new FileInfo(RaccourciSage);
            if (FI.Exists == false)
                return "";
            else
            {
                return FI.Name.Replace(FI.Extension, "").Replace(".", "").ToUpper();
            }
        }
        /// <summary>
        /// Constitue la chaine de connexion SQL
        /// </summary>
        /// <param name="CheminRaccourciSage">raccourci de l'application</param>
        /// <returns>Chaine de connexion SQL</returns>        

        private  String getChaineConnexion()
        {
            String chaineConnexion = String.Empty;              // Chaine de connexion à retourner
            String nomInstanceServeurSQL = String.Empty;        // Nom de l'instance du serveur SQL
            String nomBaseSQL = String.Empty;                   // Nom de la base SQL

            try
            {
                //   Recherche le nom de l'instance SQL dans le fichier SAGE
                nomInstanceServeurSQL = getNomInstanceServeur(this.Raccourci);
                //   Recherche le nom la base SQL dans le nom du fichier SAGE
                nomBaseSQL = getNomBase(this.Raccourci);
                //   Si l'instance et la base sont renseignées, constitution de la chaine de connexion
                if (String.IsNullOrEmpty(nomInstanceServeurSQL) == false & String.IsNullOrEmpty(nomBaseSQL) == false)
                    chaineConnexion = "Server=" + nomInstanceServeurSQL + ";Trusted_Connection=yes;Database=" + nomBaseSQL;
            }
            catch (Exception E)
            {
                Commun.Journalise("", "Erreur fatale : " + E.Message + "||" + E.StackTrace);
                if (E.InnerException != null)
                {
                    Commun.Journalise("", "Détails Erreur  : " + E.InnerException.Message + "||" + E.InnerException.StackTrace);
                }
            }

            return chaineConnexion;
        }
        public String Get_ChaineConnexion(String RaccourciSage = "")         // Utilisation par objet SQL pour requêtage à la volée
        {
            
            if (OM_baseCpta.IsOpen == true)
                return "server=" + OM_baseCpta.DatabaseInfo.ServerName + ";Trusted_Connection=yes;database=" + OM_baseCpta.DatabaseInfo.DatabaseName;
            else if (RaccourciSage != "")
            {
                return getChaineConnexion();
            }
            else
                return "";

        }

#endregion Information raccourci

#region méthodes statique (info énuméré)

        public static DocumentType DocumentIndexToTypeOM(int indexDocument)
        {
            // Retourne le type de document OM (ex: DocumentType.DocumentTypeVenteCommande) correspondant à l'index(ex: 1) :
            switch (indexDocument)
            {
                case 0: return DocumentType.DocumentTypeVenteDevis;
                case 1: return DocumentType.DocumentTypeVenteCommande;
                case 2: return DocumentType.DocumentTypeVentePrepaLivraison;
                case 3: return DocumentType.DocumentTypeVenteLivraison;
                case 4: return DocumentType.DocumentTypeVenteReprise;
                case 5: return DocumentType.DocumentTypeVenteAvoir;
                case 6: return DocumentType.DocumentTypeVenteFacture;
                case 7: return DocumentType.DocumentTypeVenteFactureCpta;
                case 8: return DocumentType.DocumentTypeVenteArchive;

                case 11: return DocumentType.DocumentTypeAchatCommande;
                case 12: return DocumentType.DocumentTypeAchatCommandeConf;
                case 13: return DocumentType.DocumentTypeAchatLivraison;
                case 14: return DocumentType.DocumentTypeAchatReprise;
                case 15: return DocumentType.DocumentTypeAchatAvoir;
                case 16: return DocumentType.DocumentTypeAchatFacture;
                case 17: return DocumentType.DocumentTypeAchatFactureCpta;
                case 18: return DocumentType.DocumentTypeAchatArchive;

                case 20: return DocumentType.DocumentTypeStockMouvIn;
                case 21: return DocumentType.DocumentTypeStockMouvIn;
                case 22: return DocumentType.DocumentTypeStockDeprec;
                case 23: return DocumentType.DocumentTypeStockVirement;
                case 24: return DocumentType.DocumentTypeStockPreparation;
                case 25: return DocumentType.DocumentTypeStockOrdreFabrication;
                case 26: return DocumentType.DocumentTypeStockFabrication;
                case 27: return DocumentType.DocumentTypeStockArchive;

                case 40: return DocumentType.DocumentTypeInterne1;
                case 41: return DocumentType.DocumentTypeInterne2;
                case 42: return DocumentType.DocumentTypeInterne3;
                case 43: return DocumentType.DocumentTypeInterne4;
                case 44: return DocumentType.DocumentTypeInterne5;
                case 45: return DocumentType.DocumentTypeInterne6;

                default: throw new Exception("Type de document '" + indexDocument.ToString() + "' invalide !");
            }
        }
        public static int DocumentTypeOMToIndex(DocumentType OM_typeDocument)
        {
            // Retourne l'index de document(ex: 7) correspondant à son type Objets Métiers(ex: DocumentTypeVenteFactureCpta) :
            switch (OM_typeDocument)
            {
                case DocumentType.DocumentTypeVenteDevis: return 0;
                case DocumentType.DocumentTypeVenteCommande: return 1;
                case DocumentType.DocumentTypeVentePrepaLivraison: return 2;
                case DocumentType.DocumentTypeVenteLivraison: return 3;
                case DocumentType.DocumentTypeVenteReprise: return 4;
                case DocumentType.DocumentTypeVenteAvoir: return 5;
                case DocumentType.DocumentTypeVenteFacture: return 6;
                case DocumentType.DocumentTypeVenteFactureCpta: return 7;
                case DocumentType.DocumentTypeVenteArchive: return 8;

                case DocumentType.DocumentTypeAchatCommande: return 11;
                case DocumentType.DocumentTypeAchatCommandeConf: return 12;
                case DocumentType.DocumentTypeAchatLivraison: return 13;
                case DocumentType.DocumentTypeAchatReprise: return 14;
                case DocumentType.DocumentTypeAchatAvoir: return 15;
                case DocumentType.DocumentTypeAchatFacture: return 16;
                case DocumentType.DocumentTypeAchatFactureCpta: return 17;
                case DocumentType.DocumentTypeAchatArchive: return 18;

                case DocumentType.DocumentTypeStockMouvIn: return 20;
                case DocumentType.DocumentTypeStockMouvOut: return 21;
                case DocumentType.DocumentTypeStockDeprec: return 22;
                case DocumentType.DocumentTypeStockVirement: return 23;
                case DocumentType.DocumentTypeStockPreparation: return 24;
                case DocumentType.DocumentTypeStockOrdreFabrication: return 25;
                case DocumentType.DocumentTypeStockFabrication: return 26;
                case DocumentType.DocumentTypeStockArchive: return 27;

                case DocumentType.DocumentTypeInterne1: return 40;
                case DocumentType.DocumentTypeInterne2: return 41;
                case DocumentType.DocumentTypeInterne3: return 42;
                case DocumentType.DocumentTypeInterne4: return 43;
                case DocumentType.DocumentTypeInterne5: return 44;
                case DocumentType.DocumentTypeInterne6: return 45;
                case DocumentType.DocumentTypeInterneArchive: return 46;

                default: throw new Exception("Type de document '" + OM_typeDocument.ToString() + "' invalide !");
            }
        }
        public static string OMToIntitule(DocumentType OM_typeDocument)
        {
            // Retourne l'intitulé de document(ex: Facture comptabilisée) correspondant à son type Objets Métiers(ex: DocumentTypeVenteFactureCpta) :
            switch (OM_typeDocument)
            {
                case DocumentType.DocumentTypeVenteDevis: return "Devis";
                case DocumentType.DocumentTypeVenteCommande: return "Bon de commande";
                case DocumentType.DocumentTypeVentePrepaLivraison: return "Préparation de livraison";
                case DocumentType.DocumentTypeVenteLivraison: return "Bon de livraison";
                case DocumentType.DocumentTypeVenteReprise: return "Bon de retour";
                case DocumentType.DocumentTypeVenteAvoir: return "Bon d'avoir";
                case DocumentType.DocumentTypeVenteFacture: return "Facture";
                case DocumentType.DocumentTypeVenteFactureCpta: return "Facture comptabilisée";
                case DocumentType.DocumentTypeVenteArchive: return "Archive";

                case DocumentType.DocumentTypeAchatCommande: return "Préparation de commande";
                case DocumentType.DocumentTypeAchatCommandeConf: return "Bon de commande";
                case DocumentType.DocumentTypeAchatLivraison: return "Bon de livraison";
                case DocumentType.DocumentTypeAchatReprise: return "Bon de retour";
                case DocumentType.DocumentTypeAchatAvoir: return "Bon d'avoir financier";
                case DocumentType.DocumentTypeAchatFacture: return "Facture";
                case DocumentType.DocumentTypeAchatFactureCpta: return "Facture comptabilisée";
                case DocumentType.DocumentTypeAchatArchive: return "Archive";

                case DocumentType.DocumentTypeStockMouvIn: return "Mouvement d'entrée";
                case DocumentType.DocumentTypeStockMouvOut: return "Mouvement de sortie";
                case DocumentType.DocumentTypeStockDeprec: return "Dépréciation de stock";
                case DocumentType.DocumentTypeStockVirement: return "Mouvement de transfert";
                case DocumentType.DocumentTypeStockPreparation: return "Préparation de fabrication";
                case DocumentType.DocumentTypeStockOrdreFabrication: return "Ordre de fabrication";
                case DocumentType.DocumentTypeStockFabrication: return "Bon de fabrication";
                case DocumentType.DocumentTypeStockArchive: return "Archive";

                case DocumentType.DocumentTypeInterne1: return "Interne 1";
                case DocumentType.DocumentTypeInterne2: return "Interne 2";
                case DocumentType.DocumentTypeInterne3: return "Interne 3";
                case DocumentType.DocumentTypeInterne4: return "Interne 4";
                case DocumentType.DocumentTypeInterne5: return "Interne 5";
                case DocumentType.DocumentTypeInterne6: return "Interne 6";
                case DocumentType.DocumentTypeInterneArchive: return "Archive";

                default: throw new Exception("Type de document '" + OM_typeDocument.ToString() + "' invalide !");
            }
        }
        public static String DocumentDomaineOMToIntitule(DomaineType OM_domaineDocument)
        {
            // Retourne l'intitulé de domaine(ex: Ventes) correspondant à son type Objets Métiers(ex: DomaineType.DomaineTypeVente) :

            switch (OM_domaineDocument)
            {
                case DomaineType.DomaineTypeVente: return "Ventes";
                case DomaineType.DomaineTypeAchat: return "Achats";
                case DomaineType.DomaineTypeStock: return "Stocks";
                case DomaineType.DomaineTypeInterne: return "Internes";
                case DomaineType.DomaineTypeTicket: return "Tickets";

                default: throw new Exception("Domaine de document '" + OM_domaineDocument.ToString() + "' invalide !");
            }
        }
        public static String DocumentStatutOMToIntitule(DocumentStatutType OM_statutDocument)
        {
            // Retourne l'intitulé de statut du document(ex: Saisi) correspondant à son type Objets Métiers(ex: DomaineType.DomaineTypeVente) :
            switch (OM_statutDocument)
            {
                case DocumentStatutType.DocumentStatutTypeSaisie: return "Saisi";
                case DocumentStatutType.DocumentStatutTypeConfirme: return "Confirmé";
                case DocumentStatutType.DocumentStatutTypeAPrepare: return "A préparer/A livrer/A facturer";

                default: throw new Exception("Statut de document '" + OM_statutDocument.ToString() + "' invalide !");
            }
        }
        public static object[] OM_CollectionToArray(IBICollection OM_collection)
        {
            // Converti une collection Objets Métiers (IBICollection) en tableau générique (T) :
            List<object> OM_Objets = new List<object>();

            foreach (object OM_objet in OM_collection)
            { OM_Objets.Add(OM_objet); }

            return OM_Objets.ToArray();
        }
        public static  String SuiviStockOMToIntitule(SuiviStockType OM_SuiviStock)
        {
            /* --------------------------------------------------------------------------------
            *   SuiviStockOMToIntitule
            *   Retourne l'intitulé de domaine(ex: Ventes) correspondant à son type Objets Métiers(ex: DomaineType.DomaineTypeVente) :
            *   27.08.2012
            *   PROJECT SI
            *   J. PAUTRAT
            * --------------------------------------------------------------------------------*/
            switch (OM_SuiviStock)
            {
                case SuiviStockType.SuiviStockTypeAucun: return "Aucun";
                case SuiviStockType.SuiviStockTypeCmup: return "CMUP";
                case SuiviStockType.SuiviStockTypeFifo: return "FIFO";
                case SuiviStockType.SuiviStockTypeLifo: return "LIFO";
                case SuiviStockType.SuiviStockTypeLot: return "par Lot";
                case SuiviStockType.SuiviStockTypeSerie: return "Sérialisé";

                default: throw new Exception("Suivi de stock '" + OM_SuiviStock.ToString() + "' invalide !");
            }
        }

#endregion méthodes statique (info énuméré)

#region méthodes publiques Spécifiques
        public IBOClientLivraison3 GetAdresse(IBOClient3 FClient, string sIntitule)
        {
            foreach (IBOClientLivraison3 FAdr in FClient.FactoryClientLivraison.List)
            {
                if (FAdr.LI_Intitule == sIntitule)
                { return FAdr; }
            }
            return null;
        }

        public IBOCompteG3 Get_ClientCompteG(IBOClient3 omclient, String Code_CompteG_Client)
        {
            
            foreach (IBOCompteG3 cpt in omclient.FactoryTiersCompteG.List)
            {
                if (cpt.CG_Num == Code_CompteG_Client)
                    return cpt;
            }
            return null;
        }

        public IBOTiersContact3 Get_ClientContact(IBOClient3 omclient, String Contact_Nom_Prenom)
        {
            foreach (IBOTiersContact3 elt in omclient.FactoryTiersContact.List)
            {
                if ( (elt.Nom + ' ' + elt.Prenom) == Contact_Nom_Prenom)
                    return elt;
            }
            return null;
        }
        public IBOClient3 Get_ClientBy_IdExterne(String Id_externe, String Col_RefExt)
        {
            String Ct_Num = Get_Ctnum_By_IdExterne(Id_externe, Col_RefExt);

            if (Ct_Num != "")
                return this.OM_baseCpta.FactoryClient.ReadNumero(Ct_Num);
            return null;
        }

        public String Get_Ctnum_By_IdExterne( String Id_externe, String Col_RefExt)
        {
            String Ct_Num = "";
            String req = "SELECT CT_Num FROM F_COMPTET WHERE [" + Col_RefExt + "] = '" + Id_externe + "' and Ct_Type = 0";

            using (SqlConnection Connexion = new SqlConnection(this.getChaineConnexion()))
            {
                Connexion.Open();
                try
                {
                    //  Connexion à la base 
                    using (SqlCommand cmd = new SqlCommand(req, Connexion))
                    using (SqlDataReader rs = cmd.ExecuteReader())
                    {
                        if (rs.HasRows)
                        {
                            rs.Read();
                            IDataRecord data = (IDataRecord)rs;
                            Ct_Num = data[0].ToString();

                            if (rs.Read() == true)
                                Commun.Journalise("Base_Sage Get_ClientBy_IdExterne", "ALERTE : il existe plusieurs client SAGE pour eCimenCode = " + Id_externe + " , Le premier a été sélectionné (" + Ct_Num + ").");
                        }
                        rs.Close();
                    }

                }
                catch (Exception ex)
                { Commun.Journalise("Base_Sage Get_ClientBy_IdExterne", "Erreur : Tentative de création des tables Spécifiques : " + ex.Message); }
                finally
                { Connexion.Close(); }
                return Ct_Num;
            }
        }
        #endregion méthodes publiques Spécifiuqes

        #region Gestion table et objet Spé
        /// <summary>
        /// Controle l'existence de la table spécifique des événements
        /// </summary>
        /// <param name="Connexion">Connexion SQL à la base de paie</param>
        /// <returns>True si existe, False si non ou erreur</returns>
        /// <remarks></remarks>
        public static Boolean test_Existance_Table_SPE(String ConnectString, String nom_table_spe = "*")
        // --------------------------------------------------------------------------------
        //   test_tableTarifException
        //   14.09.2020
        //   PROJECT SI
        //   J. BENOIT
        // --------------------------------------------------------------------------------
        //   Controle l'existence de la table spécifique 
        //   Si n'existe pas, elle est créée
        // --------------------------------------------------------------------------------       
        {
            if (String.IsNullOrEmpty(ConnectString) == true)
                return false;

            String tmpModule = "test_table_spe";
            Boolean flgOk = false;

            if (nom_table_spe == "*")
            {
                test_Existance_Table_SPE(ConnectString, "ZZ_Send_Async_Message");
                return true;
            }


            int nbTable = 0;
            String chSql = "SELECT COUNT(*) FROM sys.objects WHERE name = '" + nom_table_spe + "'";

            using (SqlConnection Connexion = new SqlConnection(ConnectString))
            {
                Connexion.Open();
                try
                {
                    //  Connexion à la base 
                    using (SqlCommand cmd = new SqlCommand(chSql, Connexion))
                    {
                        //Lecture des données de la requête
                        using (SqlDataReader rs = cmd.ExecuteReader())
                        {
                            if (rs.HasRows)
                            {
                                rs.Read();
                                IDataRecord data = (IDataRecord)rs;
                                nbTable = Int16.Parse(data[0].ToString());
                            }
                            rs.Close();
                        }
                    }

                    //   Si table trouvée
                    if (nbTable != 0)
                        flgOk = true;
                    else
                        CreationTable_Spe(ConnectString, nom_table_spe);
                }
                catch (Exception ex)
                {

                    Commun.Journalise(tmpModule, "Erreur : Tentative de création des tables Spécifiques : " + ex.Message);

                }
                finally
                { Connexion.Close(); }
            }
            return flgOk;
        }

        /// <summary>
        /// Création de la table spécifique de journalisation
        /// </summary>
        /// <param name="Connexion">Connexion SQL à la base</param>
        /// <param name="nom_table_spe">objet à crééer</param>
        /// <returns>True si traitement effectué, False si erreur</returns>
        /// <remarks></remarks>
        public static Boolean CreationTable_Spe(String ConnectString, String nom_table_spe)
        // --------------------------------------------------------------------------------
        //   CreationTableSpe
        //   14/12/2020
        //   PROJECT SI
        //   J. BENOIT
        // --------------------------------------------------------------------------------
        //   Création de table spécifique 
        // --------------------------------------------------------------------------------       
        {
            String tmpModule = "CreationTable_Spe";
            Boolean flgOk = false;

            String chSql = "";
            using (SqlConnection Connexion = new SqlConnection(ConnectString))
            {
                Connexion.Open();
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {//   définition de la connexion
                        cmd.Connection = Connexion;
                        //   paramètres de la requête
                        cmd.CommandText = "SET ANSI_NULLS ON";
                        cmd.ExecuteNonQuery();
                        //   paramètres de la requête
                        cmd.CommandText = "SET QUOTED_IDENTIFIER ON";
                        cmd.ExecuteNonQuery();
                        //   paramètres de la requête
                        cmd.CommandText = "SET ANSI_PADDING ON";
                        cmd.ExecuteNonQuery();
                        switch (nom_table_spe.ToUpper())//   Création de la table
                        {
                            case "ZZ_SEND_ASYNC_MESSAGE":
                                chSql = "CREATE  PROCEDURE [dbo].[ZZ_Send_Async_Message] @Msg varchar(MAX) ,@LstUSer varchar(MAX) /*WITH EXECUTE AS 'USER_CBASE'   */    " +
                                        "                                                                                                                                                    " +
                                        "   AS                                                                                                                                               " +
                                        "   BEGIN                                                                                                                                            " +
                                        "                                                                                                                                                    " +
                                        "       SET NOCOUNT ON;                                                                                                                              " +
                                        "                                                                                                                                                    " +
                                        "                               /*Création table de mémorisation des message si n'existe pas          */                                               " +
                                        "                                                                                                                                                    " +
                                        "       IF not exists(select object_id from sys.tables where name = 'ZZ_Async_Message' )                                                             " +
                                        "                                                                                                                                                    " +
                                        "       BEGIN                                                                                                                                        " +
                                        "           CREATE TABLE[dbo].[ZZ_Async_Message](                                                                                                    " +
                                        "                                                                                                                                                    " +
                                        "               [Msg][varchar](max) NULL,                                                                                                            " +
                                        "                                                                                                                                                    " +
                                        "               [Date] [datetime] NULL                                                                                                               " +
                                        "			) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY];                                                                                                     " +
                                        "       END                                                                                                                                          " +
                                        "                                                                                                                                                    " +
                                        "                                                                                                                                                    " +
                                        "       declare @spid smallint;                                                                                                                      " +
                                        "                                                                                                                                                    " +
                                        "		/* test si l'utilisateur est connecté    */                                                                                                    " +
                                        "		SELECT TOP 1 @spid = spid FROM sys.sysprocesses JOIN cbUserSession ON spid = cbSession AND cbUserName in (@LstUSer) WHERE dbid = DB_ID();    " +
                                        "       IF @Msg<> ''                                                                                                                                 " +
                                        "		BEGIN                                                                                                                                        " +
                                        "           IF isnull(@spid,'' )  <> ''                                                                                                              " +
                                        "				BEGIN                                                                                                                                " +
                                        "					/*SI OUI, envoi direct du message       */                                                                                        " +
                                        "                   EXEC CB_SendMessage @spid, @Msg;                                                                                                 " +
                                        "       END                                                                                                                                          " +
                                        "   ELSE                                                                                                                                             " +
                                        "                                                                                                                                                    " +
                                        "               BEGIN                                                                                                                                " +
                                        "					/* SINON, mémorisation du message   */                                                                                             " +
                                        "                   INSERT INTO dbo.ZZ_Async_Message(Msg, date) values(@Msg, SYSDATETIME() )                                                         " +
                                        "                                                                                                                                                    " +
                                        "               END                                                                                                                                  " +
                                        "       END	                                                                                                                                         " +
                                        "		/*SI utilisateur connecté, envoi de tous les message mémorisés   */                                                                           " +
                                        "                                                                                                                                                    " +
                                        "       IF isnull(@spid,'') <> ''                                                                                                                    " +
                                        "		BEGIN                                                                                                                                        " +
                                        "                                                                                                                                                    " +
                                        "           declare @msg_async varchar(MAX),                                                                                                         " +
                                        "					@date datetime;                                                                                                                  " +
                                        "                                                                                                                                                    " +
                                        "       DECLARE cMessages CURSOR LOCAL FOR SELECT Msg, date FROM dbo.ZZ_Async_Message                                                                " +
                                        "       OPEN cMessages;                                                                                                                              " +
                                        "			FETCH NEXT FROM cMessages INTO @msg_async, @Date;                                                                                        " +
                                        "       WHILE @@FETCH_STATUS = 0                                                                                                                     " +
                                        "			BEGIN                                                                                                                                    " +
                                        "                                                                                                                                                    " +
                                        "				/* Send async message   */                                                                                                             " +
                                        "               set @Msg = CONCAT('Message du ', CONVERT(VARCHAR, @Date,23) ,' : ',   @msg_async);                                                   " +
                                        "				EXEC CB_SendMessage @spid, @Msg ;                                                                                                    " +
                                        "                                                                                                                                                    " +
                                        "				/* delete from saved message     */                                                                                                    " +
                                        "                                                                                                                                                    " +
                                        "               Delete dbo.ZZ_Async_Message where Msg = @msg_async and Date = @date;                                                                 " +
                                        "                                                                                                                                                    " +
                                        "       FETCH NEXT FROM cMessages INTO @msg_async, @Date;                                                                                            " +
                                        "       END                                                                                                                                          " +
                                        "       CLOSE cMessages;                                                                                                                             " +
                                        "			DEALLOCATE cMessages;                                                                                                                    " +
                                        "       END                                                                                                                                          " +
                                        "   END                                                                                                                                              " +
                                        "";// GO";
                                break;
                            
                            default:
                                chSql = "";
                                break;
                        }

                        cmd.CommandText = chSql;
                        cmd.ExecuteNonQuery();
                        //   Désactivation du paramètre de la requête
                        cmd.CommandText = "SET ANSI_PADDING OFF";
                        cmd.ExecuteNonQuery();
                    }
                    flgOk = true;
                }
                catch (Exception ex)
                {
                    Commun.Journalise(tmpModule, ex.Message);
                    flgOk = false;
                }
                finally
                { Connexion.Close(); }
            }
            return flgOk;
        }

#endregion Gestion table et objet Spé

#region Procédure Spé
        public void Send_Sage_Notification(String txt, String UserName = "<Administrateur>")
        {
            // Test si existence de la procédure + tentative création
            if (test_Existance_Table_SPE(this.getChaineConnexion(),"ZZ_Send_Async_Message") == false)
                throw new NotImplementedException("Impossible d'envoyer une notification, la base SAGE ne le prends pas en charge.");

            try
            {
                // Appel de la procédure stockée de création des échéances
                SqlConnection sqlConn = new SqlConnection(this.getChaineConnexion());
                SqlCommand sqlCMD = new SqlCommand("ZZ_Send_Async_Message", sqlConn);
                sqlCMD.CommandType = CommandType.StoredProcedure;


                // Définition des paramètres
                SqlParameter Msg_Param = sqlCMD.Parameters.Add("@Msg", SqlDbType.VarChar);
                Msg_Param.Direction = ParameterDirection.Input;
                SqlParameter User_Param = sqlCMD.Parameters.Add("@LstUSer", SqlDbType.VarChar);
                User_Param.Direction = ParameterDirection.Input;

                // qualification des paramètres
                Msg_Param.Value = txt;
                User_Param.Value = UserName;

                sqlConn.Open();

                sqlCMD.ExecuteNonQuery();
            }
            catch (Exception E)
            {
                // Raise Error
                throw E;
            }
        }
#endregion region Procédure Spé

#region IDisposable Support
        public void Close()
        {
            this.Dispose(false);
            /*
            if (OM_baseCial.CptaApplication != null)
            { OM_baseCial.CptaApplication.Close(); }   // A valider si nécessaire 
            if (OM_baseCial != null) { OM_baseCial.Close(); }
            */
        }

        private bool disposedValue = false; // Pour détecter les appels redondants

       protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (OM_baseCpta != null) { OM_baseCpta.Close(); }   
                    if (OM_baseCial != null) { OM_baseCial.Close(); }

                }
                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

#endregion
        /// Libération de ressources */
    }
}

