﻿using System;
using Microsoft.Win32.TaskScheduler;
using System.Threading.Tasks;
using System.IO;
using System.Security.AccessControl;
using System.Windows.Forms;

namespace Interface_SAGE.Helper
{
    public class Planif
    {
        public static void Create_XML_To_Import(string task_name,string FicParam, int repeat_h, int repeat_m, int start_h, int start_m, int duree_h)
        {
            String description = "";
            switch (task_name)
                {
                case "Intégration_DOC_SAGE":
                    description = "Interrogotion API eCiment et Intégration Périodique  Document dans SAGE";
                    break;
                case "Post_Statuts_DOC_SAGE":
                    description = "Remontée statut Documents de Sage vers API eCiment";
                    break;
                case "Post_client_SAGE":
                    description = "Remontée Modification Fiches clients de Sage vers API eCiment";
                    break;
                default:
                    return;
            }
            // Get the service on the local machine
            using (TaskService ts = new TaskService())
            {

                // Create a new task definition and assign properties
                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = description;

                // Create a trigger that will fire the task at this time every other day
                DailyTrigger Trigger = new DailyTrigger { DaysInterval = 1 };
                RepetitionPattern repetition = new RepetitionPattern(new TimeSpan(repeat_h, repeat_m, 0), TimeSpan.FromHours(duree_h), true);
                Trigger.Repetition = repetition;
                Trigger.StartBoundary = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, start_h, start_m, 0);

                td.Triggers.Add(Trigger);
                td.Principal.UserId = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                td.Principal.LogonType = TaskLogonType.Password;
                // Create an action that will launch Notepad whenever the trigger fires
                td.Actions.Add(new ExecAction(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + AppDomain.CurrentDomain.SetupInformation.ApplicationName, "\"" + FicParam + "\" \"1\"", null));

                String destFic = AppDomain.CurrentDomain.BaseDirectory + task_name +".xml";
                File.WriteAllText(destFic, td.XmlText);

                using (OpenFileDialog dialogsel = new OpenFileDialog())
                {
                    dialogsel.Reset();
                    dialogsel.Title = "Fichier XML à importer dans le planificateur de tâches";
                    dialogsel.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    dialogsel.FileName = task_name + ".xml";
                    dialogsel.Filter = "*.*|*.*";
                    dialogsel.FilterIndex = 0;
                    dialogsel.ShowDialog();
                    dialogsel.Dispose();
                }
            }
        }


        public static void Create_IntegreDoc(string FicParam, int repeat_h, int repeat_m, int start_h, int start_m, int duree_h)
        {

            // Get the service on the local machine
            using (TaskService ts = new TaskService())
            {

                // Create a new task definition and assign properties
                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = "Interrogotion API eCiment et Intégration Périodique  Document dans SAGE";

                // Create a trigger that will fire the task at this time every other day
                DailyTrigger Trigger = new DailyTrigger { DaysInterval = 1 };
                RepetitionPattern repetition = new RepetitionPattern(new TimeSpan(repeat_h, repeat_m, 0), TimeSpan.FromHours(duree_h), true);
                Trigger.Repetition = repetition;
                Trigger.StartBoundary = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, start_h, start_m, 0);

                td.Triggers.Add(Trigger);
                td.Principal.UserId = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                td.Principal.LogonType = TaskLogonType.Password;
                // Create an action that will launch Notepad whenever the trigger fires
                td.Actions.Add(new ExecAction(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + AppDomain.CurrentDomain.SetupInformation.ApplicationName, "\"" + FicParam + "\" \"1\"", null));

                String destFic = AppDomain.CurrentDomain.BaseDirectory + @"Intégration_DOC_SAGE.xml";
                File.WriteAllText(destFic, td.XmlText);

                using (OpenFileDialog dialogsel = new OpenFileDialog())
                {
                    dialogsel.Reset();
                    dialogsel.Title = "Fichier XML à importer dans le planificateur de tâches";
                    dialogsel.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    dialogsel.FileName = "Intégration_DOC_SAGE.xml";
                    dialogsel.Filter = "*.*|*.*";
                    dialogsel.FilterIndex = 0;
                    dialogsel.ShowDialog();
                    dialogsel.Dispose();
                }
            }
        }

        public static void Create_PostStatutDoc(string FicParam, int repeat_h, int repeat_m, int start_h, int start_m, int duree_h)
        {
            // Get the service on the local machine
            using (TaskService ts = new TaskService())
            {
                
                // Create a new task definition and assign properties
                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = "Remontée statut Documents de Sage vers API eCiment";

                // Create a trigger that will fire the task at this time every other day
                DailyTrigger Trigger = new DailyTrigger { DaysInterval = 1 };
                RepetitionPattern repetition = new RepetitionPattern(new TimeSpan(repeat_h, repeat_m, 0), TimeSpan.FromHours(duree_h), true);
                Trigger.Repetition = repetition;
                Trigger.StartBoundary = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, start_h, start_m, 0);

                td.Triggers.Add(Trigger);
                td.Principal.UserId = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                td.Principal.LogonType = TaskLogonType.Password;
                // Create an action that will launch Notepad whenever the trigger fires
                td.Actions.Add(new ExecAction(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + AppDomain.CurrentDomain.SetupInformation.ApplicationName, "\"" + FicParam+ "\" \"2\"", null));

                String destFic = AppDomain.CurrentDomain.BaseDirectory + @"Post_Statuts_DOC_SAGE.xml";
                File.WriteAllText(destFic, td.XmlText);

                using (OpenFileDialog dialogsel = new OpenFileDialog())
                {
                    dialogsel.Reset();
                    dialogsel.Title = "Fichier XML à importer dans le planificateur de tâches";
                    dialogsel.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    dialogsel.FileName = "Post_Statuts_DOC_SAGE.xml";
                    dialogsel.Filter = "*.*|*.*";
                    dialogsel.FilterIndex = 0;
                    dialogsel.ShowDialog();
                    dialogsel.Dispose();
                }
            }
        }

        public static void Delete_Task(String Name)
        {
            // Get the service on the local machine
            using (TaskService ts = new TaskService())
            {
                // Remove the task 
                ts.RootFolder.DeleteTask(Name);
            }
        }
    }
}