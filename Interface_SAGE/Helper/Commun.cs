﻿using Objets100cLib;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface_SAGE.Helper
{
    public static class Commun
    {

            /// <summary>
            /// Mise à jour du journal de traitement + de l'affichage. Insère la chaine TexteJournal horodatée .
            /// </summary>
            /// <param name="NomModule">Indicateur de lieu utiliser seulement en mode DEBUG</param>
            /// <param name="TexteJournal">Message à journaliser</param>
            /// <param name="txBox">Objet Text à complépter</param>
            /// <param name="Fl_Alerte">Si true, affichage de messageBox</param>
        public static void Journalise(string NomModule, string TexteJournal,TextBox txBox = null, bool Fl_Alerte = false)
        {
            if (Fl_Alerte == true)
                MessageBox.Show(TexteJournal, "Erreur - " + NomModule);

            String Rec = "";
            String Msg = "";

            //   Ouverture du journal
            // Journalisation sur chaque poste, using (StreamWriter strmLog = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Interface.log", true, Encoding.UTF8, 1024))
            using (StreamWriter strmLog = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\Interface.log", true, Encoding.UTF8, 1024))
            {
                //Création de l'enregistrement
                Msg = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                Rec = Msg + "|" + (NomModule + new String(' ', 30)).Substring(0, 30) + "|" + TexteJournal;
                Msg = Msg + "-" + TexteJournal + Environment.NewLine;
                //   Impression dans le journal
                strmLog.WriteLine(Rec);
                // Fermeture
                strmLog.Close();
            }

            //   Affichage
            if (txBox != null)
            {
                txBox.AppendText(Msg);
                txBox.Refresh();
            }
        }

            /// <summary>
            /// Calcule le numéro de mode de règlement Sage
            /// </summary>
            /// <param name="LibelleOriginal">CHEQUE,VIREMENT,ESPECES...</param>
            /// <param name="NbreEcheances">à spécifier si CCARTE BLEUE ou PRELEVEMENT</param>
            /// <returns></returns>
            public static int NumeroReglement(string LibelleOriginal, int NbreEcheances)
            {

                int tmpNum = 1;

                switch (LibelleOriginal.ToUpper())
                {
                    case "CHÈQUE":
                    case "CHEQUE":
                        tmpNum = 1;                     // Chèque
                        break;
                    case "VIREMENT":
                        tmpNum = 2;                      // Virement
                        break;
                    case "ESPECES":
                    case "ESPÈCES":
                        tmpNum = 3;                     // Expèces
                        break;
                    case "CARTE BLEUE":
                        switch (NbreEcheances)
                        {
                            case 4:
                                tmpNum = 5;             // Carte bancaire X4
                                break;
                            case 12:
                                tmpNum = 6;              // Carte bancaire X12
                                break;
                            default:
                                tmpNum = 4;              // Carte bancaire X1
                                break;
                        }
                        break;
                    case "PRELEVEMENT":
                    case "PRÉLÈVEMENT":
                        switch (NbreEcheances)
                        {
                            case 4:
                                tmpNum = 9;              // Prélèvement X4
                                break;
                            case 12:
                                tmpNum = 10;             // Prélèvement X12
                                break;
                            default:
                                tmpNum = 7;              // Prélèvement X1
                                break;
                        }
                        break;
                    default:
                        tmpNum = 1;                      // Chèque par défaut
                        break;
                }

                return tmpNum;

            }
            /// <summary>
            /// Calcule le nombre de jours entre DateDebut et DateFin
            /// </summary>
            /// <param name="DateDebut"></param>
            /// <param name="DateFin"></param>
            /// <returns>nombre de jour entier, négatif si date debut est postérieure</returns>
            public static int NombreJours(DateTime DateDebut, DateTime DateFin)
            {
                return (DateFin - DateDebut).Days;
            }

        public static Char Get_Char(String str)
        {
            if (str.Contains('\\') == false)
            {
                if ( str.Length> 0 )
                    return str.ToCharArray()[0];
                else
                    return '¯';
            }
            else
            {
                Char SpecialChar = str.Replace("\\", "").FirstOrDefault<char>(); ;
                switch (SpecialChar)
                {

                    case '"': return '\"';      // for a double quote.
                    case '\\': return '\\';         // for a backslash.
                    case '0': return '\0';          // for a null character.
                    case 'a': return '\a';          //for an alert character.
                    case 'b': return '\b';          // for a backspace.
                    case 'f': return '\f';          //for a form feed.  
                    case 'n': return '\n';          // for a new line.
                    case 'r': return '\r';          // for a carriage return.
                    case 't': return '\t';          // for a horizontal tab.
                    case 'v': return '\v';          // for a vertical tab.

                    // \uxxxx for a unicode character hex value(e.g. \u0020).
                    case 'u': throw new Exception("Impossible de traduire le caractère : " + str);
                    // \x is the same as \u, but you don't need leading zeroes (e.g. \x20).
                    case 'x': throw new Exception("Impossible de traduire le caractère : " + str);
                    // \Uxxxxxxxx for a unicode character hex value(longer form needed for generating surrogates).
                    case 'U': throw new Exception("Impossible de traduire le caractère : " + str);

                    default:
                        return '¯';
                }
            }

        }

        public static DateTime String_to_Date(String strDate, String FormatPrioritaire = "")
        {
            DateTime resultDate = new DateTime();

            if (String.IsNullOrEmpty(FormatPrioritaire) == false)
                if (DateTime.TryParseExact(strDate, FormatPrioritaire, CultureInfo.CurrentCulture, DateTimeStyles.None, out resultDate) == true)
                    return resultDate;

            if (DateTime.TryParseExact(strDate, new string[] { "dd/MM/yyyy", "dd-MM-yyyy", "dd MM yyyy", "yyyy/MM/dd", "yyyy-MM-dd", "yyyy MM dd", "ddMMyy", "ddMMyyyy", "yyMMdd", "yyyyMMdd" }, CultureInfo.CurrentCulture, DateTimeStyles.None, out resultDate) == false)
                return resultDate;

            return resultDate;
        }
    }
}

