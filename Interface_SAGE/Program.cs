﻿using Interface_SAGE.Core;
using Interface_SAGE.Forms;
using Interface_SAGE.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface_SAGE
{
    static class Program
    {
        private static MainProgram Core;
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static async Task Main(String[] args)
        {
            if (args.Length > 1)
            {
               // Commun.Journalise("BATCH","args0:"+ args[0]+ "|args[1]:"+ args[1] + "|");
                Core = new MainProgram(args[0]);
                
                switch (args[1])
                {
                    // Chemin de paramètre + echanges WOOCOMMERCE
                    case "1":
                        Commun.Journalise("BATCH", "CRON Woocomerce lancé");
                        Core.WOO_ImportDocumentsSAGE(Verbose: !Core.GetParametres.Prod);     // empêché la création d'accompte selon le paramètre
                        await Core.WOO_ExportDeliverySAGE();
                        break;
                    // Chemin de paramètre + echanges FUTURLOG
                    case "2":
                        Commun.Journalise("BATCH", "CRON Futurlog lancé");
                        await Core.Futur_AchatSend();
                        await Core.Futur_AchatGet();
                        await Core.Futur_VenteSend();
                        await Core.Futur_VenteGet();
                        break;
                    // Chemin de paramètre + CRON PRODUITS
                    case "3":
                        Commun.Journalise("BATCH", "CRON Produit lancé");
                        await Core.Futur_stockGet();
                        await Core.Futur_ProductSend();
                        Core.WOO_ExportArticleSAGE();
                        break;
                    case "WOO_ImportDocumentsSAGE": Core.WOO_ImportDocumentsSAGE();
                        break;
                    case "WOO_ExportDeliverySAGE":
                        await Core.WOO_ExportDeliverySAGE();
                        break;
                    case "WOO_ExportArticleSAGE": Core.WOO_ExportArticleSAGE();
                        break;
                    case "Futur_AchatSend": await Core.Futur_AchatSend();
                            break;
                    case "Futur_AchatGet": await Core.Futur_AchatGet();
                        break;
                    case "Futur_VenteSend": await Core.Futur_VenteSend();
                        break;
                    case "Futur_VenteGet": await Core.Futur_VenteGet();
                        break;
                    case "Futur_stockGet": await Core.Futur_stockGet();
                        break;
                    case "Futur_ProductSend": await Core.Futur_ProductSend();
                        break;
                    default:

                        // Erreur paramétrage
                        MessageBox.Show("Les paramètres reçu sont incorrects.\n\n Contacter votre administrateur.", "ERREUR", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        
                        // Ouverture interface utilisateur pour paramétrage
                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);
                        Application.Run(new FormMain(Core));

                        break;
                }

            }
            else if (args.Length == 1 )
            {
                Core = new MainProgram(args[0]);
                // Chemin fichier paramètre seul
                // Ouverture interface utilisateur pour paramétrage
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FormMain(Core));
            }
            else
            {
                Core = new MainProgram();
                // demande sans argument : interface utilisateur pour paramétrage par défaut
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FormMain(Core));
            }
}
    }
}
