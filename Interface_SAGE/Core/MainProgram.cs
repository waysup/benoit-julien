﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objets100cLib;
using Interface_SAGE.Helper;
using Interface_SAGE.Tasks;
using Interface_SAGE.Entities;

namespace Interface_SAGE.Core
{
    public class MainProgram
    {
        #region propriétés publique
        public Boolean Automate;
        #endregion propriétés publique

        #region Singletons ou Manager
        public BaseSage baseSage;
        public Webap WebAPI;
        #endregion Singletons ou Manager

        public MainProgram(String CheminFichierParam = "",Boolean automate = false)
        {
            Init_Param(CheminFichierParam);
            Connect_Sage();
            Define_API();
            Automate = automate;
        }

        #region gestion Initialisation et Parametres
        public String GetCheminFichierParametres { get => DataProtection.Get_DataParam().Get_ParamFilePath(); }
        public Parametres GetParametres { get => DataProtection.Get_DataParam().parametres; }
        public void Init_Param(String CheminFichierParam)
        {
            DataProtection.Get_DataParam(CheminFichierParam);
        }

        public void Connect_Sage()
        {
            baseSage = BaseSage.Connexion_BaseSage(this);
        }

        public void Define_API()
        {
            WebAPI = Webap.Get_Webap(this);
        }


       
       public void SaveParam(FileInfo fichierJson)
        {
            DataProtection.Get_DataParam().SaveParam(fichierJson);
        }
        public void LoadParam(String fichierJson)
        {
            DataProtection.Get_DataParam().Set_ParamFilePath(fichierJson);
            Connect_Sage();
            // TODO vérifier que les liaisons  modifiées de l'API sont correctes 
            // sinon  la méthode Define_API();  doit être appellée et modifiée pour corriger les propriétés de CORE à la volée, ou recharger CORE.
        }

        #endregion gestion Initialisation et Parametres

        #region Fonctions implémentées
        public async Task Futur_VenteSend(String JSonFake = "", int mode = 0, Boolean Verbose = false)
        {
            Futur_VenteSend Futur_sendDoc = new Futur_VenteSend(this, JSonFake, Verbose);
            await Futur_sendDoc.ExportDoc(mode);
        }
        public async Task Futur_VenteGet(String JSonFake = "", int mode = 0, Boolean Verbose = false)
        {
            Futur_VenteGet Futur_GetDoc = new Futur_VenteGet(this, JSonFake, Verbose);
            await Futur_GetDoc.ImportDoc(mode);
        }
        public async Task Futur_ProductSend(String JSonFake = "", int mode = 0, Boolean Verbose = false)
        {
            Futur_ProductSend Exp_SAGE = new Futur_ProductSend(this, JSonFake, Verbose);
            await Exp_SAGE.ProductSend(mode);
        }
        public async Task Futur_stockGet(String JSonFake = "", int mode = 0, Boolean Verbose = false)
        {
            Futur_stockGet Futur_GetStock = new Futur_stockGet(this, JSonFake, Verbose);
            await Futur_GetStock.ImportStock(mode);
        }
        public async Task Futur_AchatSend(String JSonFake = "", int mode = 0, Boolean Verbose = false)
        {
            Futur_AchatSend Exp_SAGE = new Futur_AchatSend(this, JSonFake, Verbose);
            await Exp_SAGE.ExportAchat(mode);
        }
        public async Task Futur_AchatGet(String JSonFake = "", int mode = 0, Boolean Verbose = false)
        {
            Futur_AchatGet Imp_Sage = new Futur_AchatGet(this, JSonFake, Verbose);
            await Imp_Sage.ImportAchat(mode);
        }


        public void WOO_ImportDocumentsSAGE( int mode = 0, Boolean Verbose = false)
        {
            WOO_ImportOrder Imp_SAGE = new WOO_ImportOrder(this,  Verbose);
            Imp_SAGE.Recup_File_FTP();
            Imp_SAGE.Import_Document();
        }
        public void WOO_ExportArticleSAGE( int mode = 0, Boolean Verbose = false)
        {
            WOO_ExportArticle Exp_SAGE = new WOO_ExportArticle(this, Verbose);
            Exp_SAGE.Export_Article();
            Exp_SAGE.Send_File_FTP();
        }
        public async Task WOO_ExportDeliverySAGE(String JSonFake = "", int mode = 0, Boolean Verbose = false)
        {
            WOO_ExportDelivery Exp_SAGE = new WOO_ExportDelivery(this, JSonFake,Verbose);
            await Exp_SAGE.Export_Delivery();
        }
        #endregion Fonctions implémentées


    }
}

