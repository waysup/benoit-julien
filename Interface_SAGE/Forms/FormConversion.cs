﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Interface_SAGE.Helper;
using Interface_SAGE.Core;
using Interface_SAGE.Formats;

namespace Interface_SAGE.Forms
{
    public partial class FormConversion : Form
    {
        public MainProgram Core;
        public DataTable DT;
        private Boolean Donnees_modifiees;
        public String nomFicConv;
        private Dictionary<Type, List<Propriété>> liste_correspondance;

        public FormConversion(ref String NomFicConv, MainProgram core)
        {
            InitializeComponent();
            txtFicConv.Text = NomFicConv;
            nomFicConv = NomFicConv;
            this.Core = core;
            
        }

        private void FormConversion_Load(object sender, EventArgs e)
        {
            // chargement de la combobox
            /*cbentitie.Items.Add("WebClient");
            cbentitie.Items.Add("WebCommande");
            cbentitie.Items.Add("WebProduit");
            cbentitie.Items.Add("WebLivraison");
            cbentitie.Items.Add("WebPaiement");
            cbentitie.Items.Add("WebTaxes");
            cbentitie.Items.Add("WebAdresseLivraison");
            cbentitie.Items.Add("WebAdresseFacturation");
            */

            Donnees_modifiees = false;

            // Récupération de la liste via la classe dédiée 
            liste_correspondance = Liste_Correspondance.Get_Liste_Correspondance(Core).Liste_correspondance;
            foreach (var elt in liste_correspondance)
            {
                cbentitie.Items.Add(elt.Key);
            }
            
            if (String.IsNullOrEmpty(txtFicConv.Text) == false)
            {
                DT = new DataTable();
                DT.Columns.Add("TYPE");
                DT.Columns.Add("Nom");
                DT.Columns.Add("Val_Ori");
                DT.Columns.Add("Val_Dest");
                

                try
                {
                    foreach (var elt in liste_correspondance)
                        foreach (Propriété prop in elt.Value)
                            if (prop.Conversion_autorisee == true)
                                foreach (var conv in prop.Liste_conversion)
                                    DT.Rows.Add(elt.Key.Name, prop.Nom_Propriété, conv.Key, conv.Value);
                }
                catch (Exception E)
                {
                    MessageBox.Show("Erreur inatendue : " + E.Message);
                }
            }

            cbentitie.SelectedIndex = 0;

            // Définition de la combo liste des prorpiétés avec conversion autorisée
            // lancer sur l'évènement  cbentitie.SelectedIndexChanged Definir_format_tableau();

        }



        private void Definir_format_tableau()
        {
            DataGridViewComboBoxColumn cbColumn = new DataGridViewComboBoxColumn();
            {
                cbColumn.DataPropertyName = "Nom";
                cbColumn.HeaderText = "Données";
                cbColumn.DropDownWidth = 160;
                cbColumn.Width = 90;
                cbColumn.MaxDropDownItems = 10;
                cbColumn.FlatStyle = FlatStyle.Flat;
                cbColumn.DisplayMember = "Name";
                
            }
            
            Type t = (Type)cbentitie.SelectedItem;
            if (liste_correspondance.TryGetValue(t, out List<Propriété> lst) == true)
            {
                foreach (Propriété prop in lst)
                {
                    if (prop.Conversion_autorisee == true)
                        cbColumn.Items.Add(prop.Nom_Propriété);
                }   
            }
            else
                throw new ArgumentException(((Type)cbentitie.SelectedItem).Name + " Non supporté");

            DataGridViewTextBoxColumn TypeColumn = new DataGridViewTextBoxColumn();
            {
                TypeColumn.DataPropertyName = "TYPE";
                TypeColumn.HeaderText = "TYPE";
                TypeColumn.Visible = false;
            }
            DataGridViewTextBoxColumn OriColumn = new DataGridViewTextBoxColumn();
            {
                OriColumn.DataPropertyName = "Val_Ori";
                OriColumn.HeaderText = "Valeur Origine";
            }
            DataGridViewTextBoxColumn DestColumn = new DataGridViewTextBoxColumn();
            {
                DestColumn.DataPropertyName = "Val_Dest";
                DestColumn.HeaderText = "Valeur Destination";
            }


            //cbColumn.SelectedIndex = 0; 
            DGConv.Columns.Clear();
            DGConv.Columns.Add(TypeColumn);
            DGConv.Columns.Add(cbColumn);
            DGConv.Columns.Add(OriColumn);
            DGConv.Columns.Add(DestColumn);

            DGConv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }
        private void Charge_Tableau()
        {

            // initialisation du tableau
            //DGConv.Rows.Clear();

            DataView DV = new DataView(DT, "TYPE = '" + ((Type)cbentitie.SelectedItem).Name + "'","",DataViewRowState.CurrentRows);
           // DV.RowFilter = "";
            DGConv.DataSource = DV;
            
        }

        private void Refresh_Tableau()
        {
/*
            // initialisation du tableau
            DGConv.Rows.Clear();
            DataTable DT = new DataTable();

            try
            {
                foreach (var elt in liste_correspondance)
                {
                    foreach (Propriété prop in elt.Value)
                    {
                        if (prop.Conversion_autorisee == true)
                            foreach (var conv in prop.Liste_conversion)
                            {
                                DT.Rows.Add(elt.Key.Name, prop.Nom_Propriété, conv.Value, conv.Key);
                            }
                    }
                }

                DataView DV = new DataView(DT, "TYPE = " + cbentitie.SelectedItem.ToString(), "Nom DESC", DataViewRowState.CurrentRows);
                DGConv.DataSource = DV;

            }
            catch (Exception E)
            {
                MessageBox.Show("Erreur inatendue : " + E.Message);
            }
*/
        }

        private void Sauvegarde_tableau()
        {
            try
            {
                // Initialisation des conversion
                foreach (var elt in liste_correspondance)
                        foreach (Propriété prop in elt.Value)
                            if (prop.Conversion_autorisee == true)
                                prop.Liste_conversion.Clear();

                // Ré affection des conversion
                foreach (DataRow row in DT.Rows)
                {
                    Boolean trouve = false;
                    foreach (var elt in liste_correspondance)
                    {
                        if (row.Field<String>("TYPE") == elt.Key.Name)
                        {
                            foreach (Propriété prop in elt.Value)
                                if (row.Field<String>("Nom") == prop.Nom_Propriété)
                                {
                                    prop.Liste_conversion.Add(row.Field<String>("Val_Ori") != null? row.Field<String>("Val_Ori") : "" , row.Field<String>("Val_Dest"));
                                    trouve = true;
                                    break;
                                }
                        }
                        if (trouve == true)
                            break;
                    }
                }

                // Sauvegarde du nom de fichier dans paramétrage
                Liste_Correspondance.Get_Liste_Correspondance(Core).Sauve_Liste_correspondance(liste_correspondance, txtFicConv.Text);

                // Désactiver Alerte en fermeture
                Donnees_modifiees = false;
            }
            catch (Exception E)
            {
                MessageBox.Show("Erreur inatendue : " + E.Message);
            }
        }

        private void btOpenFicConv_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialogsel = new OpenFileDialog())
            {
                dialogsel.Reset();
                dialogsel.Title = "Sélectionner le fichier de conversion" ;
                if (String.IsNullOrEmpty(txtFicConv.Text) == false)
                    dialogsel.FileName = txtFicConv.Text;
                else
                    dialogsel.FileName = "*.csv";
                dialogsel.Filter = "Fichier plat CSV (*.csv)|*.csv|Tous les fichiers (*.*)|*.*";
                dialogsel.FilterIndex = 0;
                if (dialogsel.ShowDialog() == DialogResult.OK)
                {
                    if (txtFicConv.Text != dialogsel.FileName)
                        Donnees_modifiees = true;

                    txtFicConv.Text = dialogsel.FileName;
                    Charge_Tableau();

                }
                dialogsel.Dispose();
            }
        }

        private void btSaveFicConv_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dialogsel = new SaveFileDialog())
            {
                dialogsel.Reset();
                dialogsel.Title = "Sauvegarder dans le fichier";
                if (String.IsNullOrEmpty(txtFicConv.Text) == false)
                    dialogsel.FileName = txtFicConv.Text;
                else
                    dialogsel.FileName = "*.csv";
                dialogsel.Filter = "Fichier plat CSV (*.csv)|*.csv|Tous les fichiers (*.*)|*.*";
                dialogsel.FilterIndex = 0;
                if (dialogsel.ShowDialog() == DialogResult.OK)
                {
                    txtFicConv.Text = dialogsel.FileName;
                    Sauvegarde_tableau();
                    Donnees_modifiees = false;
                }
                dialogsel.Dispose();
            }
        }

        private void DGConv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Donnees_modifiees = true;
        }

        private void FormConversion_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Donnees_modifiees == true)
            {
                if (DialogResult.No == MessageBox.Show("Quitter sans sauvegarder les changements ?", "ALERTE", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2))
                    e.Cancel = true;
            }
        }

        private void cbentitie_SelectedIndexChanged(object sender, EventArgs e)
        {
            DT.AcceptChanges();

            Definir_format_tableau();
            Charge_Tableau();
            DT.Columns["TYPE"].DefaultValue = ((Type)cbentitie.SelectedItem).Name;
        }

        private void DGConv_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (String.IsNullOrEmpty((String)DGConv.CurrentRow.Cells["TYPE"].Value) == true)
                //DGConv.CurrentRow.Cells["Type"].Value = ((Type)cbentitie.SelectedItem).Name;
        }

        private void DGConv_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            //DT.AcceptChanges();
            var s = DGConv.CurrentRow.State;
            
        }

        private void DGConv_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {

        }
    }
}
