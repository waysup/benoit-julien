﻿namespace Interface_SAGE.Forms
{
    partial class FormConversion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConversion));
            this.btSaveFicConv = new System.Windows.Forms.Button();
            this.DGConv = new System.Windows.Forms.DataGridView();
            this.txtFicConv = new System.Windows.Forms.TextBox();
            this.btOpenFicConv = new System.Windows.Forms.Button();
            this.cbentitie = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGConv)).BeginInit();
            this.SuspendLayout();
            // 
            // btSaveFicConv
            // 
            this.btSaveFicConv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSaveFicConv.Image = global::Interface_SAGE.Properties.Resources.Save;
            this.btSaveFicConv.Location = new System.Drawing.Point(773, 57);
            this.btSaveFicConv.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btSaveFicConv.Name = "btSaveFicConv";
            this.btSaveFicConv.Size = new System.Drawing.Size(36, 36);
            this.btSaveFicConv.TabIndex = 7;
            this.btSaveFicConv.UseVisualStyleBackColor = true;
            this.btSaveFicConv.Click += new System.EventHandler(this.btSaveFicConv_Click);
            // 
            // DGConv
            // 
            this.DGConv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGConv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGConv.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.DGConv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGConv.GridColor = System.Drawing.SystemColors.ControlLight;
            this.DGConv.Location = new System.Drawing.Point(23, 107);
            this.DGConv.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.DGConv.Name = "DGConv";
            this.DGConv.Size = new System.Drawing.Size(787, 607);
            this.DGConv.TabIndex = 6;
            this.DGConv.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGConv_RowEnter);
            this.DGConv.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGConv_RowValidated);
            this.DGConv.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DGConv_RowValidating);
            // 
            // txtFicConv
            // 
            this.txtFicConv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFicConv.Location = new System.Drawing.Point(23, 58);
            this.txtFicConv.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.txtFicConv.Name = "txtFicConv";
            this.txtFicConv.Size = new System.Drawing.Size(662, 29);
            this.txtFicConv.TabIndex = 5;
            // 
            // btOpenFicConv
            // 
            this.btOpenFicConv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btOpenFicConv.Image = global::Interface_SAGE.Properties.Resources.Open;
            this.btOpenFicConv.Location = new System.Drawing.Point(716, 57);
            this.btOpenFicConv.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btOpenFicConv.Name = "btOpenFicConv";
            this.btOpenFicConv.Size = new System.Drawing.Size(36, 36);
            this.btOpenFicConv.TabIndex = 4;
            this.btOpenFicConv.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btOpenFicConv.UseVisualStyleBackColor = true;
            this.btOpenFicConv.Click += new System.EventHandler(this.btOpenFicConv_Click);
            // 
            // cbentitie
            // 
            this.cbentitie.FormattingEnabled = true;
            this.cbentitie.Location = new System.Drawing.Point(100, 10);
            this.cbentitie.Name = "cbentitie";
            this.cbentitie.Size = new System.Drawing.Size(278, 29);
            this.cbentitie.TabIndex = 8;
            this.cbentitie.SelectedIndexChanged += new System.EventHandler(this.cbentitie_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Entité :";
            // 
            // FormConversion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 733);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbentitie);
            this.Controls.Add(this.btSaveFicConv);
            this.Controls.Add(this.DGConv);
            this.Controls.Add(this.txtFicConv);
            this.Controls.Add(this.btOpenFicConv);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormConversion";
            this.Text = "Tableau des correspondances ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConversion_FormClosing);
            this.Load += new System.EventHandler(this.FormConversion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGConv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSaveFicConv;
        private System.Windows.Forms.TextBox txtFicConv;
        private System.Windows.Forms.Button btOpenFicConv;
        private System.Windows.Forms.ComboBox cbentitie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DGConv;
    }
}