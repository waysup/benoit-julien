﻿
using Interface_SAGE.Core;
using System;
using System.IO;
using System.Linq;

using System.Windows.Forms;

namespace Interface_SAGE.Forms
{
    public partial class FormMain : Form
    {
        private MainProgram Core;

        #region Load
        public FormMain(MainProgram core)
        {
            Core = core;
            InitializeComponent();
            Update_Label();

            cbMode.Items.Add("Normal");
            cbMode.Items.Add("Exportation sans horodatage");
            cbMode.Items.Add("Ré-exporter tout (sans horodatage)");
            cbMode.SelectedIndex = 0;

            rbSaveAPI.Checked = true;

            ckVerbose.Checked = ! Core.GetParametres.Prod;
        }


        public void Update_Label()
        {
            if (Core.baseSage.IsOpen)
            {
                /* Mise à jour des élément pour rafraichissement */

                toolStripStatusLabel1.Text = "Connexion base  " + Core.baseSage.Nom_Dossier + " OK.";
            }
            else
            {
                toolStripStatusLabel1.Text = "Connexion non établie !";
            }
            toolStripStatusLabel2.Text = "Fichier Paramètres : " + Core.GetCheminFichierParametres;
            toolStripStatusLabel3.Text = "Fichier LOG : " + AppDomain.CurrentDomain.BaseDirectory + @"\Interface.log";
        }
     
        #endregion region load


        #region Evenements
        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }
        private void paramètresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            

        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Core.baseSage.IsOpen == true)
                Core.baseSage.Close();
            this.Close();
        }

       

        private void intégrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        
    





        #endregion Mode Auto

        #region Bouton
        private void btLOG_Click(object sender, EventArgs e)
        {
            txtMessage.Clear();

            try
            {
                String[] lignes = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + @"\Interface.log");

                int compt_ini = 0;
                if (lignes.Count() > 500 )
                    compt_ini = lignes.Count() - 500;
                
                for(int i = lignes.Count()-1;  i >= compt_ini; i--)
                {
                    txtMessage.Text += lignes[i] + Environment.NewLine;
                }
                
            }
            catch (Exception E )
            { MessageBox.Show("Erreur chargement :" + E.Message); }
        }

        private void btOpenLOG_Click(object sender, EventArgs e)
        {
            try
            { System.Diagnostics.Process.Start(AppDomain.CurrentDomain.BaseDirectory + @"\Interface.log"); }
            catch
            { MessageBox.Show("Impossible d'ouvrir le fichier."); }
        }

        // WOO Commerce exchanges
        private void btImportWOO_Click(object sender, EventArgs e)
        {
            Core.WOO_ImportDocumentsSAGE(mode: cbMode.SelectedIndex, ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btExportArtWOO_Click(object sender, EventArgs e)
        {
            Core.WOO_ExportArticleSAGE(mode: cbMode.SelectedIndex, ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btExportDocWOO_Click(object sender, EventArgs e)
        {
            Core.WOO_ExportDeliverySAGE(JSonFake: txJSONFile.Text, mode: cbMode.SelectedIndex, ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }

        // FUTURLOG Exchanges
        private void btFuturExportDoc_Click(object sender, EventArgs e)
        {
            Core.Futur_VenteSend(JSonFake: txJSONFile.Text, mode: cbMode.SelectedIndex, Verbose: ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btFuturGETShipment_Click(object sender, EventArgs e)
        {
            Core.Futur_VenteGet(JSonFake: txJSONFile.Text, mode:cbMode.SelectedIndex, Verbose: ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btFuturGetProducts_Click(object sender, EventArgs e)
        {
            Core.Futur_stockGet(JSonFake: txJSONFile.Text, mode: cbMode.SelectedIndex, Verbose: ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btFuturExportProduct_Click(object sender, EventArgs e)
        {
            Core.Futur_ProductSend(mode: cbMode.SelectedIndex, JSonFake: txJSONFile.Text, Verbose: ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btFuturSendOrderPurchase_Click(object sender, EventArgs e)
        {
            Core.Futur_AchatSend(JSonFake: txJSONFile.Text, mode: cbMode.SelectedIndex, Verbose: ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btFuturGetOrderPurchase_Click(object sender, EventArgs e)
        {
            Core.Futur_AchatGet(JSonFake: txJSONFile.Text, mode: cbMode.SelectedIndex, Verbose: ckVerbose.Checked);
            this.btLOG_Click(null, null);
        }
        private void btParam_Click(object sender, EventArgs e)
        {
            // Appel de la fenêtre paramètres
            FormParam wParam = new FormParam(Core);
            wParam.Show(this);
            Update_Label();
        }


        #endregion bouton

        private void btJsonFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialogsel = new OpenFileDialog())
            {
                dialogsel.Reset();
                dialogsel.Title = "Sélectionner le Fichier Jsonde travail";
                if (String.IsNullOrEmpty(txJSONFile.Text) == false)
                {
                    dialogsel.InitialDirectory = Path.GetDirectoryName(txJSONFile.Text);
                    dialogsel.FileName = Path.GetFileName( txJSONFile.Text);
                }
                else
                {
                    dialogsel.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    dialogsel.FileName = " ";
                }
                   
                dialogsel.Filter = "Fichier Json (*.json)|*.json|Fichier Texte(*.txt)|*.txt";
                dialogsel.FilterIndex = 0;
                if (dialogsel.ShowDialog() == DialogResult.OK)
                    txJSONFile.Text = dialogsel.FileName;
                dialogsel.Dispose();
            }
        }

    }
}
