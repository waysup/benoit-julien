﻿namespace Interface_SAGE.Forms
{
    partial class FormMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsMenuParam = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paramètresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traitementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intégrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.aProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btFuturSendOrderPurchase = new System.Windows.Forms.Button();
            this.btFuturExportProduits = new System.Windows.Forms.Button();
            this.btFuturGetReceipt = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btFuturGetProducts = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btFuturSendOrder = new System.Windows.Forms.Button();
            this.btFuturGetShipment = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbSaveAPI = new System.Windows.Forms.RadioButton();
            this.rbFakeAPI = new System.Windows.Forms.RadioButton();
            this.btJsonFile = new System.Windows.Forms.Button();
            this.txJSONFile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ckVerbose = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbMode = new System.Windows.Forms.ComboBox();
            this.btOpenLOG = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btImportWOO = new System.Windows.Forms.Button();
            this.btExportDocWOO = new System.Windows.Forms.Button();
            this.btLOG = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btExportArtWOO = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuParam,
            this.toolStripMenuItem10});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(11, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(1200, 40);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsMenuParam
            // 
            this.tsMenuParam.Name = "tsMenuParam";
            this.tsMenuParam.Size = new System.Drawing.Size(122, 32);
            this.tsMenuParam.Text = "Paramètres";
            this.tsMenuParam.Click += new System.EventHandler(this.btParam_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(35, 32);
            this.toolStripMenuItem10.Text = "?";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paramètresToolStripMenuItem,
            this.toolStripSeparator1,
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // paramètresToolStripMenuItem
            // 
            this.paramètresToolStripMenuItem.Name = "paramètresToolStripMenuItem";
            this.paramètresToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.paramètresToolStripMenuItem.Text = "Paramètres";
            this.paramètresToolStripMenuItem.Click += new System.EventHandler(this.paramètresToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(162, 6);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // traitementToolStripMenuItem
            // 
            this.traitementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.intégrationToolStripMenuItem});
            this.traitementToolStripMenuItem.Name = "traitementToolStripMenuItem";
            this.traitementToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.traitementToolStripMenuItem.Text = "Traitement";
            // 
            // intégrationToolStripMenuItem
            // 
            this.intégrationToolStripMenuItem.Name = "intégrationToolStripMenuItem";
            this.intégrationToolStripMenuItem.Size = new System.Drawing.Size(325, 26);
            this.intégrationToolStripMenuItem.Text = "Intégration Complète du répertoire";
            this.intégrationToolStripMenuItem.Click += new System.EventHandler(this.intégrationToolStripMenuItem_Click);
            // 
            // aideToolStripMenuItem
            // 
            this.aideToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.aProposToolStripMenuItem});
            this.aideToolStripMenuItem.Name = "aideToolStripMenuItem";
            this.aideToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.aideToolStripMenuItem.Text = "Aide";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(152, 26);
            this.toolStripMenuItem2.Text = "?";
            // 
            // aProposToolStripMenuItem
            // 
            this.aProposToolStripMenuItem.Name = "aProposToolStripMenuItem";
            this.aProposToolStripMenuItem.Size = new System.Drawing.Size(152, 26);
            this.aProposToolStripMenuItem.Text = "A Propos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.statusStrip1.Location = new System.Drawing.Point(0, 643);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 26, 0);
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(1200, 101);
            this.statusStrip1.Stretch = false;
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(1172, 25);
            this.toolStripStatusLabel1.Text = "Connect DB statut";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(1172, 25);
            this.toolStripStatusLabel2.Text = "FicParam Info";
            this.toolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(1172, 25);
            this.toolStripStatusLabel3.Text = "Fic LOG";
            this.toolStripStatusLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStatus
            // 
            this.lblStatus.Margin = new System.Windows.Forms.Padding(0, 3, 0, 2);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(27, 17);
            this.lblStatus.Text = "Etat";
            // 
            // txtMessage
            // 
            this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessage.Font = new System.Drawing.Font("Consolas", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMessage.Location = new System.Drawing.Point(0, 0);
            this.txtMessage.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMessage.Size = new System.Drawing.Size(1200, 360);
            this.txtMessage.TabIndex = 2;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 40);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.btOpenLOG);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.btLOG);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtMessage);
            this.splitContainer1.Size = new System.Drawing.Size(1200, 603);
            this.splitContainer1.SplitterDistance = 234;
            this.splitContainer1.SplitterWidth = 9;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btFuturSendOrderPurchase);
            this.panel2.Controls.Add(this.btFuturExportProduits);
            this.panel2.Controls.Add(this.btFuturGetReceipt);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.btFuturGetProducts);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.btFuturSendOrder);
            this.panel2.Controls.Add(this.btFuturGetShipment);
            this.panel2.Location = new System.Drawing.Point(3, 93);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(675, 125);
            this.panel2.TabIndex = 27;
            // 
            // btFuturSendOrderPurchase
            // 
            this.btFuturSendOrderPurchase.Location = new System.Drawing.Point(353, 11);
            this.btFuturSendOrderPurchase.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btFuturSendOrderPurchase.Name = "btFuturSendOrderPurchase";
            this.btFuturSendOrderPurchase.Size = new System.Drawing.Size(150, 50);
            this.btFuturSendOrderPurchase.TabIndex = 27;
            this.btFuturSendOrderPurchase.Text = "Export Cdes Achats";
            this.btFuturSendOrderPurchase.UseVisualStyleBackColor = true;
            this.btFuturSendOrderPurchase.Click += new System.EventHandler(this.btFuturSendOrderPurchase_Click);
            // 
            // btFuturExportProduits
            // 
            this.btFuturExportProduits.Location = new System.Drawing.Point(512, 11);
            this.btFuturExportProduits.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btFuturExportProduits.Name = "btFuturExportProduits";
            this.btFuturExportProduits.Size = new System.Drawing.Size(150, 50);
            this.btFuturExportProduits.TabIndex = 25;
            this.btFuturExportProduits.Text = "Exportation Produits";
            this.btFuturExportProduits.UseVisualStyleBackColor = true;
            this.btFuturExportProduits.Click += new System.EventHandler(this.btFuturExportProduct_Click);
            // 
            // btFuturGetReceipt
            // 
            this.btFuturGetReceipt.Location = new System.Drawing.Point(353, 66);
            this.btFuturGetReceipt.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btFuturGetReceipt.Name = "btFuturGetReceipt";
            this.btFuturGetReceipt.Size = new System.Drawing.Size(150, 50);
            this.btFuturGetReceipt.TabIndex = 28;
            this.btFuturGetReceipt.Text = "Récupérer Réceptions";
            this.btFuturGetReceipt.UseVisualStyleBackColor = true;
            this.btFuturGetReceipt.Click += new System.EventHandler(this.btFuturGetOrderPurchase_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 30);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 28);
            this.label7.TabIndex = 26;
            this.label7.Text = "FUTURLOG";
            // 
            // btFuturGetProducts
            // 
            this.btFuturGetProducts.Location = new System.Drawing.Point(512, 66);
            this.btFuturGetProducts.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btFuturGetProducts.Name = "btFuturGetProducts";
            this.btFuturGetProducts.Size = new System.Drawing.Size(150, 50);
            this.btFuturGetProducts.TabIndex = 25;
            this.btFuturGetProducts.Text = "Récupérer Stock";
            this.btFuturGetProducts.UseVisualStyleBackColor = true;
            this.btFuturGetProducts.Click += new System.EventHandler(this.btFuturGetProducts_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 28);
            this.label5.TabIndex = 23;
            this.label5.Text = "Traitement";
            // 
            // btFuturSendOrder
            // 
            this.btFuturSendOrder.Location = new System.Drawing.Point(191, 11);
            this.btFuturSendOrder.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btFuturSendOrder.Name = "btFuturSendOrder";
            this.btFuturSendOrder.Size = new System.Drawing.Size(150, 50);
            this.btFuturSendOrder.TabIndex = 22;
            this.btFuturSendOrder.Text = "Export Cdes Ventes";
            this.btFuturSendOrder.UseVisualStyleBackColor = true;
            this.btFuturSendOrder.Click += new System.EventHandler(this.btFuturExportDoc_Click);
            // 
            // btFuturGetShipment
            // 
            this.btFuturGetShipment.Location = new System.Drawing.Point(191, 66);
            this.btFuturGetShipment.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btFuturGetShipment.Name = "btFuturGetShipment";
            this.btFuturGetShipment.Size = new System.Drawing.Size(150, 50);
            this.btFuturGetShipment.TabIndex = 24;
            this.btFuturGetShipment.Text = "Récupérer Livraisons";
            this.btFuturGetShipment.UseVisualStyleBackColor = true;
            this.btFuturGetShipment.Click += new System.EventHandler(this.btFuturGETShipment_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbSaveAPI);
            this.groupBox1.Controls.Add(this.rbFakeAPI);
            this.groupBox1.Controls.Add(this.btJsonFile);
            this.groupBox1.Controls.Add(this.txJSONFile);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ckVerbose);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbMode);
            this.groupBox1.Location = new System.Drawing.Point(693, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(495, 193);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options manuelles";
            // 
            // rbSaveAPI
            // 
            this.rbSaveAPI.AutoSize = true;
            this.rbSaveAPI.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.rbSaveAPI.Location = new System.Drawing.Point(110, 103);
            this.rbSaveAPI.Name = "rbSaveAPI";
            this.rbSaveAPI.Size = new System.Drawing.Size(343, 32);
            this.rbSaveAPI.TabIndex = 68;
            this.rbSaveAPI.TabStop = true;
            this.rbSaveAPI.Text = "Enregistrer données reçues (import)";
            this.rbSaveAPI.UseVisualStyleBackColor = true;
            // 
            // rbFakeAPI
            // 
            this.rbFakeAPI.AutoSize = true;
            this.rbFakeAPI.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbFakeAPI.Location = new System.Drawing.Point(110, 128);
            this.rbFakeAPI.Name = "rbFakeAPI";
            this.rbFakeAPI.Size = new System.Drawing.Size(353, 32);
            this.rbFakeAPI.TabIndex = 67;
            this.rbFakeAPI.TabStop = true;
            this.rbFakeAPI.Text = "Remplace appel API (import+export)";
            this.rbFakeAPI.UseVisualStyleBackColor = true;
            // 
            // btJsonFile
            // 
            this.btJsonFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btJsonFile.Location = new System.Drawing.Point(459, 157);
            this.btJsonFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btJsonFile.Name = "btJsonFile";
            this.btJsonFile.Size = new System.Drawing.Size(30, 27);
            this.btJsonFile.TabIndex = 65;
            this.btJsonFile.Text = "...";
            this.btJsonFile.UseVisualStyleBackColor = true;
            this.btJsonFile.Click += new System.EventHandler(this.btJsonFile_Click);
            // 
            // txJSONFile
            // 
            this.txJSONFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txJSONFile.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txJSONFile.Location = new System.Drawing.Point(7, 159);
            this.txJSONFile.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txJSONFile.Name = "txJSONFile";
            this.txJSONFile.Size = new System.Drawing.Size(444, 29);
            this.txJSONFile.TabIndex = 66;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 28);
            this.label4.TabIndex = 64;
            this.label4.Text = "Fichier JSON";
            // 
            // ckVerbose
            // 
            this.ckVerbose.AutoSize = true;
            this.ckVerbose.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.ckVerbose.Location = new System.Drawing.Point(110, 24);
            this.ckVerbose.Name = "ckVerbose";
            this.ckVerbose.Size = new System.Drawing.Size(259, 41);
            this.ckVerbose.TabIndex = 63;
            this.ckVerbose.Text = "Mode Simultation";
            this.ckVerbose.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 67);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 28);
            this.label3.TabIndex = 23;
            this.label3.Text = "Mode export";
            // 
            // cbMode
            // 
            this.cbMode.FormattingEnabled = true;
            this.cbMode.Location = new System.Drawing.Point(116, 64);
            this.cbMode.Name = "cbMode";
            this.cbMode.Size = new System.Drawing.Size(326, 36);
            this.cbMode.TabIndex = 61;
            // 
            // btOpenLOG
            // 
            this.btOpenLOG.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.btOpenLOG.Location = new System.Drawing.Point(1092, 196);
            this.btOpenLOG.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btOpenLOG.Name = "btOpenLOG";
            this.btOpenLOG.Size = new System.Drawing.Size(90, 37);
            this.btOpenLOG.TabIndex = 27;
            this.btOpenLOG.Text = "Ouvrir";
            this.btOpenLOG.UseVisualStyleBackColor = true;
            this.btOpenLOG.Click += new System.EventHandler(this.btOpenLOG_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btExportArtWOO);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btImportWOO);
            this.panel1.Controls.Add(this.btExportDocWOO);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(675, 75);
            this.panel1.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 28);
            this.label6.TabIndex = 26;
            this.label6.Text = "WOO commerces";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 4);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 28);
            this.label2.TabIndex = 23;
            this.label2.Text = "Traitement";
            // 
            // btImportWOO
            // 
            this.btImportWOO.Location = new System.Drawing.Point(191, 11);
            this.btImportWOO.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btImportWOO.Name = "btImportWOO";
            this.btImportWOO.Size = new System.Drawing.Size(150, 50);
            this.btImportWOO.TabIndex = 22;
            this.btImportWOO.Text = "Importation Commandes";
            this.btImportWOO.UseVisualStyleBackColor = true;
            this.btImportWOO.Click += new System.EventHandler(this.btImportWOO_Click);
            // 
            // btExportDocWOO
            // 
            this.btExportDocWOO.Location = new System.Drawing.Point(353, 11);
            this.btExportDocWOO.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btExportDocWOO.Name = "btExportDocWOO";
            this.btExportDocWOO.Size = new System.Drawing.Size(150, 50);
            this.btExportDocWOO.TabIndex = 24;
            this.btExportDocWOO.Text = "Exportation Statut Livraisons";
            this.btExportDocWOO.UseVisualStyleBackColor = true;
            this.btExportDocWOO.Click += new System.EventHandler(this.btExportDocWOO_Click);
            // 
            // btLOG
            // 
            this.btLOG.Location = new System.Drawing.Point(953, 196);
            this.btLOG.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btLOG.Name = "btLOG";
            this.btLOG.Size = new System.Drawing.Size(93, 37);
            this.btLOG.TabIndex = 25;
            this.btLOG.Text = "Charger";
            this.btLOG.UseVisualStyleBackColor = true;
            this.btLOG.Click += new System.EventHandler(this.btLOG_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(807, 203);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 28);
            this.label1.TabIndex = 20;
            this.label1.Text = "Fichier Journal";
            // 
            // btExportArtWOO
            // 
            this.btExportArtWOO.Location = new System.Drawing.Point(512, 11);
            this.btExportArtWOO.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.btExportArtWOO.Name = "btExportArtWOO";
            this.btExportArtWOO.Size = new System.Drawing.Size(150, 50);
            this.btExportArtWOO.TabIndex = 27;
            this.btExportArtWOO.Text = "Exportation Produits/Tarifs";
            this.btExportArtWOO.UseVisualStyleBackColor = true;
            this.btExportArtWOO.Click += new System.EventHandler(this.btExportArtWOO_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 744);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormMain";
            this.Text = "Interface Document";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paramètresToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem traitementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem aProposToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem intégrationToolStripMenuItem;
        private System.Windows.Forms.Button btImportWOO;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.Button btExportDocWOO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btLOG;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btOpenLOG;
        private System.Windows.Forms.ComboBox cbMode;
        private System.Windows.Forms.Button btFuturExportProduits;
        private System.Windows.Forms.ToolStripMenuItem tsMenuParam;
        private System.Windows.Forms.CheckBox ckVerbose;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btJsonFile;
        private System.Windows.Forms.TextBox txJSONFile;
        private System.Windows.Forms.RadioButton rbSaveAPI;
        private System.Windows.Forms.RadioButton rbFakeAPI;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btFuturGetProducts;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btFuturSendOrder;
        private System.Windows.Forms.Button btFuturGetShipment;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btFuturSendOrderPurchase;
        private System.Windows.Forms.Button btFuturGetReceipt;
        private System.Windows.Forms.Button btExportArtWOO;
    }
}

