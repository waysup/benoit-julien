﻿namespace Interface_SAGE.Forms
{
    partial class FormParam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormParam));
            this.BtAnnul = new System.Windows.Forms.Button();
            this.BtOk = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ckBox2 = new System.Windows.Forms.CheckBox();
            this.PwBoxFTP = new System.Windows.Forms.MaskedTextBox();
            this.txtSSHFTP = new System.Windows.Forms.TextBox();
            this.txtUserFTP = new System.Windows.Forms.TextBox();
            this.txtAdrFTP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btTestFTP = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btSelAppli = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.txtBoxRaccourci = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.PwBox = new System.Windows.Forms.MaskedTextBox();
            this.ckPwd = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.chargerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sauvegarderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label85 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.ckProd = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btXML_TaskIntegrDoc = new System.Windows.Forms.Button();
            this.BtConversion2 = new System.Windows.Forms.Button();
            this.BtConversion = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label77 = new System.Windows.Forms.Label();
            this.txWOO_CptaDOM = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.txWOO_CptaUE = new System.Windows.Forms.TextBox();
            this.txWOO_CptaAutres = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label100 = new System.Windows.Forms.Label();
            this.txTiersPayeur3 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.txCatCompta3 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.txCatTarif3 = new System.Windows.Forms.TextBox();
            this.txCompteCol3 = new System.Windows.Forms.TextBox();
            this.txWOO_ISOFrance = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label99 = new System.Windows.Forms.Label();
            this.txTiersPayeur2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txCatCompta2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.txCatTarif2 = new System.Windows.Forms.TextBox();
            this.txCompteCol2 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.txStat3 = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.txStat2 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.txStat1 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.txtPrefixClient = new System.Windows.Forms.TextBox();
            this.txStatClient = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label101 = new System.Windows.Forms.Label();
            this.txTiersPayeur1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.txCatCompta1 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.txCatTarif1 = new System.Windows.Forms.TextBox();
            this.txCompteCol1 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label107 = new System.Windows.Forms.Label();
            this.txWOO_Libr_Horo = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label57 = new System.Windows.Forms.Label();
            this.txWOO_IDClient = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label56 = new System.Windows.Forms.Label();
            this.txWOO_Libr_OrigCde = new System.Windows.Forms.TextBox();
            this.txWOO_Libr_CommentLIV = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label110 = new System.Windows.Forms.Label();
            this.txtWOO_APIPass = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.txtWOO_APILogin = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.txWOO_Exportdir = new System.Windows.Forms.TextBox();
            this.btWOO_ExportDir = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.txWOO_FicNomPRODUCT = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.txtWOO_APIUrl = new System.Windows.Forms.TextBox();
            this.gpGratuit = new System.Windows.Forms.GroupBox();
            this.rbNoValeur = new System.Windows.Forms.RadioButton();
            this.rbRem100 = new System.Windows.Forms.RadioButton();
            this.ckWOO_Entete = new System.Windows.Forms.CheckBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txWOO_DataDel = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txWOO_DataSep = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.txWOO_FicNomORDER = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txWOO_WorkDir = new System.Windows.Forms.TextBox();
            this.btWOO_WorkDir = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.txtSoucheNewDoc = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtFuturAPIMerchantCode = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtFuturAPIURL = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtFuturAPILogin = new System.Windows.Forms.TextBox();
            this.txtFuturAPIKey = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.txtFuturDepot = new System.Windows.Forms.TextBox();
            this.ckExportComposant = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.ckExportConfirme = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFuturHoro = new System.Windows.Forms.TextBox();
            this.txDernImport_Liv = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtFuturListSouche = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txDernImport_Rec = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.txtFuturProductSend = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.txtFuturStockGet = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.txtFuturAchatGet = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.txtFuturAchatSend = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.txtFuturVentesGet = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.btXML_TaskPostStatutDoc = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.txtFuturVentesSend = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFTPURI = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFTPPort = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtFTPUser = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFTPMdp = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtFTPSSH = new System.Windows.Forms.TextBox();
            this.cbFTPProtocol = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtFTPPathProduct = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtFTPPathOrder = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gpGratuit.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtAnnul
            // 
            this.BtAnnul.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtAnnul.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtAnnul.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.BtAnnul.Location = new System.Drawing.Point(779, 776);
            this.BtAnnul.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtAnnul.Name = "BtAnnul";
            this.BtAnnul.Size = new System.Drawing.Size(114, 37);
            this.BtAnnul.TabIndex = 31;
            this.BtAnnul.Text = "Annuler";
            this.BtAnnul.UseVisualStyleBackColor = true;
            this.BtAnnul.Click += new System.EventHandler(this.BtAnnul_Click);
            // 
            // BtOk
            // 
            this.BtOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtOk.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.BtOk.Location = new System.Drawing.Point(544, 776);
            this.BtOk.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtOk.Name = "BtOk";
            this.BtOk.Size = new System.Drawing.Size(114, 37);
            this.BtOk.TabIndex = 30;
            this.BtOk.Text = "OK";
            this.BtOk.UseVisualStyleBackColor = true;
            this.BtOk.Click += new System.EventHandler(this.BtOk_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.ckBox2);
            this.panel1.Controls.Add(this.PwBoxFTP);
            this.panel1.Controls.Add(this.txtSSHFTP);
            this.panel1.Controls.Add(this.txtUserFTP);
            this.panel1.Controls.Add(this.txtAdrFTP);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.btTestFTP);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(16, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(323, 224);
            this.panel1.TabIndex = 33;
            // 
            // ckBox2
            // 
            this.ckBox2.AutoSize = true;
            this.ckBox2.Location = new System.Drawing.Point(120, 108);
            this.ckBox2.Name = "ckBox2";
            this.ckBox2.Size = new System.Drawing.Size(15, 14);
            this.ckBox2.TabIndex = 15;
            this.ckBox2.UseVisualStyleBackColor = true;
            // 
            // PwBoxFTP
            // 
            this.PwBoxFTP.Location = new System.Drawing.Point(141, 105);
            this.PwBoxFTP.Name = "PwBoxFTP";
            this.PwBoxFTP.Size = new System.Drawing.Size(162, 20);
            this.PwBoxFTP.TabIndex = 14;
            // 
            // txtSSHFTP
            // 
            this.txtSSHFTP.Location = new System.Drawing.Point(141, 131);
            this.txtSSHFTP.Name = "txtSSHFTP";
            this.txtSSHFTP.Size = new System.Drawing.Size(162, 20);
            this.txtSSHFTP.TabIndex = 13;
            // 
            // txtUserFTP
            // 
            this.txtUserFTP.Location = new System.Drawing.Point(141, 76);
            this.txtUserFTP.Name = "txtUserFTP";
            this.txtUserFTP.Size = new System.Drawing.Size(162, 20);
            this.txtUserFTP.TabIndex = 12;
            // 
            // txtAdrFTP
            // 
            this.txtAdrFTP.Location = new System.Drawing.Point(141, 49);
            this.txtAdrFTP.Name = "txtAdrFTP";
            this.txtAdrFTP.Size = new System.Drawing.Size(162, 20);
            this.txtAdrFTP.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(439, 33);
            this.label5.TabIndex = 10;
            this.label5.Text = "Paramétrage du Service FTP";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btTestFTP
            // 
            this.btTestFTP.Location = new System.Drawing.Point(141, 171);
            this.btTestFTP.Name = "btTestFTP";
            this.btTestFTP.Size = new System.Drawing.Size(162, 23);
            this.btTestFTP.TabIndex = 9;
            this.btTestFTP.Text = "Tester la connexion";
            this.btTestFTP.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Token SSH";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Adresse du service";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mot de Passe ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Utilisateur ";
            // 
            // btSelAppli
            // 
            this.btSelAppli.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSelAppli.Location = new System.Drawing.Point(1164, 58);
            this.btSelAppli.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSelAppli.Name = "btSelAppli";
            this.btSelAppli.Size = new System.Drawing.Size(30, 27);
            this.btSelAppli.TabIndex = 27;
            this.btSelAppli.Text = "...";
            this.btSelAppli.UseVisualStyleBackColor = true;
            this.btSelAppli.Click += new System.EventHandler(this.btSelAppli_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(11, 62);
            this.lbl1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(172, 19);
            this.lbl1.TabIndex = 28;
            this.lbl1.Text = "Raccourci de l\'application";
            // 
            // txtBoxRaccourci
            // 
            this.txtBoxRaccourci.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxRaccourci.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtBoxRaccourci.Location = new System.Drawing.Point(213, 59);
            this.txtBoxRaccourci.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBoxRaccourci.Name = "txtBoxRaccourci";
            this.txtBoxRaccourci.Size = new System.Drawing.Size(952, 25);
            this.txtBoxRaccourci.TabIndex = 29;
            this.txtBoxRaccourci.TextChanged += new System.EventHandler(this.txtBoxRaccourci_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 106);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 19);
            this.label6.TabIndex = 33;
            this.label6.Text = "Utilisateur de l\'application";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(823, 109);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 19);
            this.label7.TabIndex = 34;
            this.label7.Text = "Mot de Passe";
            // 
            // txtUser
            // 
            this.txtUser.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtUser.Location = new System.Drawing.Point(213, 105);
            this.txtUser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(224, 25);
            this.txtUser.TabIndex = 35;
            // 
            // PwBox
            // 
            this.PwBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PwBox.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.PwBox.Location = new System.Drawing.Point(945, 105);
            this.PwBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PwBox.Name = "PwBox";
            this.PwBox.Size = new System.Drawing.Size(220, 25);
            this.PwBox.TabIndex = 36;
            // 
            // ckPwd
            // 
            this.ckPwd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckPwd.AutoSize = true;
            this.ckPwd.Location = new System.Drawing.Point(918, 113);
            this.ckPwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckPwd.Name = "ckPwd";
            this.ckPwd.Size = new System.Drawing.Size(15, 14);
            this.ckPwd.TabIndex = 41;
            this.ckPwd.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 10, 0, 10);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chargerToolStripMenuItem,
            this.sauvegarderToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Margin = new System.Windows.Forms.Padding(30, 0, 30, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1238, 31);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // chargerToolStripMenuItem
            // 
            this.chargerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("chargerToolStripMenuItem.Image")));
            this.chargerToolStripMenuItem.Name = "chargerToolStripMenuItem";
            this.chargerToolStripMenuItem.Size = new System.Drawing.Size(100, 25);
            this.chargerToolStripMenuItem.Text = "Charger";
            this.chargerToolStripMenuItem.Click += new System.EventHandler(this.chargerToolStripMenuItem_Click);
            // 
            // sauvegarderToolStripMenuItem
            // 
            this.sauvegarderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sauvegarderToolStripMenuItem.Image")));
            this.sauvegarderToolStripMenuItem.Name = "sauvegarderToolStripMenuItem";
            this.sauvegarderToolStripMenuItem.Size = new System.Drawing.Size(134, 25);
            this.sauvegarderToolStripMenuItem.Text = "Sauvegarder";
            this.sauvegarderToolStripMenuItem.Click += new System.EventHandler(this.sauvegarderToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.tabControl1.Location = new System.Drawing.Point(5, 34);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1233, 734);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label85);
            this.tabPage1.Controls.Add(this.label83);
            this.tabPage1.Controls.Add(this.label82);
            this.tabPage1.Controls.Add(this.label81);
            this.tabPage1.Controls.Add(this.ckProd);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1225, 704);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Paramètres généraux";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(78, 503);
            this.label85.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(457, 19);
            this.label85.TabIndex = 104;
            this.label85.Text = "- Les échanges API seront mise en pause par Pop-UP à l\'interconnexion.";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(78, 440);
            this.label83.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(634, 19);
            this.label83.TabIndex = 103;
            this.label83.Text = "- * /!\\ *  Les Règlements ne sont jamais créés, ainsi le process est reversible e" +
    "t reproductibble  * /!\\ *";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(78, 538);
            this.label82.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(354, 19);
            this.label82.TabIndex = 102;
            this.label82.Text = "- Les informations journalisées sont plus conséquantes.";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(78, 471);
            this.label81.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(410, 19);
            this.label81.TabIndex = 42;
            this.label81.Text = "- Les échanges API seront enregistrés en fichier en entrée/sortie.";
            // 
            // ckProd
            // 
            this.ckProd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckProd.AutoSize = true;
            this.ckProd.Location = new System.Drawing.Point(35, 412);
            this.ckProd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckProd.Name = "ckProd";
            this.ckProd.Size = new System.Drawing.Size(187, 23);
            this.ckProd.TabIndex = 101;
            this.ckProd.Text = "Mode Information / TEST";
            this.ckProd.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.txtFTPPathProduct);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.txtFTPPathOrder);
            this.panel6.Controls.Add(this.cbFTPProtocol);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.txtFTPSSH);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.txtFTPMdp);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.txtFTPUser);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.txtFTPPort);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.txtFTPURI);
            this.panel6.Controls.Add(this.btXML_TaskIntegrDoc);
            this.panel6.Controls.Add(this.BtConversion2);
            this.panel6.Controls.Add(this.BtConversion);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Location = new System.Drawing.Point(7, 178);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1209, 197);
            this.panel6.TabIndex = 58;
            // 
            // btXML_TaskIntegrDoc
            // 
            this.btXML_TaskIntegrDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btXML_TaskIntegrDoc.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btXML_TaskIntegrDoc.Location = new System.Drawing.Point(915, 37);
            this.btXML_TaskIntegrDoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btXML_TaskIntegrDoc.Name = "btXML_TaskIntegrDoc";
            this.btXML_TaskIntegrDoc.Size = new System.Drawing.Size(279, 32);
            this.btXML_TaskIntegrDoc.TabIndex = 62;
            this.btXML_TaskIntegrDoc.Text = "Générer XML Tâche Planifiée";
            this.btXML_TaskIntegrDoc.UseVisualStyleBackColor = true;
            this.btXML_TaskIntegrDoc.Visible = false;
            // 
            // BtConversion2
            // 
            this.BtConversion2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtConversion2.Location = new System.Drawing.Point(26, 36);
            this.BtConversion2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtConversion2.Name = "BtConversion2";
            this.BtConversion2.Size = new System.Drawing.Size(206, 63);
            this.BtConversion2.TabIndex = 55;
            this.BtConversion2.Text = "Tables Correspondances WooCommerce";
            this.BtConversion2.UseVisualStyleBackColor = true;
            this.BtConversion2.Click += new System.EventHandler(this.BtConversion2_Click);
            // 
            // BtConversion
            // 
            this.BtConversion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtConversion.Location = new System.Drawing.Point(26, 109);
            this.BtConversion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtConversion.Name = "BtConversion";
            this.BtConversion.Size = new System.Drawing.Size(206, 62);
            this.BtConversion.TabIndex = 52;
            this.BtConversion.Text = "Tables Correspondances API Futurlog";
            this.BtConversion.UseVisualStyleBackColor = true;
            this.BtConversion.Visible = false;
            this.BtConversion.Click += new System.EventHandler(this.BtConversion_Click);
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label37.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(0, 0);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(1207, 32);
            this.label37.TabIndex = 10;
            this.label37.Text = "Paramétrage";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.lbl1);
            this.panel3.Controls.Add(this.txtBoxRaccourci);
            this.panel3.Controls.Add(this.btSelAppli);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.ckPwd);
            this.panel3.Controls.Add(this.txtUser);
            this.panel3.Controls.Add(this.PwBox);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Location = new System.Drawing.Point(7, 8);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1209, 158);
            this.panel3.TabIndex = 57;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(1207, 32);
            this.label16.TabIndex = 10;
            this.label16.Text = "Informations SAGE 100c";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Controls.Add(this.panel10);
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1225, 704);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Interface WooCommerce";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.label103);
            this.panel9.Controls.Add(this.label102);
            this.panel9.Controls.Add(this.groupBox12);
            this.panel9.Controls.Add(this.label80);
            this.panel9.Controls.Add(this.groupBox9);
            this.panel9.Controls.Add(this.txWOO_ISOFrance);
            this.panel9.Controls.Add(this.groupBox7);
            this.panel9.Controls.Add(this.label72);
            this.panel9.Controls.Add(this.txStat3);
            this.panel9.Controls.Add(this.label71);
            this.panel9.Controls.Add(this.txStat2);
            this.panel9.Controls.Add(this.label70);
            this.panel9.Controls.Add(this.txStat1);
            this.panel9.Controls.Add(this.label62);
            this.panel9.Controls.Add(this.txtPrefixClient);
            this.panel9.Controls.Add(this.txStatClient);
            this.panel9.Controls.Add(this.groupBox8);
            this.panel9.Controls.Add(this.label69);
            this.panel9.Controls.Add(this.label74);
            this.panel9.Location = new System.Drawing.Point(7, 356);
            this.panel9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1211, 343);
            this.panel9.TabIndex = 76;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic);
            this.label103.Location = new System.Drawing.Point(116, 305);
            this.label103.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(404, 19);
            this.label103.TabIndex = 89;
            this.label103.Text = "** Laissé vide pour les clients avec Compte comptable attitré **";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic);
            this.label102.Location = new System.Drawing.Point(116, 286);
            this.label102.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(612, 19);
            this.label102.TabIndex = 80;
            this.label102.Text = "** Le TIERS PAYEUR est à utilisé pour les clients se déversant dans le même compt" +
    "e comptable **";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label77);
            this.groupBox12.Controls.Add(this.txWOO_CptaDOM);
            this.groupBox12.Controls.Add(this.label78);
            this.groupBox12.Controls.Add(this.label79);
            this.groupBox12.Controls.Add(this.txWOO_CptaUE);
            this.groupBox12.Controls.Add(this.txWOO_CptaAutres);
            this.groupBox12.Location = new System.Drawing.Point(857, 120);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(319, 123);
            this.groupBox12.TabIndex = 85;
            this.groupBox12.TabStop = false;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(7, 25);
            this.label77.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(118, 17);
            this.label77.TabIndex = 72;
            this.label77.Text = "Cat. Comptable UE";
            // 
            // txWOO_CptaDOM
            // 
            this.txWOO_CptaDOM.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_CptaDOM.Location = new System.Drawing.Point(173, 46);
            this.txWOO_CptaDOM.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_CptaDOM.Name = "txWOO_CptaDOM";
            this.txWOO_CptaDOM.Size = new System.Drawing.Size(132, 25);
            this.txWOO_CptaDOM.TabIndex = 65;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(7, 50);
            this.label78.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(165, 17);
            this.label78.TabIndex = 63;
            this.label78.Text = "Cat. Comptable DOM TOM";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(7, 74);
            this.label79.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(139, 17);
            this.label79.TabIndex = 66;
            this.label79.Text = "Cat. Comptable Autres";
            // 
            // txWOO_CptaUE
            // 
            this.txWOO_CptaUE.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_CptaUE.Location = new System.Drawing.Point(173, 22);
            this.txWOO_CptaUE.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_CptaUE.Name = "txWOO_CptaUE";
            this.txWOO_CptaUE.Size = new System.Drawing.Size(132, 25);
            this.txWOO_CptaUE.TabIndex = 58;
            // 
            // txWOO_CptaAutres
            // 
            this.txWOO_CptaAutres.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_CptaAutres.Location = new System.Drawing.Point(173, 70);
            this.txWOO_CptaAutres.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_CptaAutres.Name = "txWOO_CptaAutres";
            this.txWOO_CptaAutres.Size = new System.Drawing.Size(132, 25);
            this.txWOO_CptaAutres.TabIndex = 64;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label80.Location = new System.Drawing.Point(863, 98);
            this.label80.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(113, 19);
            this.label80.TabIndex = 87;
            this.label80.Text = "Code ISO France";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label100);
            this.groupBox9.Controls.Add(this.txTiersPayeur3);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Controls.Add(this.txCatCompta3);
            this.groupBox9.Controls.Add(this.label75);
            this.groupBox9.Controls.Add(this.label76);
            this.groupBox9.Controls.Add(this.txCatTarif3);
            this.groupBox9.Controls.Add(this.txCompteCol3);
            this.groupBox9.Location = new System.Drawing.Point(544, 120);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(256, 163);
            this.groupBox9.TabIndex = 75;
            this.groupBox9.TabStop = false;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(7, 127);
            this.label100.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(79, 17);
            this.label100.TabIndex = 78;
            this.label100.Text = "Tiers Payeur";
            // 
            // txTiersPayeur3
            // 
            this.txTiersPayeur3.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txTiersPayeur3.Location = new System.Drawing.Point(113, 124);
            this.txTiersPayeur3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txTiersPayeur3.Name = "txTiersPayeur3";
            this.txTiersPayeur3.Size = new System.Drawing.Size(132, 25);
            this.txTiersPayeur3.TabIndex = 75;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(7, 25);
            this.label73.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(81, 17);
            this.label73.TabIndex = 72;
            this.label73.Text = "Cat. Tarifaire";
            // 
            // txCatCompta3
            // 
            this.txCatCompta3.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCatCompta3.Location = new System.Drawing.Point(114, 46);
            this.txCatCompta3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCatCompta3.Name = "txCatCompta3";
            this.txCatCompta3.Size = new System.Drawing.Size(132, 25);
            this.txCatCompta3.TabIndex = 65;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(7, 50);
            this.label75.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(98, 17);
            this.label75.TabIndex = 63;
            this.label75.Text = "Cat. Comptable";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(7, 74);
            this.label76.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(104, 17);
            this.label76.TabIndex = 66;
            this.label76.Text = "Compte Collectif";
            // 
            // txCatTarif3
            // 
            this.txCatTarif3.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCatTarif3.Location = new System.Drawing.Point(114, 22);
            this.txCatTarif3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCatTarif3.Name = "txCatTarif3";
            this.txCatTarif3.Size = new System.Drawing.Size(132, 25);
            this.txCatTarif3.TabIndex = 58;
            // 
            // txCompteCol3
            // 
            this.txCompteCol3.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCompteCol3.Location = new System.Drawing.Point(114, 70);
            this.txCompteCol3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCompteCol3.Name = "txCompteCol3";
            this.txCompteCol3.Size = new System.Drawing.Size(132, 25);
            this.txCompteCol3.TabIndex = 64;
            // 
            // txWOO_ISOFrance
            // 
            this.txWOO_ISOFrance.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.txWOO_ISOFrance.Location = new System.Drawing.Point(1031, 95);
            this.txWOO_ISOFrance.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_ISOFrance.Name = "txWOO_ISOFrance";
            this.txWOO_ISOFrance.Size = new System.Drawing.Size(30, 25);
            this.txWOO_ISOFrance.TabIndex = 86;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label99);
            this.groupBox7.Controls.Add(this.txTiersPayeur2);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.txCatCompta2);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label49);
            this.groupBox7.Controls.Add(this.txCatTarif2);
            this.groupBox7.Controls.Add(this.txCompteCol2);
            this.groupBox7.Location = new System.Drawing.Point(278, 120);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(256, 163);
            this.groupBox7.TabIndex = 75;
            this.groupBox7.TabStop = false;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(7, 128);
            this.label99.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(79, 17);
            this.label99.TabIndex = 77;
            this.label99.Text = "Tiers Payeur";
            // 
            // txTiersPayeur2
            // 
            this.txTiersPayeur2.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txTiersPayeur2.Location = new System.Drawing.Point(119, 124);
            this.txTiersPayeur2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txTiersPayeur2.Name = "txTiersPayeur2";
            this.txTiersPayeur2.Size = new System.Drawing.Size(132, 25);
            this.txTiersPayeur2.TabIndex = 74;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(7, 25);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 17);
            this.label11.TabIndex = 72;
            this.label11.Text = "Cat. Tarifaire";
            // 
            // txCatCompta2
            // 
            this.txCatCompta2.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCatCompta2.Location = new System.Drawing.Point(119, 46);
            this.txCatCompta2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCatCompta2.Name = "txCatCompta2";
            this.txCatCompta2.Size = new System.Drawing.Size(132, 25);
            this.txCatCompta2.TabIndex = 65;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 50);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 17);
            this.label21.TabIndex = 63;
            this.label21.Text = "Cat. Comptable";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(7, 74);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(104, 17);
            this.label49.TabIndex = 66;
            this.label49.Text = "Compte Collectif";
            // 
            // txCatTarif2
            // 
            this.txCatTarif2.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCatTarif2.Location = new System.Drawing.Point(119, 22);
            this.txCatTarif2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCatTarif2.Name = "txCatTarif2";
            this.txCatTarif2.Size = new System.Drawing.Size(132, 25);
            this.txCatTarif2.TabIndex = 58;
            // 
            // txCompteCol2
            // 
            this.txCompteCol2.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCompteCol2.Location = new System.Drawing.Point(119, 70);
            this.txCompteCol2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCompteCol2.Name = "txCompteCol2";
            this.txCompteCol2.Size = new System.Drawing.Size(132, 25);
            this.txCompteCol2.TabIndex = 64;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label72.Location = new System.Drawing.Point(544, 98);
            this.label72.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(88, 19);
            this.label72.TabIndex = 84;
            this.label72.Text = "Catégorie 3 :";
            // 
            // txStat3
            // 
            this.txStat3.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.txStat3.Location = new System.Drawing.Point(658, 95);
            this.txStat3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txStat3.Name = "txStat3";
            this.txStat3.Size = new System.Drawing.Size(131, 25);
            this.txStat3.TabIndex = 83;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label71.Location = new System.Drawing.Point(278, 98);
            this.label71.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(88, 19);
            this.label71.TabIndex = 82;
            this.label71.Text = "Catégorie 2 :";
            // 
            // txStat2
            // 
            this.txStat2.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.txStat2.Location = new System.Drawing.Point(396, 95);
            this.txStat2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txStat2.Name = "txStat2";
            this.txStat2.Size = new System.Drawing.Size(131, 25);
            this.txStat2.TabIndex = 81;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label70.Location = new System.Drawing.Point(7, 98);
            this.label70.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(86, 19);
            this.label70.TabIndex = 80;
            this.label70.Text = "Catégorie 1 :";
            // 
            // txStat1
            // 
            this.txStat1.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.txStat1.Location = new System.Drawing.Point(120, 95);
            this.txStat1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txStat1.Name = "txStat1";
            this.txStat1.Size = new System.Drawing.Size(131, 25);
            this.txStat1.TabIndex = 79;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label62.Location = new System.Drawing.Point(7, 42);
            this.label62.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(181, 19);
            this.label62.TabIndex = 88;
            this.label62.Text = "Préfixe pour nouveau client";
            // 
            // txtPrefixClient
            // 
            this.txtPrefixClient.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtPrefixClient.Location = new System.Drawing.Point(223, 39);
            this.txtPrefixClient.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPrefixClient.Name = "txtPrefixClient";
            this.txtPrefixClient.Size = new System.Drawing.Size(29, 25);
            this.txtPrefixClient.TabIndex = 87;
            // 
            // txStatClient
            // 
            this.txStatClient.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txStatClient.Location = new System.Drawing.Point(223, 66);
            this.txStatClient.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txStatClient.Name = "txStatClient";
            this.txStatClient.Size = new System.Drawing.Size(29, 25);
            this.txStatClient.TabIndex = 64;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label101);
            this.groupBox8.Controls.Add(this.txTiersPayeur1);
            this.groupBox8.Controls.Add(this.label8);
            this.groupBox8.Controls.Add(this.label58);
            this.groupBox8.Controls.Add(this.txCatCompta1);
            this.groupBox8.Controls.Add(this.label68);
            this.groupBox8.Controls.Add(this.label67);
            this.groupBox8.Controls.Add(this.txCatTarif1);
            this.groupBox8.Controls.Add(this.txCompteCol1);
            this.groupBox8.Location = new System.Drawing.Point(7, 120);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(256, 163);
            this.groupBox8.TabIndex = 74;
            this.groupBox8.TabStop = false;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(7, 128);
            this.label101.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(79, 17);
            this.label101.TabIndex = 79;
            this.label101.Text = "Tiers Payeur";
            // 
            // txTiersPayeur1
            // 
            this.txTiersPayeur1.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txTiersPayeur1.Location = new System.Drawing.Point(113, 124);
            this.txTiersPayeur1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txTiersPayeur1.Name = "txTiersPayeur1";
            this.txTiersPayeur1.Size = new System.Drawing.Size(132, 25);
            this.txTiersPayeur1.TabIndex = 76;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(24, 100);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(218, 15);
            this.label8.TabIndex = 73;
            this.label8.Text = "** utilisée pour export tarif FUTURLOG **";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(7, 25);
            this.label58.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(81, 17);
            this.label58.TabIndex = 72;
            this.label58.Text = "Cat. Tarifaire";
            // 
            // txCatCompta1
            // 
            this.txCatCompta1.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCatCompta1.Location = new System.Drawing.Point(113, 46);
            this.txCatCompta1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCatCompta1.Name = "txCatCompta1";
            this.txCatCompta1.Size = new System.Drawing.Size(132, 25);
            this.txCatCompta1.TabIndex = 65;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(7, 50);
            this.label68.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(98, 17);
            this.label68.TabIndex = 63;
            this.label68.Text = "Cat. Comptable";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(7, 74);
            this.label67.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(104, 17);
            this.label67.TabIndex = 66;
            this.label67.Text = "Compte Collectif";
            // 
            // txCatTarif1
            // 
            this.txCatTarif1.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCatTarif1.Location = new System.Drawing.Point(113, 22);
            this.txCatTarif1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCatTarif1.Name = "txCatTarif1";
            this.txCatTarif1.Size = new System.Drawing.Size(132, 25);
            this.txCatTarif1.TabIndex = 58;
            // 
            // txCompteCol1
            // 
            this.txCompteCol1.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txCompteCol1.Location = new System.Drawing.Point(113, 70);
            this.txCompteCol1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txCompteCol1.Name = "txCompteCol1";
            this.txCompteCol1.Size = new System.Drawing.Size(132, 25);
            this.txCompteCol1.TabIndex = 64;
            // 
            // label69
            // 
            this.label69.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label69.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label69.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label69.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label69.Location = new System.Drawing.Point(0, 0);
            this.label69.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(1209, 32);
            this.label69.TabIndex = 10;
            this.label69.Text = "Catégorie des clients";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(5, 69);
            this.label74.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(192, 17);
            this.label74.TabIndex = 66;
            this.label74.Text = "(N° Indice STAT) Type de Clients";
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.groupBox2);
            this.panel10.Controls.Add(this.groupBox10);
            this.panel10.Controls.Add(this.groupBox11);
            this.panel10.Controls.Add(this.label84);
            this.panel10.Location = new System.Drawing.Point(7, 208);
            this.panel10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1211, 140);
            this.panel10.TabIndex = 75;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label107);
            this.groupBox2.Controls.Add(this.txWOO_Libr_Horo);
            this.groupBox2.Location = new System.Drawing.Point(346, 35);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 88);
            this.groupBox2.TabIndex = 83;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ligne de document";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label107.Location = new System.Drawing.Point(7, 27);
            this.label107.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(106, 17);
            this.label107.TabIndex = 82;
            this.label107.Text = "Horodatage Livr.";
            // 
            // txWOO_Libr_Horo
            // 
            this.txWOO_Libr_Horo.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_Libr_Horo.Location = new System.Drawing.Point(117, 24);
            this.txWOO_Libr_Horo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_Libr_Horo.Name = "txWOO_Libr_Horo";
            this.txWOO_Libr_Horo.Size = new System.Drawing.Size(177, 25);
            this.txWOO_Libr_Horo.TabIndex = 81;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label57);
            this.groupBox10.Controls.Add(this.txWOO_IDClient);
            this.groupBox10.Location = new System.Drawing.Point(665, 33);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(313, 88);
            this.groupBox10.TabIndex = 73;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Clients";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label57.Location = new System.Drawing.Point(7, 27);
            this.label57.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(92, 17);
            this.label57.TabIndex = 82;
            this.label57.Text = "ID Client WOO";
            // 
            // txWOO_IDClient
            // 
            this.txWOO_IDClient.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_IDClient.Location = new System.Drawing.Point(101, 24);
            this.txWOO_IDClient.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_IDClient.Name = "txWOO_IDClient";
            this.txWOO_IDClient.Size = new System.Drawing.Size(177, 25);
            this.txWOO_IDClient.TabIndex = 81;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label56);
            this.groupBox11.Controls.Add(this.txWOO_Libr_OrigCde);
            this.groupBox11.Controls.Add(this.txWOO_Libr_CommentLIV);
            this.groupBox11.Controls.Add(this.label59);
            this.groupBox11.Location = new System.Drawing.Point(10, 33);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(327, 88);
            this.groupBox11.TabIndex = 68;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Documents";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label56.Location = new System.Drawing.Point(7, 27);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(122, 17);
            this.label56.TabIndex = 80;
            this.label56.Text = "Origine Commande";
            // 
            // txWOO_Libr_OrigCde
            // 
            this.txWOO_Libr_OrigCde.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_Libr_OrigCde.Location = new System.Drawing.Point(135, 24);
            this.txWOO_Libr_OrigCde.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_Libr_OrigCde.Name = "txWOO_Libr_OrigCde";
            this.txWOO_Libr_OrigCde.Size = new System.Drawing.Size(177, 25);
            this.txWOO_Libr_OrigCde.TabIndex = 79;
            // 
            // txWOO_Libr_CommentLIV
            // 
            this.txWOO_Libr_CommentLIV.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_Libr_CommentLIV.Location = new System.Drawing.Point(135, 53);
            this.txWOO_Libr_CommentLIV.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_Libr_CommentLIV.Name = "txWOO_Libr_CommentLIV";
            this.txWOO_Libr_CommentLIV.Size = new System.Drawing.Size(177, 25);
            this.txWOO_Libr_CommentLIV.TabIndex = 85;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label59.Location = new System.Drawing.Point(7, 56);
            this.label59.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(113, 17);
            this.label59.TabIndex = 86;
            this.label59.Text = "Commentaires LIV";
            // 
            // label84
            // 
            this.label84.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label84.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label84.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label84.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label84.Location = new System.Drawing.Point(0, 0);
            this.label84.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(1209, 32);
            this.label84.TabIndex = 10;
            this.label84.Text = "Libellés Champs Libres";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label110);
            this.panel2.Controls.Add(this.txtWOO_APIPass);
            this.panel2.Controls.Add(this.label109);
            this.panel2.Controls.Add(this.txtWOO_APILogin);
            this.panel2.Controls.Add(this.label108);
            this.panel2.Controls.Add(this.label106);
            this.panel2.Controls.Add(this.txWOO_Exportdir);
            this.panel2.Controls.Add(this.btWOO_ExportDir);
            this.panel2.Controls.Add(this.label105);
            this.panel2.Controls.Add(this.txWOO_FicNomPRODUCT);
            this.panel2.Controls.Add(this.label104);
            this.panel2.Controls.Add(this.txtWOO_APIUrl);
            this.panel2.Controls.Add(this.gpGratuit);
            this.panel2.Controls.Add(this.ckWOO_Entete);
            this.panel2.Controls.Add(this.label66);
            this.panel2.Controls.Add(this.label65);
            this.panel2.Controls.Add(this.txWOO_DataDel);
            this.panel2.Controls.Add(this.label64);
            this.panel2.Controls.Add(this.txWOO_DataSep);
            this.panel2.Controls.Add(this.label63);
            this.panel2.Controls.Add(this.txWOO_FicNomORDER);
            this.panel2.Controls.Add(this.label46);
            this.panel2.Controls.Add(this.txWOO_WorkDir);
            this.panel2.Controls.Add(this.btWOO_WorkDir);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.txtSoucheNewDoc);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Location = new System.Drawing.Point(7, 8);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1211, 193);
            this.panel2.TabIndex = 55;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label110.Location = new System.Drawing.Point(698, 164);
            this.label110.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(36, 19);
            this.label110.TabIndex = 113;
            this.label110.Text = "Pass";
            // 
            // txtWOO_APIPass
            // 
            this.txtWOO_APIPass.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtWOO_APIPass.Location = new System.Drawing.Point(742, 161);
            this.txtWOO_APIPass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtWOO_APIPass.Name = "txtWOO_APIPass";
            this.txtWOO_APIPass.Size = new System.Drawing.Size(424, 25);
            this.txtWOO_APIPass.TabIndex = 112;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label109.Location = new System.Drawing.Point(430, 164);
            this.label109.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(44, 19);
            this.label109.TabIndex = 111;
            this.label109.Text = "Login";
            // 
            // txtWOO_APILogin
            // 
            this.txtWOO_APILogin.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtWOO_APILogin.Location = new System.Drawing.Point(474, 161);
            this.txtWOO_APILogin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtWOO_APILogin.Name = "txtWOO_APILogin";
            this.txtWOO_APILogin.Size = new System.Drawing.Size(216, 25);
            this.txtWOO_APILogin.TabIndex = 110;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label108.Location = new System.Drawing.Point(162, 164);
            this.label108.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(35, 19);
            this.label108.TabIndex = 109;
            this.label108.Text = "URL";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(5, 78);
            this.label106.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(149, 19);
            this.label106.TabIndex = 108;
            this.label106.Text = "Répertoire des exports";
            // 
            // txWOO_Exportdir
            // 
            this.txWOO_Exportdir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txWOO_Exportdir.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_Exportdir.Location = new System.Drawing.Point(162, 75);
            this.txWOO_Exportdir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_Exportdir.Name = "txWOO_Exportdir";
            this.txWOO_Exportdir.Size = new System.Drawing.Size(636, 25);
            this.txWOO_Exportdir.TabIndex = 107;
            // 
            // btWOO_ExportDir
            // 
            this.btWOO_ExportDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btWOO_ExportDir.Location = new System.Drawing.Point(796, 73);
            this.btWOO_ExportDir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btWOO_ExportDir.Name = "btWOO_ExportDir";
            this.btWOO_ExportDir.Size = new System.Drawing.Size(30, 27);
            this.btWOO_ExportDir.TabIndex = 106;
            this.btWOO_ExportDir.Text = "...";
            this.btWOO_ExportDir.UseVisualStyleBackColor = true;
            this.btWOO_ExportDir.Click += new System.EventHandler(this.btWOO_ExportDir_Click);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label105.Location = new System.Drawing.Point(5, 137);
            this.label105.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(128, 19);
            this.label105.TabIndex = 105;
            this.label105.Text = "Fichier PRODUCTS";
            // 
            // txWOO_FicNomPRODUCT
            // 
            this.txWOO_FicNomPRODUCT.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_FicNomPRODUCT.Location = new System.Drawing.Point(162, 134);
            this.txWOO_FicNomPRODUCT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_FicNomPRODUCT.Name = "txWOO_FicNomPRODUCT";
            this.txWOO_FicNomPRODUCT.Size = new System.Drawing.Size(260, 25);
            this.txWOO_FicNomPRODUCT.TabIndex = 104;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label104.Location = new System.Drawing.Point(5, 164);
            this.label104.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(99, 19);
            this.label104.TabIndex = 103;
            this.label104.Text = "API DELIVERY ";
            // 
            // txtWOO_APIUrl
            // 
            this.txtWOO_APIUrl.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtWOO_APIUrl.Location = new System.Drawing.Point(206, 161);
            this.txtWOO_APIUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtWOO_APIUrl.Name = "txtWOO_APIUrl";
            this.txtWOO_APIUrl.Size = new System.Drawing.Size(216, 25);
            this.txtWOO_APIUrl.TabIndex = 102;
            // 
            // gpGratuit
            // 
            this.gpGratuit.Controls.Add(this.rbNoValeur);
            this.gpGratuit.Controls.Add(this.rbRem100);
            this.gpGratuit.Location = new System.Drawing.Point(964, 35);
            this.gpGratuit.Name = "gpGratuit";
            this.gpGratuit.Size = new System.Drawing.Size(198, 62);
            this.gpGratuit.TabIndex = 101;
            this.gpGratuit.TabStop = false;
            this.gpGratuit.Text = "Gratuit ou Prix à 0";
            // 
            // rbNoValeur
            // 
            this.rbNoValeur.AutoSize = true;
            this.rbNoValeur.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.rbNoValeur.Location = new System.Drawing.Point(90, 29);
            this.rbNoValeur.Name = "rbNoValeur";
            this.rbNoValeur.Size = new System.Drawing.Size(100, 21);
            this.rbNoValeur.TabIndex = 1;
            this.rbNoValeur.TabStop = true;
            this.rbNoValeur.Text = "Non valorisé";
            this.rbNoValeur.UseVisualStyleBackColor = true;
            // 
            // rbRem100
            // 
            this.rbRem100.AutoSize = true;
            this.rbRem100.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.rbRem100.Location = new System.Drawing.Point(6, 29);
            this.rbRem100.Name = "rbRem100";
            this.rbRem100.Size = new System.Drawing.Size(64, 21);
            this.rbRem100.TabIndex = 0;
            this.rbRem100.TabStop = true;
            this.rbRem100.Text = "PU à 0";
            this.rbRem100.UseVisualStyleBackColor = true;
            // 
            // ckWOO_Entete
            // 
            this.ckWOO_Entete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckWOO_Entete.AutoSize = true;
            this.ckWOO_Entete.Location = new System.Drawing.Point(774, 110);
            this.ckWOO_Entete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckWOO_Entete.Name = "ckWOO_Entete";
            this.ckWOO_Entete.Size = new System.Drawing.Size(15, 14);
            this.ckWOO_Entete.TabIndex = 100;
            this.ckWOO_Entete.UseVisualStyleBackColor = true;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label66.Location = new System.Drawing.Point(682, 108);
            this.label66.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(82, 19);
            this.label66.TabIndex = 99;
            this.label66.Text = "Avec entête";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label65.Location = new System.Drawing.Point(557, 108);
            this.label65.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(75, 19);
            this.label65.TabIndex = 97;
            this.label65.Text = "Délimiteur";
            // 
            // txWOO_DataDel
            // 
            this.txWOO_DataDel.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_DataDel.Location = new System.Drawing.Point(646, 105);
            this.txWOO_DataDel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_DataDel.Name = "txWOO_DataDel";
            this.txWOO_DataDel.Size = new System.Drawing.Size(19, 25);
            this.txWOO_DataDel.TabIndex = 96;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label64.Location = new System.Drawing.Point(430, 108);
            this.label64.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(76, 19);
            this.label64.TabIndex = 95;
            this.label64.Text = "Séparateur";
            // 
            // txWOO_DataSep
            // 
            this.txWOO_DataSep.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_DataSep.Location = new System.Drawing.Point(519, 105);
            this.txWOO_DataSep.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_DataSep.Name = "txWOO_DataSep";
            this.txWOO_DataSep.Size = new System.Drawing.Size(19, 25);
            this.txWOO_DataSep.TabIndex = 94;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label63.Location = new System.Drawing.Point(5, 108);
            this.label63.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(101, 19);
            this.label63.TabIndex = 93;
            this.label63.Text = "Fichier ORDER";
            // 
            // txWOO_FicNomORDER
            // 
            this.txWOO_FicNomORDER.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_FicNomORDER.Location = new System.Drawing.Point(162, 105);
            this.txWOO_FicNomORDER.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_FicNomORDER.Name = "txWOO_FicNomORDER";
            this.txWOO_FicNomORDER.Size = new System.Drawing.Size(260, 25);
            this.txWOO_FicNomORDER.TabIndex = 92;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(5, 49);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(151, 19);
            this.label46.TabIndex = 91;
            this.label46.Text = "Répertoire des imports";
            // 
            // txWOO_WorkDir
            // 
            this.txWOO_WorkDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txWOO_WorkDir.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txWOO_WorkDir.Location = new System.Drawing.Point(162, 46);
            this.txWOO_WorkDir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txWOO_WorkDir.Name = "txWOO_WorkDir";
            this.txWOO_WorkDir.Size = new System.Drawing.Size(636, 25);
            this.txWOO_WorkDir.TabIndex = 90;
            // 
            // btWOO_WorkDir
            // 
            this.btWOO_WorkDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btWOO_WorkDir.Location = new System.Drawing.Point(796, 44);
            this.btWOO_WorkDir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btWOO_WorkDir.Name = "btWOO_WorkDir";
            this.btWOO_WorkDir.Size = new System.Drawing.Size(30, 27);
            this.btWOO_WorkDir.TabIndex = 89;
            this.btWOO_WorkDir.Text = "...";
            this.btWOO_WorkDir.UseVisualStyleBackColor = true;
            this.btWOO_WorkDir.Click += new System.EventHandler(this.btWOO_WorkDir_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label45.Location = new System.Drawing.Point(971, 102);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(191, 19);
            this.label45.TabIndex = 74;
            this.label45.Text = "Code Souche des Documents";
            // 
            // txtSoucheNewDoc
            // 
            this.txtSoucheNewDoc.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtSoucheNewDoc.Location = new System.Drawing.Point(965, 126);
            this.txtSoucheNewDoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSoucheNewDoc.Name = "txtSoucheNewDoc";
            this.txtSoucheNewDoc.Size = new System.Drawing.Size(201, 25);
            this.txtSoucheNewDoc.TabIndex = 73;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(1209, 32);
            this.label14.TabIndex = 10;
            this.label14.Text = "Information Intégration Document";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel11);
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1225, 704);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Interface FuturLog";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.txtFuturAPIMerchantCode);
            this.panel11.Controls.Add(this.label23);
            this.panel11.Controls.Add(this.label50);
            this.panel11.Controls.Add(this.label47);
            this.panel11.Controls.Add(this.label38);
            this.panel11.Controls.Add(this.txtFuturAPIURL);
            this.panel11.Controls.Add(this.label48);
            this.panel11.Controls.Add(this.txtFuturAPILogin);
            this.panel11.Controls.Add(this.txtFuturAPIKey);
            this.panel11.Location = new System.Drawing.Point(7, 5);
            this.panel11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(940, 132);
            this.panel11.TabIndex = 100;
            // 
            // txtFuturAPIMerchantCode
            // 
            this.txtFuturAPIMerchantCode.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturAPIMerchantCode.Location = new System.Drawing.Point(163, 87);
            this.txtFuturAPIMerchantCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturAPIMerchantCode.Name = "txtFuturAPIMerchantCode";
            this.txtFuturAPIMerchantCode.Size = new System.Drawing.Size(224, 25);
            this.txtFuturAPIMerchantCode.TabIndex = 85;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(0, 0);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(938, 32);
            this.label23.TabIndex = 10;
            this.label23.Text = "Identification";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(7, 90);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(105, 19);
            this.label50.TabIndex = 86;
            this.label50.Text = "Merchant Code";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(407, 48);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(44, 19);
            this.label47.TabIndex = 81;
            this.label47.Text = "Login";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(7, 48);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 19);
            this.label38.TabIndex = 79;
            this.label38.Text = "URL racine";
            // 
            // txtFuturAPIURL
            // 
            this.txtFuturAPIURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFuturAPIURL.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturAPIURL.Location = new System.Drawing.Point(163, 45);
            this.txtFuturAPIURL.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturAPIURL.Name = "txtFuturAPIURL";
            this.txtFuturAPIURL.Size = new System.Drawing.Size(224, 25);
            this.txtFuturAPIURL.TabIndex = 80;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(407, 90);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(32, 19);
            this.label48.TabIndex = 83;
            this.label48.Text = "Key";
            // 
            // txtFuturAPILogin
            // 
            this.txtFuturAPILogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFuturAPILogin.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturAPILogin.Location = new System.Drawing.Point(484, 45);
            this.txtFuturAPILogin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturAPILogin.Name = "txtFuturAPILogin";
            this.txtFuturAPILogin.Size = new System.Drawing.Size(390, 25);
            this.txtFuturAPILogin.TabIndex = 82;
            // 
            // txtFuturAPIKey
            // 
            this.txtFuturAPIKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFuturAPIKey.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturAPIKey.Location = new System.Drawing.Point(484, 87);
            this.txtFuturAPIKey.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturAPIKey.Name = "txtFuturAPIKey";
            this.txtFuturAPIKey.Size = new System.Drawing.Size(390, 25);
            this.txtFuturAPIKey.TabIndex = 84;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label60);
            this.panel4.Controls.Add(this.label53);
            this.panel4.Controls.Add(this.txtFuturDepot);
            this.panel4.Controls.Add(this.ckExportComposant);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.ckExportConfirme);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.txtFuturHoro);
            this.panel4.Controls.Add(this.txDernImport_Liv);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.txtFuturListSouche);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.txDernImport_Rec);
            this.panel4.Location = new System.Drawing.Point(9, 504);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(940, 181);
            this.panel4.TabIndex = 78;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(710, 130);
            this.label60.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(116, 15);
            this.label60.TabIndex = 109;
            this.label60.Text = "vide pour tous dépôt";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label53.Location = new System.Drawing.Point(539, 106);
            this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(102, 19);
            this.label53.TabIndex = 108;
            this.label53.Text = "Dépôt futurlog";
            // 
            // txtFuturDepot
            // 
            this.txtFuturDepot.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturDepot.Location = new System.Drawing.Point(777, 103);
            this.txtFuturDepot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturDepot.Name = "txtFuturDepot";
            this.txtFuturDepot.Size = new System.Drawing.Size(135, 25);
            this.txtFuturDepot.TabIndex = 107;
            // 
            // ckExportComposant
            // 
            this.ckExportComposant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckExportComposant.AutoSize = true;
            this.ckExportComposant.Location = new System.Drawing.Point(781, 150);
            this.ckExportComposant.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckExportComposant.Name = "ckExportComposant";
            this.ckExportComposant.Size = new System.Drawing.Size(15, 14);
            this.ckExportComposant.TabIndex = 106;
            this.ckExportComposant.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(579, 148);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(198, 19);
            this.label31.TabIndex = 105;
            this.label31.Text = "Export de composant BUNDLE";
            // 
            // ckExportConfirme
            // 
            this.ckExportConfirme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckExportConfirme.AutoSize = true;
            this.ckExportConfirme.Location = new System.Drawing.Point(498, 153);
            this.ckExportConfirme.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ckExportConfirme.Name = "ckExportConfirme";
            this.ckExportConfirme.Size = new System.Drawing.Size(15, 14);
            this.ckExportConfirme.TabIndex = 104;
            this.ckExportConfirme.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(260, 148);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(230, 19);
            this.label30.TabIndex = 103;
            this.label30.Text = "Limiter l\'export au Statut A préparé";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 79);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(294, 19);
            this.label18.TabIndex = 99;
            this.label18.Text = "Champ libre Horodatage des pièces exportées";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(5, 106);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(200, 19);
            this.label10.TabIndex = 79;
            this.label10.Text = "Date Dernier Import Livraisons";
            // 
            // txtFuturHoro
            // 
            this.txtFuturHoro.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturHoro.Location = new System.Drawing.Point(354, 73);
            this.txtFuturHoro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturHoro.Name = "txtFuturHoro";
            this.txtFuturHoro.Size = new System.Drawing.Size(135, 25);
            this.txtFuturHoro.TabIndex = 58;
            // 
            // txDernImport_Liv
            // 
            this.txDernImport_Liv.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txDernImport_Liv.Location = new System.Drawing.Point(354, 101);
            this.txDernImport_Liv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txDernImport_Liv.Name = "txDernImport_Liv";
            this.txDernImport_Liv.Size = new System.Drawing.Size(135, 25);
            this.txDernImport_Liv.TabIndex = 78;
            this.txDernImport_Liv.Validating += new System.ComponentModel.CancelEventHandler(this.txDernImport_Liv_Validating);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(938, 32);
            this.label19.TabIndex = 10;
            this.label19.Text = "Paramètrages";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 42);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(155, 19);
            this.label26.TabIndex = 48;
            this.label26.Text = "Liste Souche à exporter";
            // 
            // txtFuturListSouche
            // 
            this.txtFuturListSouche.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturListSouche.Location = new System.Drawing.Point(164, 39);
            this.txtFuturListSouche.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturListSouche.Name = "txtFuturListSouche";
            this.txtFuturListSouche.Size = new System.Drawing.Size(325, 25);
            this.txtFuturListSouche.TabIndex = 47;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(493, 45);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(313, 15);
            this.label24.TabIndex = 74;
            this.label24.Text = "vide ou * = Toutes / sinon liste des codes séparées par des ;";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(539, 76);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(202, 19);
            this.label44.TabIndex = 81;
            this.label44.Text = "Date Denier Import Receptions";
            // 
            // txDernImport_Rec
            // 
            this.txDernImport_Rec.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txDernImport_Rec.Location = new System.Drawing.Point(777, 73);
            this.txDernImport_Rec.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txDernImport_Rec.Name = "txDernImport_Rec";
            this.txDernImport_Rec.Size = new System.Drawing.Size(135, 25);
            this.txDernImport_Rec.TabIndex = 80;
            this.txDernImport_Rec.Validating += new System.ComponentModel.CancelEventHandler(this.txDernImport_Rec_Validating);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label98);
            this.panel5.Controls.Add(this.label97);
            this.panel5.Controls.Add(this.label96);
            this.panel5.Controls.Add(this.label94);
            this.panel5.Controls.Add(this.txtFuturProductSend);
            this.panel5.Controls.Add(this.label95);
            this.panel5.Controls.Add(this.label92);
            this.panel5.Controls.Add(this.txtFuturStockGet);
            this.panel5.Controls.Add(this.label93);
            this.panel5.Controls.Add(this.label90);
            this.panel5.Controls.Add(this.txtFuturAchatGet);
            this.panel5.Controls.Add(this.label91);
            this.panel5.Controls.Add(this.label88);
            this.panel5.Controls.Add(this.txtFuturAchatSend);
            this.panel5.Controls.Add(this.label89);
            this.panel5.Controls.Add(this.label86);
            this.panel5.Controls.Add(this.txtFuturVentesGet);
            this.panel5.Controls.Add(this.label87);
            this.panel5.Controls.Add(this.btXML_TaskPostStatutDoc);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.txtFuturVentesSend);
            this.panel5.Controls.Add(this.label29);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Location = new System.Drawing.Point(7, 146);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(940, 350);
            this.panel5.TabIndex = 56;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(4, 32);
            this.label98.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(50, 19);
            this.label98.TabIndex = 98;
            this.label98.Text = "Ventes";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(4, 127);
            this.label97.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(51, 19);
            this.label97.TabIndex = 97;
            this.label97.Text = "Achats";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(4, 213);
            this.label96.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(116, 19);
            this.label96.TabIndex = 80;
            this.label96.Text = "Produits et Stock";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(497, 267);
            this.label94.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(356, 15);
            this.label94.TabIndex = 96;
            this.label94.Text = "Exemple : /Product/CreateNewProducts/{merchantCode}/{login}/{key}";
            // 
            // txtFuturProductSend
            // 
            this.txtFuturProductSend.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturProductSend.Location = new System.Drawing.Point(163, 259);
            this.txtFuturProductSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturProductSend.Name = "txtFuturProductSend";
            this.txtFuturProductSend.Size = new System.Drawing.Size(325, 25);
            this.txtFuturProductSend.TabIndex = 94;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label95.Location = new System.Drawing.Point(34, 264);
            this.label95.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(102, 17);
            this.label95.TabIndex = 95;
            this.label95.Text = "Envoi Catalogue";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(497, 243);
            this.label92.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(313, 15);
            this.label92.TabIndex = 93;
            this.label92.Text = "Exemple : /Product/GetStocks/{merchantCode}/{login}/{ key}/";
            // 
            // txtFuturStockGet
            // 
            this.txtFuturStockGet.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturStockGet.Location = new System.Drawing.Point(163, 235);
            this.txtFuturStockGet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturStockGet.Name = "txtFuturStockGet";
            this.txtFuturStockGet.Size = new System.Drawing.Size(325, 25);
            this.txtFuturStockGet.TabIndex = 91;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label93.Location = new System.Drawing.Point(34, 240);
            this.label93.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(102, 17);
            this.label93.TabIndex = 92;
            this.label93.Text = "Interroger Stock";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(497, 182);
            this.label90.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(392, 15);
            this.label90.TabIndex = 90;
            this.label90.Text = "Exemple : /Product/GetReceipts/{merchantCode}/{login}/{key}/{dateFromUtc}";
            // 
            // txtFuturAchatGet
            // 
            this.txtFuturAchatGet.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturAchatGet.Location = new System.Drawing.Point(163, 174);
            this.txtFuturAchatGet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturAchatGet.Name = "txtFuturAchatGet";
            this.txtFuturAchatGet.Size = new System.Drawing.Size(325, 25);
            this.txtFuturAchatGet.TabIndex = 88;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label91.Location = new System.Drawing.Point(34, 179);
            this.label91.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(109, 17);
            this.label91.TabIndex = 89;
            this.label91.Text = "Retour Réception";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(497, 158);
            this.label88.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(395, 15);
            this.label88.TabIndex = 87;
            this.label88.Text = "Exemple : /Product/CreateNewExpectedReceipt/{merchantCode}/{login}/{key}";
            // 
            // txtFuturAchatSend
            // 
            this.txtFuturAchatSend.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturAchatSend.Location = new System.Drawing.Point(163, 150);
            this.txtFuturAchatSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturAchatSend.Name = "txtFuturAchatSend";
            this.txtFuturAchatSend.Size = new System.Drawing.Size(325, 25);
            this.txtFuturAchatSend.TabIndex = 85;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label89.Location = new System.Drawing.Point(34, 155);
            this.label89.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(110, 17);
            this.label89.TabIndex = 86;
            this.label89.Text = "Envoi Commande";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(497, 89);
            this.label86.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(393, 15);
            this.label86.TabIndex = 84;
            this.label86.Text = "Exemple : /Order/GetShipments/{merchantCode}/{login}/{key}/{dateFromUtc}";
            // 
            // txtFuturVentesGet
            // 
            this.txtFuturVentesGet.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturVentesGet.Location = new System.Drawing.Point(163, 81);
            this.txtFuturVentesGet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturVentesGet.Name = "txtFuturVentesGet";
            this.txtFuturVentesGet.Size = new System.Drawing.Size(325, 25);
            this.txtFuturVentesGet.TabIndex = 82;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label87.Location = new System.Drawing.Point(34, 86);
            this.label87.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(102, 17);
            this.label87.TabIndex = 83;
            this.label87.Text = "Retour Livraison";
            // 
            // btXML_TaskPostStatutDoc
            // 
            this.btXML_TaskPostStatutDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btXML_TaskPostStatutDoc.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btXML_TaskPostStatutDoc.Location = new System.Drawing.Point(639, 303);
            this.btXML_TaskPostStatutDoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btXML_TaskPostStatutDoc.Name = "btXML_TaskPostStatutDoc";
            this.btXML_TaskPostStatutDoc.Size = new System.Drawing.Size(279, 32);
            this.btXML_TaskPostStatutDoc.TabIndex = 62;
            this.btXML_TaskPostStatutDoc.Text = "Générer XML Tâche Planifiée";
            this.btXML_TaskPostStatutDoc.UseVisualStyleBackColor = true;
            this.btXML_TaskPostStatutDoc.Visible = false;
            this.btXML_TaskPostStatutDoc.Click += new System.EventHandler(this.btXML_TaskPostStatutDoc_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI Emoji", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(497, 65);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(331, 15);
            this.label28.TabIndex = 77;
            this.label28.Text = "Exemple : /Order/CreateNewOrder/{merchantCode}/{login}/{key}";
            // 
            // txtFuturVentesSend
            // 
            this.txtFuturVentesSend.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFuturVentesSend.Location = new System.Drawing.Point(163, 57);
            this.txtFuturVentesSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFuturVentesSend.Name = "txtFuturVentesSend";
            this.txtFuturVentesSend.Size = new System.Drawing.Size(325, 25);
            this.txtFuturVentesSend.TabIndex = 75;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label29.Location = new System.Drawing.Point(34, 62);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(110, 17);
            this.label29.TabIndex = 76;
            this.label29.Text = "Envoi Commande";
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(0, 0);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(938, 32);
            this.label25.TabIndex = 10;
            this.label25.Text = "Syntaxe des connexions API";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.toolStripStatusLabel2});
            this.statusStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.statusStrip1.Location = new System.Drawing.Point(0, 822);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1238, 75);
            this.statusStrip1.TabIndex = 71;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.lblStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(1235, 19);
            this.lblStatus.Text = "état Connexion";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(1235, 19);
            this.toolStripStatusLabel2.Text = "FicParam Info";
            this.toolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(351, 37);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 19);
            this.label9.TabIndex = 42;
            this.label9.Text = "Accès FTP : URI";
            // 
            // txtFTPURI
            // 
            this.txtFTPURI.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFTPURI.Location = new System.Drawing.Point(468, 34);
            this.txtFTPURI.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFTPURI.Name = "txtFTPURI";
            this.txtFTPURI.Size = new System.Drawing.Size(224, 25);
            this.txtFTPURI.TabIndex = 43;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(351, 67);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 19);
            this.label12.TabIndex = 63;
            this.label12.Text = "Protocole";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(607, 67);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 19);
            this.label13.TabIndex = 65;
            this.label13.Text = "Port";
            // 
            // txtFTPPort
            // 
            this.txtFTPPort.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFTPPort.Location = new System.Drawing.Point(667, 64);
            this.txtFTPPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFTPPort.Name = "txtFTPPort";
            this.txtFTPPort.Size = new System.Drawing.Size(25, 25);
            this.txtFTPPort.TabIndex = 66;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(351, 127);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 19);
            this.label15.TabIndex = 67;
            this.label15.Text = "Utilisateur";
            // 
            // txtFTPUser
            // 
            this.txtFTPUser.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFTPUser.Location = new System.Drawing.Point(468, 124);
            this.txtFTPUser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFTPUser.Name = "txtFTPUser";
            this.txtFTPUser.Size = new System.Drawing.Size(224, 25);
            this.txtFTPUser.TabIndex = 68;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(351, 156);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(92, 19);
            this.label17.TabIndex = 69;
            this.label17.Text = "Mot de Passe";
            // 
            // txtFTPMdp
            // 
            this.txtFTPMdp.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFTPMdp.Location = new System.Drawing.Point(468, 153);
            this.txtFTPMdp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFTPMdp.Name = "txtFTPMdp";
            this.txtFTPMdp.Size = new System.Drawing.Size(224, 25);
            this.txtFTPMdp.TabIndex = 70;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(351, 97);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 19);
            this.label20.TabIndex = 71;
            this.label20.Text = "SSH";
            // 
            // txtFTPSSH
            // 
            this.txtFTPSSH.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFTPSSH.Location = new System.Drawing.Point(468, 94);
            this.txtFTPSSH.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFTPSSH.Name = "txtFTPSSH";
            this.txtFTPSSH.Size = new System.Drawing.Size(224, 25);
            this.txtFTPSSH.TabIndex = 72;
            // 
            // cbFTPProtocol
            // 
            this.cbFTPProtocol.FormattingEnabled = true;
            this.cbFTPProtocol.Location = new System.Drawing.Point(468, 65);
            this.cbFTPProtocol.Name = "cbFTPProtocol";
            this.cbFTPProtocol.Size = new System.Drawing.Size(58, 25);
            this.cbFTPProtocol.TabIndex = 73;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(716, 156);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(155, 19);
            this.label22.TabIndex = 112;
            this.label22.Text = "Répertoire des produits";
            // 
            // txtFTPPathProduct
            // 
            this.txtFTPPathProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFTPPathProduct.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFTPPathProduct.Location = new System.Drawing.Point(915, 153);
            this.txtFTPPathProduct.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFTPPathProduct.Name = "txtFTPPathProduct";
            this.txtFTPPathProduct.Size = new System.Drawing.Size(279, 25);
            this.txtFTPPathProduct.TabIndex = 111;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(716, 127);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(180, 19);
            this.label27.TabIndex = 110;
            this.label27.Text = "Répertoire des Commandes";
            // 
            // txtFTPPathOrder
            // 
            this.txtFTPPathOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFTPPathOrder.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.txtFTPPathOrder.Location = new System.Drawing.Point(915, 124);
            this.txtFTPPathOrder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFTPPathOrder.Name = "txtFTPPathOrder";
            this.txtFTPPathOrder.Size = new System.Drawing.Size(279, 25);
            this.txtFTPPathOrder.TabIndex = 109;
            // 
            // FormParam
            // 
            this.AcceptButton = this.BtOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.BtAnnul;
            this.ClientSize = new System.Drawing.Size(1238, 897);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.BtOk);
            this.Controls.Add(this.BtAnnul);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(4, 28);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormParam";
            this.Text = "Paramètres";
            this.Load += new System.EventHandler(this.FormParam_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gpGratuit.ResumeLayout(false);
            this.gpGratuit.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BtAnnul;
        private System.Windows.Forms.Button BtOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox ckBox2;
        private System.Windows.Forms.MaskedTextBox PwBoxFTP;
        private System.Windows.Forms.TextBox txtSSHFTP;
        private System.Windows.Forms.TextBox txtUserFTP;
        private System.Windows.Forms.TextBox txtAdrFTP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btTestFTP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtBoxRaccourci;
        private System.Windows.Forms.CheckBox ckPwd;
        private System.Windows.Forms.MaskedTextBox PwBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Button btSelAppli;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem chargerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button BtConversion;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtFuturHoro;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtFuturListSouche;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Button btXML_TaskPostStatutDoc;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtFuturVentesSend;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txDernImport_Liv;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txDernImport_Rec;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtSoucheNewDoc;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txWOO_IDClient;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txWOO_Libr_OrigCde;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txWOO_Libr_CommentLIV;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtPrefixClient;
        private System.Windows.Forms.Button BtConversion2;
        private System.Windows.Forms.Button btXML_TaskIntegrDoc;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txWOO_WorkDir;
        private System.Windows.Forms.Button btWOO_WorkDir;
        private System.Windows.Forms.CheckBox ckWOO_Entete;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txWOO_DataDel;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox txWOO_DataSep;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox txWOO_FicNomORDER;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txStat3;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txStat2;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox txStat1;
        private System.Windows.Forms.TextBox txStatClient;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txCatCompta1;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txCatTarif1;
        private System.Windows.Forms.TextBox txCompteCol1;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox txCatCompta3;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox txCatTarif3;
        private System.Windows.Forms.TextBox txCompteCol3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txCatCompta2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txCatTarif2;
        private System.Windows.Forms.TextBox txCompteCol2;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox txWOO_CptaDOM;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox txWOO_CptaUE;
        private System.Windows.Forms.TextBox txWOO_CptaAutres;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox txWOO_ISOFrance;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.CheckBox ckProd;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.GroupBox gpGratuit;
        private System.Windows.Forms.RadioButton rbNoValeur;
        private System.Windows.Forms.RadioButton rbRem100;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox txtFuturAchatGet;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox txtFuturAchatSend;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox txtFuturVentesGet;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox txtFuturStockGet;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox txtFuturProductSend;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox txtFuturAPIMerchantCode;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtFuturAPIURL;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtFuturAPILogin;
        private System.Windows.Forms.TextBox txtFuturAPIKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox ckExportConfirme;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox ckExportComposant;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtFuturDepot;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox txTiersPayeur3;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox txTiersPayeur2;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox txTiersPayeur1;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.TextBox txWOO_Exportdir;
        private System.Windows.Forms.Button btWOO_ExportDir;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox txWOO_FicNomPRODUCT;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox txtWOO_APIUrl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox txWOO_Libr_Horo;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox txtWOO_APIPass;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox txtWOO_APILogin;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtFTPSSH;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFTPMdp;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtFTPUser;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtFTPPort;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFTPURI;
        private System.Windows.Forms.ComboBox cbFTPProtocol;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtFTPPathProduct;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtFTPPathOrder;
    }
}