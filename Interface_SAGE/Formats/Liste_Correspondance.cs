﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Interface_SAGE.Helper;
using System.Reflection;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;
using Interface_SAGE.Entities;
using Interface_SAGE.Core;

namespace Interface_SAGE.Formats
{
    public class Liste_Correspondance
    {
        public MainProgram Core;

        private Dictionary<Type, List<Propriété>> liste_correspondance;
        public Dictionary<Type, List<Propriété>> Liste_correspondance { get =>  liste_correspondance; }
        #region Methode statique SINGLETON
        private static Liste_Correspondance Instance;

        public static Liste_Correspondance Get_Liste_Correspondance(MainProgram core = null)
        {
            if (Instance == null )
            {
                Instance = new Liste_Correspondance(core);
                return Instance;
            }
            if (core == null) // Cas des appel statique ou isolés
            {
                return Instance;
            }
            if ( Instance.Core.Equals(core) == false)
            {
                Instance = new Liste_Correspondance(core);
                return Instance;
            }
            else
                return Instance;
        }
        #endregion Methode statique SINGLETON

        private Liste_Correspondance(MainProgram core)
        {
            Core = core;
            Charge_Propriétés(Core.GetParametres.WooConversion);
        }

        public object Conv_allDonnee(Object o)
        {

            if (liste_correspondance.TryGetValue(o.GetType(), out List<Propriété> lst) == true)
            {
                foreach( Propriété propriété in lst.Where(x => x.Conversion_autorisee == true) )
                {
                    this.get_donnee(o, propriété.Nom_Propriété);
                }
            }
            else
                throw new ArgumentException(o.GetType().Name + " Non supporté ");

            return o;
        }
        public object get_donnee(Object obj, String Nom_Propriété)
        {
            object donnee = null;

            // Récupération dde la valeur de la propriété par réflexion
            Type type = obj.GetType();
            PropertyInfo property = null;

            PropertyInfo[] props = type.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                if (prop.Name == Nom_Propriété)
                {
                    property = prop;
                    break;
                }
            }

            donnee = property.GetValue(obj, null);
            // Gestion des conversion
            // Récupération de l'objet Propriété 
            Propriété propriété;
            if (liste_correspondance.TryGetValue(obj.GetType(), out List<Propriété> lst) == true)
            {
                propriété = lst.Find(x => x.Nom_Propriété.Equals(Nom_Propriété));
            }
            else
                throw new ArgumentException(obj.GetType().Name + " Non supporté ou " + Nom_Propriété + " non trouvé");

            // recherche d'une correspondance
            if (propriété.Conversion_autorisee == true)
            {
                foreach (KeyValuePair<String, String> conv in propriété.Liste_conversion)
                {
                    // TODO La conversion n'est paramétrée que sur des données STRING / A justifier
                    // TODO vérifier le cast sur des donnée DATETIME et DOUBLE
                    if (conv.Key == donnee.ToString())
                    {
                        try
                        {
                            if (property.PropertyType.Equals(typeof(Double)))
                                property.SetValue(obj, Double.Parse(conv.Value));
                            if (property.PropertyType.Equals(typeof(Decimal)))
                                property.SetValue(obj, Decimal.Parse(conv.Value));
                            if (property.PropertyType.Equals(typeof(int)))
                                property.SetValue(obj, Int32.Parse(conv.Value));
                            if (property.PropertyType.Equals(typeof(DateTime)))
                                property.SetValue(obj, DateTime.Parse(conv.Value));
                            if (property.PropertyType.Equals(typeof(Boolean)))
                                property.SetValue(obj, Boolean.Parse(conv.Value));
                            if (property.PropertyType.Equals(typeof(String)))
                                property.SetValue(obj, conv.Value);
                        }
                        catch (InvalidCastException iE)
                        {
                            property.SetValue(obj, conv.Value);
                        }

                        return conv.Value;
                    }
                }
            }

            return donnee;
        }
        public void Set_donnee_Reverse(Object obj, String Nom_Propriété, Object donnée)
        {
            object conv_donnee = donnée == null ? "" : donnée;

            // Gestion des conversion
            // Récupération de l'objet Propriété 
            Propriété propriété;
            if (liste_correspondance.TryGetValue(obj.GetType(), out List<Propriété> lst) == true)
            {
                propriété = lst.Find(x => x.Nom_Propriété.Equals(Nom_Propriété));
            }
            else
                throw new ArgumentException(obj.GetType().Name + " Non supporté ou " + Nom_Propriété + " non trouvé");

            // recherche d'une correspondance
            if (propriété.Conversion_autorisee == true)
            {
                foreach (KeyValuePair<String, String> conv in propriété.Liste_conversion)
                {
                    String key = conv.Key == null ? "" : conv.Key ;
                    String value = conv.Value == null ? "" : conv.Value;


                    // TODO La conversion n'est paramétrée que sur des données STRING / A justifier
                    // TODO vérifier le cast sur des donnée DATETIME et DOUBLE
                    if (key == conv_donnee.ToString())
                        conv_donnee = value;
                }
            }
            
            // Affectation dde la valeur de la propriété par réflexion
            Type type = obj.GetType();
            PropertyInfo[] props = type.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                if (prop.Name == Nom_Propriété)
                {
                    try
                    {
                        if (prop.PropertyType.Equals(typeof(Double)))
                            prop.SetValue(obj, (Double)conv_donnee);
                        if (prop.PropertyType.Equals(typeof(Decimal)))
                            prop.SetValue(obj, (Decimal)conv_donnee);
                        if (prop.PropertyType.Equals(typeof(int)))
                            prop.SetValue(obj, (int)conv_donnee);
                        if (prop.PropertyType.Equals(typeof(String)))
                            prop.SetValue(obj, (String)conv_donnee);
                    }
                    catch (InvalidCastException iE)
                    {
                        prop.SetValue(obj, donnée);
                    }
                }
            }

        }
        #region Colonne SAGE
        public static List<Propriété> Get_Propriétés<T>()
        {
            Type t = typeof(T);

            PropertyInfo[] props = t.GetProperties();

            List<Propriété> Liste = new List<Propriété>();

            foreach (PropertyInfo prop in props)
            {
                Propriété P = new Propriété();

                P.Nom_Propriété = prop.Name;
                
                if ( t == typeof(WOO_Doc))
                {
                    if (prop.Name == "Cde_Etat" |
                        prop.Name == "Cde_Devise" |
                        prop.Name == "Mode_Paiement" |
                        prop.Name == "Cde_LivraisonCode" |
                        prop.Name == "Client_TypeClient")

                        P.Conversion_autorisee = true;
                    else
                        P.Conversion_autorisee = false;
                }
                else if ( t == typeof(WOO_DOC_statut))
                {
                    if (prop.Name == "Cde_Etat"  )
                        P.Conversion_autorisee = true;
                    else
                        P.Conversion_autorisee = false;
                }
                else if (t == typeof(WOO_Stock))
                {
                    if (prop.Name == "Famille" |
                        prop.Name == "Taux")
                        P.Conversion_autorisee = true;
                    else
                        P.Conversion_autorisee = false;
                }

                Liste.Add(P);
            }
            
            return Liste;
        }

        private void Charge_Propriétés(String Fic_conversion)
        {
            liste_correspondance = new Dictionary<Type, List<Propriété>>();
            if (String.IsNullOrEmpty(Fic_conversion) == true)
                return;

            liste_correspondance.Add(typeof(WOO_Doc), Get_Propriétés<WOO_Doc>());
            liste_correspondance.Add(typeof(WOO_DOC_statut), Get_Propriétés<WOO_DOC_statut>());
            liste_correspondance.Add(typeof(WOO_Stock), Get_Propriétés<WOO_Stock>());

            int nb_lig = 0;
            Boolean abandon = false;

            try
            {
                // Lecture du fichier ligne à ligne 
                using (StreamReader SR = new StreamReader(Fic_conversion, Encoding.UTF8, true))
                {
                    CsvConfiguration csvConfiguration = new CsvConfiguration(System.Globalization.CultureInfo.InvariantCulture, delimiter: ";");
                    using (CsvReader csv = new CsvReader(SR, csvConfiguration))
                    {

                        while (csv.Read() == true & abandon == false)
                        {
                            String[] data = new string[4];
                            nb_lig++;
                            // Lecture de la ligne en cours
                            if (csv.ColumnCount != 4)
                            {
                                try
                                {
                                    // Qualification de la nouvelle ligne
                                    data[0] = csv.GetField(0);
                                    data[1] = csv.GetField(1);
                                    data[2] = csv.GetField(2);
                                    data[3] = csv.GetField(3);
                                }
                                catch (Exception E)
                                {
                                    throw new Exception("Erreur Ligne " + nb_lig + ", nombre de colonne attendue = 4. / ou erreur détail :\n" + E.Message);
                                }

                                foreach (var elt in liste_correspondance)
                                    if (elt.Key.Name == data[0])
                                        foreach (Propriété prop in elt.Value)
                                            if (prop.Nom_Propriété == data[1])
                                            {
                                                if (prop.Conversion_autorisee == true)
                                                {
                                                    prop.Liste_conversion.Add(data[2], data[3]);
                                                    break;
                                                }
                                                else
                                                {
                                                    throw new Exception("Erreur Ligne " + nb_lig + ", Conversion sur colonne non autorisée.");
                                                }
                                            }
                            }
                        }
                    }
                }
            }
            catch (Exception E)
            {
                throw new Exception("Erreur inatendue : " + E.Message);
            }

        }
        #endregion 

        public void Sauve_Liste_correspondance(Dictionary<Type, List<Propriété>> new_liste, String new_Fic)
        {
            if (String.IsNullOrEmpty(new_Fic) == true)
                return;
            try
            {
                // Lecture du fichier ligne à ligne 
                using (StreamWriter SW = new StreamWriter(new_Fic,false, Encoding.UTF8))
                {
                    CsvConfiguration csvConfiguration = new CsvConfiguration(System.Globalization.CultureInfo.InvariantCulture,delimiter:";");

                    using (CsvWriter csv = new CsvWriter(SW, csvConfiguration))
                    {
                        foreach (var elt in new_liste)
                            foreach (Propriété prop in elt.Value)
                                if (prop.Conversion_autorisee == true)
                                    foreach (var conv in prop.Liste_conversion)
                                    {
                                        String[] data = new string[4];
                                        data[0] = elt.Key.Name;
                                        data[1] = prop.Nom_Propriété;
                                        data[2] = conv.Key;
                                        data[3] = conv.Value;

                                        csv.WriteField<String>(elt.Key.Name);
                                        csv.WriteField<String>(prop.Nom_Propriété);
                                        csv.WriteField<String>(conv.Key);
                                        csv.WriteField<String>(conv.Value);
                                        csv.NextRecord();
                                    }
                    }
                }
            }
            catch (Exception E)
            {
                throw new Exception("Erreur inatendue : " + E.Message);
            }


        }

    }
    public class Propriété
    {
        public String Nom_Propriété;
        public Boolean Obligatoire;
        public Int16 Position;
        private Boolean conversion_autorisee;
        public SortedList<String, String> Liste_conversion;

        public bool Conversion_autorisee
        {
            get
            { return conversion_autorisee; }
            set
            {
                if (value == false)
                    Liste_conversion = null;
                else
                    Liste_conversion = new SortedList<String, String>();

                conversion_autorisee = value;
            }
        }

        public Propriété()
        {
            Conversion_autorisee = false;
            Obligatoire = false;
        }

    }

   
    
}

